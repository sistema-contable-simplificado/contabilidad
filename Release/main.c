/*
 * Harbour 3.2.0dev (r1801051438)
 * Borland/Embarcadero C++ 7.0 (32-bit)
 * Generated C source from "C:\contabilidad\prg\main.prg"
 */

#include "hbvmpub.h"
#include "hbinit.h"


HB_FUNC( MAIN );
HB_FUNC_EXTERN( OVERRIDEANDEXTEND );
HB_FUNC_EXTERN( SET );
HB_FUNC_EXTERN( __SETCENTURY );
HB_FUNC_EXTERN( D_SETCASESENSITIVE );
HB_FUNC_EXTERN( D_SETPADRIGHT );
HB_FUNC_EXTERN( D_LOGICALVALUE );
HB_FUNC_EXTERN( USECLIPPERDEFAULTVALUE );
HB_FUNC_EXTERN( SET_MYLANG );
HB_FUNC_EXTERN( HB_LANGSELECT );
HB_FUNC_EXTERN( HB_SETCODEPAGE );
HB_FUNC_EXTERN( RDDSETDEFAULT );
HB_FUNC_EXTERN( FW_SETUNICODE );
HB_FUNC_EXTERN( FWNUMFORMAT );
HB_FUNC_EXTERN( SETCANCEL );
HB_FUNC_EXTERN( SETBALLOON );
HB_FUNC_EXTERN( SETGETCOLORFOCUS );
HB_FUNC_EXTERN( SETCBXCOLORFOCUS );
HB_FUNC_EXTERN( SETMGETCOLORFOCUS );
HB_FUNC_EXTERN( ISEXERUNNING );
HB_FUNC_EXTERN( CFILENAME );
HB_FUNC_EXTERN( HB_ARGV );
HB_FUNC_EXTERN( MSGINFO );
HB_FUNC_EXTERN( TVARIABLE );
HB_FUNC_STATIC( OPENSERVERMYSQL );
HB_FUNC_STATIC( ACCESOTABLASMYSQL );
HB_FUNC_EXTERN( GETNHEIGHTITEM );
HB_FUNC_EXTERN( GETNWIDTHITEM );
HB_FUNC_EXTERN( TFONT );
HB_FUNC_EXTERN( TWINDOW );
HB_FUNC_EXTERN( MSGYESNO );
HB_FUNC_EXTERN( GETKEYSTATE );
HB_FUNC_EXTERN( TMSGBAR );
HB_FUNC_EXTERN( TMSGITEM );
HB_FUNC_EXTERN( AMPM );
HB_FUNC_EXTERN( TIME );
HB_FUNC_STATIC( CREATEBUTTONBAR );
HB_FUNC_EXTERN( HB_ISOBJECT );
HB_FUNC( OAPP );
HB_FUNC_EXTERN( ARRAY );
HB_FUNC_EXTERN( TBAR );
HB_FUNC_EXTERN( TBTNBMP );
HB_FUNC_EXTERN( DO_EMPRESAS );
HB_FUNC_STATIC( POPUPTABLAS );
HB_FUNC_EXTERN( DO_ASIENTOS );
HB_FUNC_STATIC( POPUPREPORTE );
HB_FUNC_STATIC( POPUPDESIGNER );
HB_FUNC_EXTERN( TMYSQLSERVER );
HB_FUNC_STATIC( CLOSESERVERMYSQL );
HB_FUNC_EXTERN( MSGSTOP );
HB_FUNC_EXTERN( MSGPROGRESS );
HB_FUNC_STATIC( CHECKTABLASMYSQL );
HB_FUNC_EXTERN( CREAR_CATALOGO_MAESTRO );
HB_FUNC_EXTERN( CREAR_EMPRESAS );
HB_FUNC_EXTERN( CREAR_TIPOEMPRESA );
HB_FUNC_EXTERN( CREAR_CLIENTES );
HB_FUNC_EXTERN( CREAR_PROVEDORES );
HB_FUNC_EXTERN( CREAR_CATALOGO );
HB_FUNC_EXTERN( CREAR_REGIONES );
HB_FUNC_EXTERN( CREAR_CIUDADES );
HB_FUNC_EXTERN( CREAR_COMUNAS );
HB_FUNC_EXTERN( CREAR_TIPODOCUMENTO );
HB_FUNC_EXTERN( CREAR_BALANCE );
HB_FUNC_EXTERN( CREAR_ASIENTOMAESTRO );
HB_FUNC_EXTERN( CREAR_ASIENTOLINEAS );
HB_FUNC_EXTERN( CREAR_TEMPLIBROMAYOR );
HB_FUNC_EXTERN( MENUBEGIN );
HB_FUNC_EXTERN( MENUADDITEM );
HB_FUNC_EXTERN( CATALOGO_MAESTRO );
HB_FUNC_EXTERN( REGIONES );
HB_FUNC_EXTERN( CIUDADES );
HB_FUNC_EXTERN( COMUNAS );
HB_FUNC_EXTERN( TIPO_DOCUMENTO );
HB_FUNC_EXTERN( CATALOGO );
HB_FUNC_EXTERN( CLIENTES );
HB_FUNC_EXTERN( PROVEDORES );
HB_FUNC_EXTERN( MENUEND );
HB_FUNC_EXTERN( FR3_CATALOGO );
HB_FUNC_EXTERN( FR3_CLIENTE );
HB_FUNC_EXTERN( FR3_PROVEDOR );
HB_FUNC_EXTERN( FR3_LIBRODIARIOBORRADOR );
HB_FUNC_EXTERN( FR3_LIBRODIARIOOFICIAL );
HB_FUNC_EXTERN( FR3_LIBROMAYOR );
HB_FUNC_EXTERN( FR3_LIBROBALANCE );
HB_FUNC_EXTERN( FW_GT );
HB_FUNC_EXTERN( ERRORSYS );
HB_FUNC_EXTERN( DBFFPT );
HB_FUNC_EXTERN( DBFCDX );
HB_FUNC_EXTERN( HB_LANG_ES );
HB_FUNC_EXTERN( HB_CODEPAGE_ESWIN );
HB_FUNC_INITSTATICS();


HB_INIT_SYMBOLS_BEGIN( hb_vm_SymbolInit_MAIN )
{ "MAIN", {HB_FS_PUBLIC | HB_FS_FIRST | HB_FS_LOCAL}, {HB_FUNCNAME( MAIN )}, NULL },
{ "OVERRIDEANDEXTEND", {HB_FS_PUBLIC}, {HB_FUNCNAME( OVERRIDEANDEXTEND )}, NULL },
{ "SET", {HB_FS_PUBLIC}, {HB_FUNCNAME( SET )}, NULL },
{ "__SETCENTURY", {HB_FS_PUBLIC}, {HB_FUNCNAME( __SETCENTURY )}, NULL },
{ "D_SETCASESENSITIVE", {HB_FS_PUBLIC}, {HB_FUNCNAME( D_SETCASESENSITIVE )}, NULL },
{ "D_SETPADRIGHT", {HB_FS_PUBLIC}, {HB_FUNCNAME( D_SETPADRIGHT )}, NULL },
{ "D_LOGICALVALUE", {HB_FS_PUBLIC}, {HB_FUNCNAME( D_LOGICALVALUE )}, NULL },
{ "USECLIPPERDEFAULTVALUE", {HB_FS_PUBLIC}, {HB_FUNCNAME( USECLIPPERDEFAULTVALUE )}, NULL },
{ "SET_MYLANG", {HB_FS_PUBLIC}, {HB_FUNCNAME( SET_MYLANG )}, NULL },
{ "HB_LANGSELECT", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_LANGSELECT )}, NULL },
{ "HB_SETCODEPAGE", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_SETCODEPAGE )}, NULL },
{ "RDDSETDEFAULT", {HB_FS_PUBLIC}, {HB_FUNCNAME( RDDSETDEFAULT )}, NULL },
{ "FW_SETUNICODE", {HB_FS_PUBLIC}, {HB_FUNCNAME( FW_SETUNICODE )}, NULL },
{ "FWNUMFORMAT", {HB_FS_PUBLIC}, {HB_FUNCNAME( FWNUMFORMAT )}, NULL },
{ "SETCANCEL", {HB_FS_PUBLIC}, {HB_FUNCNAME( SETCANCEL )}, NULL },
{ "SETBALLOON", {HB_FS_PUBLIC}, {HB_FUNCNAME( SETBALLOON )}, NULL },
{ "SETGETCOLORFOCUS", {HB_FS_PUBLIC}, {HB_FUNCNAME( SETGETCOLORFOCUS )}, NULL },
{ "SETCBXCOLORFOCUS", {HB_FS_PUBLIC}, {HB_FUNCNAME( SETCBXCOLORFOCUS )}, NULL },
{ "SETMGETCOLORFOCUS", {HB_FS_PUBLIC}, {HB_FUNCNAME( SETMGETCOLORFOCUS )}, NULL },
{ "ISEXERUNNING", {HB_FS_PUBLIC}, {HB_FUNCNAME( ISEXERUNNING )}, NULL },
{ "CFILENAME", {HB_FS_PUBLIC}, {HB_FUNCNAME( CFILENAME )}, NULL },
{ "HB_ARGV", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_ARGV )}, NULL },
{ "MSGINFO", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGINFO )}, NULL },
{ "NEW", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TVARIABLE", {HB_FS_PUBLIC}, {HB_FUNCNAME( TVARIABLE )}, NULL },
{ "ADDVAR", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "LSUCESS", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "OPENSERVERMYSQL", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( OPENSERVERMYSQL )}, NULL },
{ "ACCESOTABLASMYSQL", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( ACCESOTABLASMYSQL )}, NULL },
{ "GETNHEIGHTITEM", {HB_FS_PUBLIC}, {HB_FUNCNAME( GETNHEIGHTITEM )}, NULL },
{ "GETNWIDTHITEM", {HB_FS_PUBLIC}, {HB_FUNCNAME( GETNWIDTHITEM )}, NULL },
{ "TFONT", {HB_FS_PUBLIC}, {HB_FUNCNAME( TFONT )}, NULL },
{ "TWINDOW", {HB_FS_PUBLIC}, {HB_FUNCNAME( TWINDOW )}, NULL },
{ "ABITMAPS", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "MSGYESNO", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGYESNO )}, NULL },
{ "END", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_BKEYDOWN", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "GETKEYSTATE", {HB_FS_PUBLIC}, {HB_FUNCNAME( GETKEYSTATE )}, NULL },
{ "EVAL", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TMSGBAR", {HB_FS_PUBLIC}, {HB_FUNCNAME( TMSGBAR )}, NULL },
{ "_OMSGITEM1", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TMSGITEM", {HB_FS_PUBLIC}, {HB_FUNCNAME( TMSGITEM )}, NULL },
{ "KEYBON", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "DATEON", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "CLOCKON", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_BMSG", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "OCLOCK", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "OMSGBAR", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "AMPM", {HB_FS_PUBLIC}, {HB_FUNCNAME( AMPM )}, NULL },
{ "TIME", {HB_FS_PUBLIC}, {HB_FUNCNAME( TIME )}, NULL },
{ "_NWIDTH", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "NWIDTH", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "ACTIVATE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BLCLICKED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BRCLICKED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BMOVED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BRESIZED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BPAINTED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BKEYDOWN", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_BINIT", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "CREATEBUTTONBAR", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( CREATEBUTTONBAR )}, NULL },
{ "_OWND", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "SETTITLE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BLBUTTONUP", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "HB_ISOBJECT", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_ISOBJECT )}, NULL },
{ "OAPP", {HB_FS_PUBLIC | HB_FS_LOCAL}, {HB_FUNCNAME( OAPP )}, NULL },
{ "ARRAY", {HB_FS_PUBLIC}, {HB_FUNCNAME( ARRAY )}, NULL },
{ "TBAR", {HB_FS_PUBLIC}, {HB_FUNCNAME( TBAR )}, NULL },
{ "NEWBAR", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TBTNBMP", {HB_FS_PUBLIC}, {HB_FUNCNAME( TBTNBMP )}, NULL },
{ "DO_EMPRESAS", {HB_FS_PUBLIC}, {HB_FUNCNAME( DO_EMPRESAS )}, NULL },
{ "POPUPTABLAS", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( POPUPTABLAS )}, NULL },
{ "SESSION", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "DO_ASIENTOS", {HB_FS_PUBLIC}, {HB_FUNCNAME( DO_ASIENTOS )}, NULL },
{ "POPUPREPORTE", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( POPUPREPORTE )}, NULL },
{ "POPUPDESIGNER", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( POPUPDESIGNER )}, NULL },
{ "OSERVER", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "ISKINDOF", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_OSERVER", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TMYSQLSERVER", {HB_FS_PUBLIC}, {HB_FUNCNAME( TMYSQLSERVER )}, NULL },
{ "CLOSESERVERMYSQL", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( CLOSESERVERMYSQL )}, NULL },
{ "MSGSTOP", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGSTOP )}, NULL },
{ "MSGPROGRESS", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGPROGRESS )}, NULL },
{ "CHECKTABLASMYSQL", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( CHECKTABLASMYSQL )}, NULL },
{ "CREAR_CATALOGO_MAESTRO", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_CATALOGO_MAESTRO )}, NULL },
{ "CREAR_EMPRESAS", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_EMPRESAS )}, NULL },
{ "CREAR_TIPOEMPRESA", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_TIPOEMPRESA )}, NULL },
{ "CREAR_CLIENTES", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_CLIENTES )}, NULL },
{ "CREAR_PROVEDORES", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_PROVEDORES )}, NULL },
{ "CREAR_CATALOGO", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_CATALOGO )}, NULL },
{ "CREAR_REGIONES", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_REGIONES )}, NULL },
{ "CREAR_CIUDADES", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_CIUDADES )}, NULL },
{ "CREAR_COMUNAS", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_COMUNAS )}, NULL },
{ "CREAR_TIPODOCUMENTO", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_TIPODOCUMENTO )}, NULL },
{ "CREAR_BALANCE", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_BALANCE )}, NULL },
{ "CREAR_ASIENTOMAESTRO", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_ASIENTOMAESTRO )}, NULL },
{ "CREAR_ASIENTOLINEAS", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_ASIENTOLINEAS )}, NULL },
{ "CREAR_TEMPLIBROMAYOR", {HB_FS_PUBLIC}, {HB_FUNCNAME( CREAR_TEMPLIBROMAYOR )}, NULL },
{ "MENUBEGIN", {HB_FS_PUBLIC}, {HB_FUNCNAME( MENUBEGIN )}, NULL },
{ "MENUADDITEM", {HB_FS_PUBLIC}, {HB_FUNCNAME( MENUADDITEM )}, NULL },
{ "CATALOGO_MAESTRO", {HB_FS_PUBLIC}, {HB_FUNCNAME( CATALOGO_MAESTRO )}, NULL },
{ "REGIONES", {HB_FS_PUBLIC}, {HB_FUNCNAME( REGIONES )}, NULL },
{ "CIUDADES", {HB_FS_PUBLIC}, {HB_FUNCNAME( CIUDADES )}, NULL },
{ "COMUNAS", {HB_FS_PUBLIC}, {HB_FUNCNAME( COMUNAS )}, NULL },
{ "TIPO_DOCUMENTO", {HB_FS_PUBLIC}, {HB_FUNCNAME( TIPO_DOCUMENTO )}, NULL },
{ "CATALOGO", {HB_FS_PUBLIC}, {HB_FUNCNAME( CATALOGO )}, NULL },
{ "CLIENTES", {HB_FS_PUBLIC}, {HB_FUNCNAME( CLIENTES )}, NULL },
{ "PROVEDORES", {HB_FS_PUBLIC}, {HB_FUNCNAME( PROVEDORES )}, NULL },
{ "MENUEND", {HB_FS_PUBLIC}, {HB_FUNCNAME( MENUEND )}, NULL },
{ "NHEIGHT", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "NTOP", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "NLEFT", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "OWND", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "FR3_CATALOGO", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_CATALOGO )}, NULL },
{ "FR3_CLIENTE", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_CLIENTE )}, NULL },
{ "FR3_PROVEDOR", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_PROVEDOR )}, NULL },
{ "FR3_LIBRODIARIOBORRADOR", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_LIBRODIARIOBORRADOR )}, NULL },
{ "FR3_LIBRODIARIOOFICIAL", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_LIBRODIARIOOFICIAL )}, NULL },
{ "FR3_LIBROMAYOR", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_LIBROMAYOR )}, NULL },
{ "FR3_LIBROBALANCE", {HB_FS_PUBLIC}, {HB_FUNCNAME( FR3_LIBROBALANCE )}, NULL },
{ "FW_GT", {HB_FS_PUBLIC}, {HB_FUNCNAME( FW_GT )}, NULL },
{ "ERRORSYS", {HB_FS_PUBLIC}, {HB_FUNCNAME( ERRORSYS )}, NULL },
{ "DBFFPT", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBFFPT )}, NULL },
{ "DBFCDX", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBFCDX )}, NULL },
{ "HB_LANG_ES", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_LANG_ES )}, NULL },
{ "HB_CODEPAGE_ESWIN", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_CODEPAGE_ESWIN )}, NULL },
{ "(_INITSTATICS00005)", {HB_FS_INITEXIT | HB_FS_LOCAL}, {hb_INITSTATICS}, NULL }
HB_INIT_SYMBOLS_EX_END( hb_vm_SymbolInit_MAIN, "C:\\contabilidad\\prg\\main.prg", 0x0, 0x0003 )

#if defined( HB_PRAGMA_STARTUP )
   #pragma startup hb_vm_SymbolInit_MAIN
#elif defined( HB_DATASEG_STARTUP )
   #define HB_DATASEG_BODY    HB_DATASEG_FUNC( hb_vm_SymbolInit_MAIN )
   #include "hbiniseg.h"
#endif

HB_FUNC( MAIN )
{
	static const HB_BYTE pcode[] =
	{
		13,5,0,116,126,0,36,54,0,9,80,5,36,57,
		0,176,1,0,20,0,36,59,0,176,2,0,92,5,
		93,158,7,20,2,36,60,0,176,3,0,106,3,79,
		78,0,20,1,36,61,0,176,2,0,92,4,106,11,
		68,68,47,77,77,47,89,89,89,89,0,20,2,36,
		62,0,176,2,0,92,116,106,9,72,72,58,77,77,
		58,83,83,0,20,2,36,64,0,176,2,0,92,11,
		106,3,79,78,0,20,2,36,65,0,176,4,0,120,
		20,1,36,66,0,176,5,0,120,20,1,36,67,0,
		176,6,0,120,20,1,36,68,0,176,7,0,120,20,
		1,36,70,0,176,2,0,122,106,3,79,78,0,20,
		2,36,71,0,176,2,0,92,11,106,3,79,78,0,
		20,2,36,75,0,176,8,0,106,6,101,115,95,69,
		83,0,20,1,36,77,0,176,9,0,106,3,69,83,
		0,20,1,36,78,0,176,10,0,106,6,69,83,87,
		73,78,0,20,1,36,80,0,176,11,0,106,7,68,
		66,70,67,68,88,0,20,1,36,82,0,176,12,0,
		9,20,1,36,84,0,176,13,0,106,2,69,0,120,
		20,2,36,86,0,176,14,0,9,20,1,36,88,0,
		176,15,0,120,20,1,36,90,0,176,16,0,97,0,
		255,255,0,20,1,36,91,0,176,17,0,97,0,255,
		255,0,20,1,36,92,0,176,18,0,97,0,255,255,
		0,20,1,36,94,0,176,19,0,176,20,0,176,21,
		0,121,12,1,12,1,12,1,28,68,36,95,0,176,
		22,0,176,20,0,176,21,0,121,12,1,12,1,106,
		23,32,121,97,32,115,101,32,101,115,116,97,32,69,
		106,101,99,117,116,97,110,100,111,0,72,106,13,65,
		118,99,32,83,105,115,116,101,109,97,115,0,20,2,
		36,96,0,100,110,7,36,100,0,48,23,0,176,24,
		0,12,0,112,0,82,3,0,36,102,0,48,25,0,
		103,3,0,106,5,111,66,114,119,0,100,112,2,73,
		36,103,0,48,25,0,103,3,0,106,6,98,69,120,
		105,116,0,100,112,2,73,36,104,0,48,25,0,103,
		3,0,106,10,111,109,115,103,73,116,101,109,49,0,
		100,112,2,73,36,105,0,48,25,0,103,3,0,106,
		10,111,77,115,103,73,116,101,109,50,0,100,112,2,
		73,36,107,0,48,26,0,103,3,0,112,0,31,8,
		36,108,0,100,110,7,36,111,0,176,27,0,20,0,
		36,112,0,176,28,0,20,0,36,114,0,176,29,0,
		101,0,0,0,0,0,0,252,63,10,2,20,1,36,
		115,0,176,30,0,101,0,0,0,0,0,0,248,63,
		10,2,20,1,36,117,0,48,23,0,176,31,0,12,
		0,106,7,84,65,72,79,77,65,0,121,92,244,100,
		120,100,100,100,100,100,100,100,100,100,100,100,100,100,
		112,18,80,3,36,118,0,48,23,0,176,31,0,12,
		0,106,7,84,65,72,79,77,65,0,121,92,245,100,
		120,100,100,100,100,100,100,100,100,100,100,100,100,100,
		112,18,82,4,0,36,119,0,48,23,0,176,31,0,
		12,0,106,7,84,65,72,79,77,65,0,121,92,245,
		100,120,100,100,100,100,100,100,100,100,100,100,100,100,
		100,112,18,82,5,0,36,121,0,48,23,0,176,32,
		0,12,0,100,100,100,100,100,100,100,100,48,33,0,
		103,3,0,112,0,92,12,1,100,100,100,100,100,100,
		100,120,120,120,120,9,100,106,5,111,87,110,100,0,
		100,100,112,25,80,1,36,129,0,89,76,0,0,0,
		2,0,5,0,1,0,36,124,0,176,34,0,106,18,
		83,97,108,105,114,32,100,101,108,32,83,105,115,116,
		101,109,97,0,106,11,83,101,108,101,99,99,105,111,
		110,101,0,12,2,28,19,36,125,0,120,80,255,36,
		126,0,48,35,0,95,254,112,0,73,36,128,0,100,
		6,80,4,36,131,0,48,36,0,95,1,89,29,0,
		1,0,1,0,4,0,176,37,0,92,27,12,1,28,
		11,48,38,0,95,255,112,0,25,3,100,6,112,1,
		73,36,134,0,48,23,0,176,39,0,12,0,95,1,
		100,120,9,9,9,100,100,95,3,100,9,9,100,120,
		112,14,80,2,36,136,0,48,40,0,103,3,0,48,
		23,0,176,41,0,12,0,95,2,106,1,0,93,132,
		3,95,3,100,100,120,100,100,100,100,112,11,112,1,
		73,36,138,0,48,42,0,95,2,112,0,73,36,139,
		0,48,43,0,95,2,112,0,73,36,140,0,48,44,
		0,95,2,112,0,73,36,141,0,48,45,0,48,46,
		0,48,47,0,95,1,112,0,112,0,90,13,176,48,
		0,176,49,0,12,0,12,1,6,112,1,73,36,142,
		0,48,50,0,48,46,0,48,47,0,95,1,112,0,
		112,0,21,48,51,0,163,0,112,0,92,20,72,112,
		1,73,36,148,0,48,52,0,95,1,106,10,77,65,
		88,73,77,73,90,69,68,0,48,53,0,95,1,112,
		0,48,54,0,95,1,112,0,48,55,0,95,1,112,
		0,48,56,0,95,1,112,0,48,57,0,95,1,112,
		0,48,58,0,95,1,112,0,48,59,0,95,1,89,
		40,0,1,0,2,0,1,0,4,0,176,60,0,95,
		255,95,254,20,2,48,61,0,103,3,0,95,255,112,
		1,73,48,62,0,103,3,0,112,0,6,112,1,100,
		100,100,100,100,100,100,100,89,12,0,0,0,1,0,
		5,0,95,255,6,100,48,63,0,95,1,112,0,9,
		112,20,73,36,150,0,176,64,0,95,3,12,1,28,
		10,48,35,0,95,3,112,0,73,100,80,3,36,151,
		0,176,64,0,103,4,0,12,1,28,11,48,35,0,
		103,4,0,112,0,73,100,82,4,0,36,152,0,176,
		64,0,103,5,0,12,1,28,11,48,35,0,103,5,
		0,112,0,73,100,82,5,0,36,154,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC( OAPP )
{
	static const HB_BYTE pcode[] =
	{
		116,126,0,36,159,0,103,3,0,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( CREATEBUTTONBAR )
{
	static const HB_BYTE pcode[] =
	{
		13,2,2,116,126,0,36,165,0,176,66,0,92,7,
		12,1,80,4,36,167,0,48,23,0,176,67,0,12,
		0,95,1,92,70,92,70,9,100,100,9,9,9,100,
		100,100,100,100,120,100,9,9,112,18,80,3,36,174,
		0,48,68,0,176,69,0,12,0,48,33,0,103,3,
		0,112,0,92,7,1,100,100,100,100,89,13,0,1,
		0,0,0,176,70,0,12,0,6,9,95,3,9,100,
		106,8,69,109,112,114,101,115,97,0,9,100,106,14,
		68,111,95,69,109,112,114,101,115,97,115,40,41,0,
		100,106,8,69,109,112,114,101,115,97,0,103,4,0,
		100,100,100,100,100,100,100,9,100,100,100,112,28,95,
		4,122,2,36,183,0,48,68,0,176,69,0,12,0,
		48,33,0,103,3,0,112,0,92,27,1,100,100,100,
		100,89,20,0,1,0,1,0,4,0,176,71,0,95,
		255,92,2,1,12,1,6,120,95,3,9,90,21,48,
		72,0,103,3,0,112,0,106,7,97,99,116,105,118,
		97,0,1,6,106,9,67,97,116,97,108,111,103,111,
		0,9,100,106,23,80,111,112,117,112,84,97,98,108,
		97,115,40,32,111,66,116,110,91,50,93,32,41,0,
		100,106,7,84,97,98,108,97,115,0,103,4,0,100,
		100,100,100,100,100,100,9,100,100,100,112,28,95,4,
		92,2,2,36,192,0,48,68,0,176,69,0,12,0,
		48,33,0,103,3,0,112,0,92,18,1,100,100,100,
		100,89,13,0,1,0,0,0,176,73,0,12,0,6,
		120,95,3,9,90,21,48,72,0,103,3,0,112,0,
		106,7,97,99,116,105,118,97,0,1,6,106,9,65,
		115,105,101,110,116,111,115,0,9,100,106,14,68,111,
		95,65,115,105,101,110,116,111,115,40,41,0,100,106,
		9,65,115,105,101,110,116,111,115,0,103,4,0,100,
		100,100,100,100,100,100,9,100,100,100,112,28,95,4,
		92,3,2,36,201,0,48,68,0,176,69,0,12,0,
		48,33,0,103,3,0,112,0,92,17,1,100,100,100,
		100,89,20,0,1,0,1,0,4,0,176,74,0,95,
		255,92,4,1,12,1,6,120,95,3,9,90,21,48,
		72,0,103,3,0,112,0,106,7,97,99,116,105,118,
		97,0,1,6,106,9,82,101,112,111,114,116,101,115,
		0,9,100,106,24,80,111,112,117,112,82,101,112,111,
		114,116,101,40,32,111,66,116,110,91,52,93,32,41,
		0,100,106,9,82,101,112,111,114,116,101,115,0,103,
		4,0,100,100,100,100,100,100,100,9,100,100,100,112,
		28,95,4,92,4,2,36,210,0,48,68,0,176,69,
		0,12,0,48,33,0,103,3,0,112,0,92,54,1,
		100,100,100,100,89,20,0,1,0,1,0,4,0,176,
		75,0,95,255,92,5,1,12,1,6,120,95,3,9,
		90,21,48,72,0,103,3,0,112,0,106,7,97,99,
		116,105,118,97,0,1,6,106,10,68,105,115,101,241,
		97,100,111,114,0,9,100,106,25,80,111,112,117,112,
		68,101,115,105,103,110,101,114,40,32,111,66,116,110,
		91,53,93,32,41,0,100,106,10,68,105,115,101,241,
		97,100,111,114,0,103,4,0,100,100,100,100,100,100,
		100,9,100,100,100,112,28,95,4,92,5,2,36,217,
		0,48,68,0,176,69,0,12,0,48,33,0,103,3,
		0,112,0,92,5,1,100,100,100,100,89,17,0,1,
		0,1,0,2,0,48,38,0,95,255,112,0,6,120,
		95,3,9,100,100,9,100,106,12,69,118,97,108,40,
		98,69,120,105,116,41,0,100,106,6,83,97,108,105,
		114,0,103,4,0,100,100,100,100,100,100,100,9,100,
		100,100,112,28,95,4,92,6,2,36,219,0,100,110,
		7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( OPENSERVERMYSQL )
{
	static const HB_BYTE pcode[] =
	{
		116,126,0,36,225,0,176,64,0,48,76,0,103,3,
		0,112,0,12,1,28,57,36,226,0,48,77,0,48,
		76,0,103,3,0,112,0,106,13,84,77,89,83,81,
		76,83,69,82,86,69,82,0,112,1,31,24,36,227,
		0,48,78,0,103,3,0,48,23,0,176,79,0,12,
		0,112,0,112,1,73,36,231,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( CLOSESERVERMYSQL )
{
	static const HB_BYTE pcode[] =
	{
		116,126,0,36,237,0,176,64,0,48,76,0,103,3,
		0,112,0,12,1,28,120,36,238,0,48,77,0,48,
		76,0,103,3,0,112,0,106,13,84,77,89,83,81,
		76,83,69,82,86,69,82,0,112,1,28,21,36,239,
		0,48,35,0,48,76,0,103,3,0,112,0,112,0,
		73,25,68,36,241,0,176,81,0,106,42,78,111,32,
		115,101,32,112,117,100,111,32,99,101,114,114,97,32,
		108,97,32,99,111,110,101,120,105,111,110,32,77,89,
		83,81,76,32,83,101,114,118,101,114,0,106,12,69,
		114,114,111,114,32,77,89,83,81,76,0,20,2,36,
		245,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( ACCESOTABLASMYSQL )
{
	static const HB_BYTE pcode[] =
	{
		36,253,0,176,82,0,106,19,86,101,114,105,102,105,
		99,97,110,100,111,32,84,97,98,108,97,115,0,106,
		21,69,115,112,101,114,101,32,112,111,114,32,102,97,
		118,111,114,32,46,46,46,0,90,8,176,83,0,12,
		0,6,20,3,36,255,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( CHECKTABLASMYSQL )
{
	static const HB_BYTE pcode[] =
	{
		36,5,1,176,84,0,20,0,36,6,1,176,85,0,
		20,0,36,7,1,176,86,0,20,0,36,8,1,176,
		87,0,20,0,36,9,1,176,88,0,20,0,36,10,
		1,176,89,0,20,0,36,11,1,176,90,0,20,0,
		36,12,1,176,91,0,20,0,36,13,1,176,92,0,
		20,0,36,14,1,176,93,0,20,0,36,15,1,176,
		94,0,20,0,36,16,1,176,95,0,20,0,36,17,
		1,176,96,0,20,0,36,19,1,176,97,0,20,0,
		36,21,1,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( POPUPTABLAS )
{
	static const HB_BYTE pcode[] =
	{
		13,1,1,116,126,0,36,28,1,176,98,0,120,100,
		100,9,9,100,100,100,100,100,100,100,100,100,9,103,
		5,0,9,9,9,120,100,100,100,100,100,101,0,0,
		0,0,0,0,0,64,255,1,100,9,100,9,9,100,
		100,100,100,100,100,100,100,100,9,100,100,100,100,100,
		100,100,12,48,80,2,36,30,1,176,99,0,106,17,
		67,97,116,97,108,111,103,111,32,77,97,101,115,116,
		114,111,0,100,9,100,89,13,0,1,0,0,0,176,
		100,0,12,0,6,100,100,100,100,100,100,9,100,90,
		21,48,72,0,103,3,0,112,0,106,7,97,99,116,
		105,118,97,0,1,6,9,100,9,9,100,100,100,100,
		100,100,100,100,100,9,9,100,100,100,100,100,100,100,
		100,9,9,9,100,100,100,100,9,9,9,20,47,36,
		31,1,176,99,0,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,100,100,100,120,100,100,100,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,20,35,
		36,32,1,176,99,0,106,9,82,101,103,105,111,110,
		101,115,0,100,9,100,89,13,0,1,0,0,0,176,
		101,0,12,0,6,100,100,100,100,100,100,9,100,90,
		21,48,72,0,103,3,0,112,0,106,7,97,99,116,
		105,118,97,0,1,6,9,100,9,9,100,100,100,100,
		100,100,100,100,100,9,9,100,100,100,100,100,100,100,
		100,9,9,9,100,100,100,100,9,9,9,20,47,36,
		33,1,176,99,0,106,9,67,105,117,100,97,100,101,
		115,0,100,9,100,89,13,0,1,0,0,0,176,102,
		0,12,0,6,100,100,100,100,100,100,9,100,90,21,
		48,72,0,103,3,0,112,0,106,7,97,99,116,105,
		118,97,0,1,6,9,100,9,9,100,100,100,100,100,
		100,100,100,100,9,9,100,100,100,100,100,100,100,100,
		9,9,9,100,100,100,100,9,9,9,20,47,36,34,
		1,176,99,0,106,8,67,111,109,117,110,97,115,0,
		100,9,100,89,13,0,1,0,0,0,176,103,0,12,
		0,6,100,100,100,100,100,100,9,100,90,21,48,72,
		0,103,3,0,112,0,106,7,97,99,116,105,118,97,
		0,1,6,9,100,9,9,100,100,100,100,100,100,100,
		100,100,9,9,100,100,100,100,100,100,100,100,9,9,
		9,100,100,100,100,9,9,9,20,47,36,35,1,176,
		99,0,106,15,84,105,112,111,32,68,111,99,117,109,
		101,110,116,111,0,100,9,100,89,13,0,1,0,0,
		0,176,104,0,12,0,6,100,100,100,100,100,100,9,
		100,90,21,48,72,0,103,3,0,112,0,106,7,97,
		99,116,105,118,97,0,1,6,9,100,9,9,100,100,
		100,100,100,100,100,100,100,9,9,100,100,100,100,100,
		100,100,100,9,9,9,100,100,100,100,9,9,9,20,
		47,36,36,1,176,99,0,100,100,100,100,100,100,100,
		100,100,100,100,100,100,100,100,100,100,120,100,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		20,35,36,37,1,176,99,0,106,9,67,97,116,97,
		108,111,103,111,0,100,9,100,89,13,0,1,0,0,
		0,176,105,0,12,0,6,100,100,100,100,100,100,9,
		100,90,21,48,72,0,103,3,0,112,0,106,7,97,
		99,116,105,118,97,0,1,6,9,100,9,9,100,100,
		100,100,100,100,100,100,100,9,9,100,100,100,100,100,
		100,100,100,9,9,9,100,100,100,100,9,9,9,20,
		47,36,38,1,176,99,0,100,100,100,100,100,100,100,
		100,100,100,100,100,100,100,100,100,100,120,100,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		20,35,36,39,1,176,99,0,106,9,67,108,105,101,
		110,116,101,115,0,100,9,100,89,13,0,1,0,0,
		0,176,106,0,12,0,6,100,100,100,100,100,100,9,
		100,90,21,48,72,0,103,3,0,112,0,106,7,97,
		99,116,105,118,97,0,1,6,9,100,9,9,100,100,
		100,100,100,100,100,100,100,9,9,100,100,100,100,100,
		100,100,100,9,9,9,100,100,100,100,9,9,9,20,
		47,36,40,1,176,99,0,106,11,80,114,111,118,101,
		100,111,114,101,115,0,100,9,100,89,13,0,1,0,
		0,0,176,107,0,12,0,6,100,100,100,100,100,100,
		9,100,90,21,48,72,0,103,3,0,112,0,106,7,
		97,99,116,105,118,97,0,1,6,9,100,9,9,100,
		100,100,100,100,100,100,100,100,9,9,100,100,100,100,
		100,100,100,100,9,9,9,100,100,100,100,9,9,9,
		20,47,36,42,1,176,108,0,20,0,36,44,1,48,
		52,0,95,2,48,109,0,95,1,112,0,48,110,0,
		95,1,112,0,72,48,111,0,95,1,112,0,48,112,
		0,95,1,112,0,120,100,112,5,73,36,46,1,95,
		2,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( POPUPREPORTE )
{
	static const HB_BYTE pcode[] =
	{
		13,1,1,116,126,0,36,53,1,176,98,0,120,100,
		100,9,9,100,100,100,100,100,100,100,100,100,9,103,
		5,0,9,9,9,120,100,100,100,100,100,101,0,0,
		0,0,0,0,0,64,255,1,100,9,100,9,9,100,
		100,100,100,100,100,100,100,100,9,100,100,100,100,100,
		100,100,12,48,80,2,36,55,1,176,99,0,106,9,
		67,97,116,97,108,111,103,111,0,100,9,100,89,14,
		0,1,0,0,0,176,113,0,9,12,1,6,100,100,
		100,100,100,100,9,100,90,21,48,72,0,103,3,0,
		112,0,106,7,97,99,116,105,118,97,0,1,6,9,
		100,9,9,100,100,100,100,100,100,100,100,100,9,9,
		100,100,100,100,100,100,100,100,9,9,9,100,100,100,
		100,9,9,9,20,47,36,56,1,176,99,0,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,120,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,20,35,36,57,1,176,99,0,106,
		9,67,108,105,101,110,116,101,115,0,100,9,100,89,
		14,0,1,0,0,0,176,114,0,9,12,1,6,100,
		100,100,100,100,100,9,100,90,21,48,72,0,103,3,
		0,112,0,106,7,97,99,116,105,118,97,0,1,6,
		9,100,9,9,100,100,100,100,100,100,100,100,100,9,
		9,100,100,100,100,100,100,100,100,9,9,9,100,100,
		100,100,9,9,9,20,47,36,58,1,176,99,0,106,
		9,80,114,111,118,101,100,111,114,0,100,9,100,89,
		14,0,1,0,0,0,176,115,0,9,12,1,6,100,
		100,100,100,100,100,9,100,90,21,48,72,0,103,3,
		0,112,0,106,7,97,99,116,105,118,97,0,1,6,
		9,100,9,9,100,100,100,100,100,100,100,100,100,9,
		9,100,100,100,100,100,100,100,100,9,9,9,100,100,
		100,100,9,9,9,20,47,36,59,1,176,99,0,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,120,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,100,20,35,36,60,1,176,99,0,
		106,22,76,105,98,114,111,32,68,105,97,114,105,111,
		32,66,111,114,114,97,100,111,114,0,100,9,100,89,
		14,0,1,0,0,0,176,116,0,9,12,1,6,100,
		100,100,100,100,100,9,100,90,21,48,72,0,103,3,
		0,112,0,106,7,97,99,116,105,118,97,0,1,6,
		9,100,9,9,100,100,100,100,100,100,100,100,100,9,
		9,100,100,100,100,100,100,100,100,9,9,9,100,100,
		100,100,9,9,9,20,47,36,61,1,176,99,0,106,
		21,76,105,98,114,111,32,68,105,97,114,105,111,32,
		79,102,105,99,105,97,108,0,100,9,100,89,14,0,
		1,0,0,0,176,117,0,9,12,1,6,100,100,100,
		100,100,100,9,100,90,21,48,72,0,103,3,0,112,
		0,106,7,97,99,116,105,118,97,0,1,6,9,100,
		9,9,100,100,100,100,100,100,100,100,100,9,9,100,
		100,100,100,100,100,100,100,9,9,9,100,100,100,100,
		9,9,9,20,47,36,62,1,176,99,0,100,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		120,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,20,35,36,63,1,176,99,0,106,12,
		76,105,98,114,111,32,77,97,121,111,114,0,100,9,
		100,89,22,0,1,0,0,0,176,118,0,106,6,84,
		79,68,65,83,0,9,12,2,6,100,100,100,100,100,
		100,9,100,90,21,48,72,0,103,3,0,112,0,106,
		7,97,99,116,105,118,97,0,1,6,9,100,9,9,
		100,100,100,100,100,100,100,100,100,9,9,100,100,100,
		100,100,100,100,100,9,9,9,100,100,100,100,9,9,
		9,20,47,36,64,1,176,99,0,106,19,76,105,98,
		114,111,32,77,97,121,111,114,32,67,117,101,110,116,
		97,0,100,9,100,89,23,0,1,0,0,0,176,118,
		0,106,7,67,85,69,78,84,65,0,9,12,2,6,
		100,100,100,100,100,100,9,100,90,21,48,72,0,103,
		3,0,112,0,106,7,97,99,116,105,118,97,0,1,
		6,9,100,9,9,100,100,100,100,100,100,100,100,100,
		9,9,100,100,100,100,100,100,100,100,9,9,9,100,
		100,100,100,9,9,9,20,47,36,65,1,176,99,0,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,120,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,100,100,20,35,36,66,1,176,99,
		0,106,8,66,97,108,97,110,99,101,0,100,9,100,
		89,14,0,1,0,0,0,176,119,0,9,12,1,6,
		100,100,100,100,100,100,9,100,90,21,48,72,0,103,
		3,0,112,0,106,7,97,99,116,105,118,97,0,1,
		6,9,100,9,9,100,100,100,100,100,100,100,100,100,
		9,9,100,100,100,100,100,100,100,100,9,9,9,100,
		100,100,100,9,9,9,20,47,36,68,1,176,108,0,
		20,0,36,70,1,48,52,0,95,2,48,109,0,95,
		1,112,0,48,110,0,95,1,112,0,72,48,111,0,
		95,1,112,0,48,112,0,95,1,112,0,120,100,112,
		5,73,36,72,1,95,2,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( POPUPDESIGNER )
{
	static const HB_BYTE pcode[] =
	{
		13,1,1,116,126,0,36,79,1,176,98,0,120,100,
		100,9,9,100,100,100,100,100,100,100,100,100,9,103,
		5,0,9,9,9,120,100,100,100,100,100,101,0,0,
		0,0,0,0,0,64,255,1,100,9,100,9,9,100,
		100,100,100,100,100,100,100,100,9,100,100,100,100,100,
		100,100,12,48,80,2,36,81,1,176,99,0,106,9,
		67,97,116,97,108,111,103,111,0,100,9,100,89,14,
		0,1,0,0,0,176,113,0,120,12,1,6,100,100,
		100,100,100,100,9,100,90,21,48,72,0,103,3,0,
		112,0,106,7,97,99,116,105,118,97,0,1,6,9,
		100,9,9,100,100,100,100,100,100,100,100,100,9,9,
		100,100,100,100,100,100,100,100,9,9,9,100,100,100,
		100,9,9,9,20,47,36,82,1,176,99,0,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,120,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,20,35,36,83,1,176,99,0,106,
		9,67,108,105,101,110,116,101,115,0,100,9,100,89,
		14,0,1,0,0,0,176,114,0,120,12,1,6,100,
		100,100,100,100,100,9,100,90,21,48,72,0,103,3,
		0,112,0,106,7,97,99,116,105,118,97,0,1,6,
		9,100,9,9,100,100,100,100,100,100,100,100,100,9,
		9,100,100,100,100,100,100,100,100,9,9,9,100,100,
		100,100,9,9,9,20,47,36,84,1,176,99,0,106,
		9,80,114,111,118,101,100,111,114,0,100,9,100,89,
		14,0,1,0,0,0,176,115,0,120,12,1,6,100,
		100,100,100,100,100,9,100,90,21,48,72,0,103,3,
		0,112,0,106,7,97,99,116,105,118,97,0,1,6,
		9,100,9,9,100,100,100,100,100,100,100,100,100,9,
		9,100,100,100,100,100,100,100,100,9,9,9,100,100,
		100,100,9,9,9,20,47,36,85,1,176,99,0,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,120,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,100,20,35,36,86,1,176,99,0,
		106,22,76,105,98,114,111,32,68,105,97,114,105,111,
		32,66,111,114,114,97,100,111,114,0,100,9,100,89,
		14,0,1,0,0,0,176,116,0,120,12,1,6,100,
		100,100,100,100,100,9,100,90,21,48,72,0,103,3,
		0,112,0,106,7,97,99,116,105,118,97,0,1,6,
		9,100,9,9,100,100,100,100,100,100,100,100,100,9,
		9,100,100,100,100,100,100,100,100,9,9,9,100,100,
		100,100,9,9,9,20,47,36,87,1,176,99,0,106,
		21,76,105,98,114,111,32,68,105,97,114,105,111,32,
		79,102,105,99,105,97,108,0,100,9,100,89,14,0,
		1,0,0,0,176,117,0,120,12,1,6,100,100,100,
		100,100,100,9,100,90,21,48,72,0,103,3,0,112,
		0,106,7,97,99,116,105,118,97,0,1,6,9,100,
		9,9,100,100,100,100,100,100,100,100,100,9,9,100,
		100,100,100,100,100,100,100,9,9,9,100,100,100,100,
		9,9,9,20,47,36,88,1,176,99,0,100,100,100,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		120,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,20,35,36,89,1,176,99,0,106,12,
		76,105,98,114,111,32,77,97,121,111,114,0,100,9,
		100,89,22,0,1,0,0,0,176,118,0,106,6,84,
		79,68,65,83,0,120,12,2,6,100,100,100,100,100,
		100,9,100,90,21,48,72,0,103,3,0,112,0,106,
		7,97,99,116,105,118,97,0,1,6,9,100,9,9,
		100,100,100,100,100,100,100,100,100,9,9,100,100,100,
		100,100,100,100,100,9,9,9,100,100,100,100,9,9,
		9,20,47,36,90,1,176,99,0,106,19,76,105,98,
		114,111,32,77,97,121,111,114,32,67,117,101,110,116,
		97,0,100,9,100,89,23,0,1,0,0,0,176,118,
		0,106,7,67,85,69,78,84,65,0,120,12,2,6,
		100,100,100,100,100,100,9,100,90,21,48,72,0,103,
		3,0,112,0,106,7,97,99,116,105,118,97,0,1,
		6,9,100,9,9,100,100,100,100,100,100,100,100,100,
		9,9,100,100,100,100,100,100,100,100,9,9,9,100,
		100,100,100,9,9,9,20,47,36,91,1,176,99,0,
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,
		100,100,100,120,100,100,100,100,100,100,100,100,100,100,
		100,100,100,100,100,100,100,20,35,36,92,1,176,99,
		0,106,8,66,97,108,97,110,99,101,0,100,9,100,
		89,14,0,1,0,0,0,176,119,0,120,12,1,6,
		100,100,100,100,100,100,9,100,90,21,48,72,0,103,
		3,0,112,0,106,7,97,99,116,105,118,97,0,1,
		6,9,100,9,9,100,100,100,100,100,100,100,100,100,
		9,9,100,100,100,100,100,100,100,100,9,9,9,100,
		100,100,100,9,9,9,20,47,36,94,1,176,108,0,
		20,0,36,96,1,48,52,0,95,2,48,109,0,95,
		1,112,0,48,110,0,95,1,112,0,72,48,111,0,
		95,1,112,0,48,112,0,95,1,112,0,120,100,112,
		5,73,36,98,1,95,2,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_INITSTATICS()
{
	static const HB_BYTE pcode[] =
	{
		117,126,0,5,0,116,126,0,4,0,0,82,1,0,
		100,82,2,0,7
	};

	hb_vmExecute( pcode, symbols );
}

