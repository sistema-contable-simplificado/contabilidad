/*
 * Harbour 3.2.0dev (r1801051438)
 * Borland/Embarcadero C++ 7.0 (32-bit)
 * Generated C source from "C:\contabilidad\prg\regiones.prg"
 */

#include "hbvmpub.h"
#include "hbinit.h"


HB_FUNC( REGIONES );
HB_FUNC_EXTERN( STRFORMAT );
HB_FUNC_EXTERN( OAPP );
HB_FUNC_EXTERN( TFONT );
HB_FUNC_EXTERN( TDIALOG );
HB_FUNC_EXTERN( XBROWSENEW );
HB_FUNC_EXTERN( QUERYSEEKWILD );
HB_FUNC_EXTERN( TSAY );
HB_FUNC_EXTERN( MSGYESNO );
HB_FUNC_EXTERN( DISABLESYSMENUDLG );
HB_FUNC_STATIC( CREABUTONBAR );
HB_FUNC_EXTERN( HB_ISOBJECT );
HB_FUNC_EXTERN( TBAR );
HB_FUNC_EXTERN( TBTNBMP );
HB_FUNC_STATIC( LEADATOS );
HB_FUNC_STATIC( DELRECORD );
HB_FUNC_EXTERN( ARRAY );
HB_FUNC_STATIC( LASTIDREGION );
HB_FUNC_EXTERN( SPACE );
HB_FUNC_EXTERN( TGROUP );
HB_FUNC_EXTERN( TGET );
HB_FUNC_EXTERN( PCOUNT );
HB_FUNC_EXTERN( BTNGRADGREEN );
HB_FUNC_EXTERN( BTNGRADRED );
HB_FUNC_EXTERN( MSGPROGRESS );
HB_FUNC_STATIC( NEWRECORD );
HB_FUNC_STATIC( MODRECORD );
HB_FUNC_EXTERN( CLIPVALUE2SQL );
HB_FUNC_EXTERN( MSGSTOP );
HB_FUNC( SIIDREGION );
HB_FUNC( GETARRAYREGIONES );
HB_FUNC_EXTERN( AADD );
HB_FUNC_EXTERN( LEN );
HB_FUNC( GETNAMEREGION );
HB_FUNC( CREAR_REGIONES );
HB_FUNC_EXTERN( FW_GT );
HB_FUNC_EXTERN( ERRORSYS );
HB_FUNC_INITSTATICS();


HB_INIT_SYMBOLS_BEGIN( hb_vm_SymbolInit_REGIONES )
{ "REGIONES", {HB_FS_PUBLIC | HB_FS_FIRST | HB_FS_LOCAL}, {HB_FUNCNAME( REGIONES )}, NULL },
{ "ID_REGION", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "END", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "STRFORMAT", {HB_FS_PUBLIC}, {HB_FUNCNAME( STRFORMAT )}, NULL },
{ "REGIONES", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "OAPP", {HB_FS_PUBLIC}, {HB_FUNCNAME( OAPP )}, NULL },
{ "CREATEQUERY", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "OSERVER", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "NEW", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TFONT", {HB_FS_PUBLIC}, {HB_FUNCNAME( TFONT )}, NULL },
{ "TDIALOG", {HB_FS_PUBLIC}, {HB_FUNCNAME( TDIALOG )}, NULL },
{ "ABITMAPS", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "XBROWSENEW", {HB_FS_PUBLIC}, {HB_FUNCNAME( XBROWSENEW )}, NULL },
{ "MYCONFIG", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_BKEYDOWN", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "EVAL", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_BKEYCHAR", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "QUERYSEEKWILD", {HB_FS_PUBLIC}, {HB_FUNCNAME( QUERYSEEKWILD )}, NULL },
{ "_BCHANGE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BCLRHEADERFOOTER", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "_BLDBLCLICK", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "REDEFINE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TSAY", {HB_FS_PUBLIC}, {HB_FUNCNAME( TSAY )}, NULL },
{ "_OSEEK", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "CSEEK", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "ACTIVATE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BLCLICKED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BMOVED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "BPAINTED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "MSGYESNO", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGYESNO )}, NULL },
{ "VERSION", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "DISABLESYSMENUDLG", {HB_FS_PUBLIC}, {HB_FUNCNAME( DISABLESYSMENUDLG )}, NULL },
{ "CREABUTONBAR", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( CREABUTONBAR )}, NULL },
{ "BRCLICKED", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "HB_ISOBJECT", {HB_FS_PUBLIC}, {HB_FUNCNAME( HB_ISOBJECT )}, NULL },
{ "TBAR", {HB_FS_PUBLIC}, {HB_FUNCNAME( TBAR )}, NULL },
{ "NEWBAR", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TBTNBMP", {HB_FS_PUBLIC}, {HB_FUNCNAME( TBTNBMP )}, NULL },
{ "LEADATOS", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( LEADATOS )}, NULL },
{ "REFRESH", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "SETFOCUS", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "DELRECORD", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( DELRECORD )}, NULL },
{ "GOTOP", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "GOBOTTOM", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "REPORT", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "ARRAY", {HB_FS_PUBLIC}, {HB_FUNCNAME( ARRAY )}, NULL },
{ "LASTIDREGION", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( LASTIDREGION )}, NULL },
{ "SPACE", {HB_FS_PUBLIC}, {HB_FUNCNAME( SPACE )}, NULL },
{ "NOMBRE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "TGROUP", {HB_FS_PUBLIC}, {HB_FUNCNAME( TGROUP )}, NULL },
{ "TGET", {HB_FS_PUBLIC}, {HB_FUNCNAME( TGET )}, NULL },
{ "PCOUNT", {HB_FS_PUBLIC}, {HB_FUNCNAME( PCOUNT )}, NULL },
{ "BTNGRADGREEN", {HB_FS_PUBLIC}, {HB_FUNCNAME( BTNGRADGREEN )}, NULL },
{ "BTNGRADRED", {HB_FS_PUBLIC}, {HB_FUNCNAME( BTNGRADRED )}, NULL },
{ "_LCANCEL", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "MSGPROGRESS", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGPROGRESS )}, NULL },
{ "NEWRECORD", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( NEWRECORD )}, NULL },
{ "MODRECORD", {HB_FS_STATIC | HB_FS_LOCAL}, {HB_FUNCNAME( MODRECORD )}, NULL },
{ "INSERTHASH", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "REQUERY", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "UPDATEHASH", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "CLIPVALUE2SQL", {HB_FS_PUBLIC}, {HB_FUNCNAME( CLIPVALUE2SQL )}, NULL },
{ "MY_RECNO", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "RECCOUNT", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "DELETE2", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "MSGSTOP", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSGSTOP )}, NULL },
{ "SIIDREGION", {HB_FS_PUBLIC | HB_FS_LOCAL}, {HB_FUNCNAME( SIIDREGION )}, NULL },
{ "GETARRAYREGIONES", {HB_FS_PUBLIC | HB_FS_LOCAL}, {HB_FUNCNAME( GETARRAYREGIONES )}, NULL },
{ "EOF", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "AADD", {HB_FS_PUBLIC}, {HB_FUNCNAME( AADD )}, NULL },
{ "SKIP", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "LEN", {HB_FS_PUBLIC}, {HB_FUNCNAME( LEN )}, NULL },
{ "GETNAMEREGION", {HB_FS_PUBLIC | HB_FS_LOCAL}, {HB_FUNCNAME( GETNAMEREGION )}, NULL },
{ "CREAR_REGIONES", {HB_FS_PUBLIC | HB_FS_LOCAL}, {HB_FUNCNAME( CREAR_REGIONES )}, NULL },
{ "VERIFYTABLE", {HB_FS_PUBLIC | HB_FS_MESSAGE}, {NULL}, NULL },
{ "FW_GT", {HB_FS_PUBLIC}, {HB_FUNCNAME( FW_GT )}, NULL },
{ "ERRORSYS", {HB_FS_PUBLIC}, {HB_FUNCNAME( ERRORSYS )}, NULL },
{ "(_INITSTATICS00002)", {HB_FS_INITEXIT | HB_FS_LOCAL}, {hb_INITSTATICS}, NULL }
HB_INIT_SYMBOLS_EX_END( hb_vm_SymbolInit_REGIONES, "C:\\contabilidad\\prg\\regiones.prg", 0x0, 0x0003 )

#if defined( HB_PRAGMA_STARTUP )
   #pragma startup hb_vm_SymbolInit_REGIONES
#elif defined( HB_DATASEG_STARTUP )
   #define HB_DATASEG_BODY    HB_DATASEG_FUNC( hb_vm_SymbolInit_REGIONES )
   #include "hbiniseg.h"
#endif

HB_FUNC( REGIONES )
{
	static const HB_BYTE pcode[] =
	{
		13,11,1,36,30,0,177,0,0,80,11,36,31,0,
		9,80,12,36,33,0,121,95,11,106,10,105,100,95,
		114,101,103,105,111,110,0,2,36,35,0,95,1,100,
		8,28,5,9,80,1,36,37,0,89,48,0,0,0,
		4,0,11,0,2,0,12,0,3,0,48,1,0,95,
		254,112,0,95,255,106,10,105,100,95,114,101,103,105,
		111,110,0,2,120,80,253,48,2,0,95,252,112,0,
		6,80,8,36,38,0,95,1,28,18,36,39,0,95,
		8,80,10,36,40,0,95,8,80,9,25,15,36,42,
		0,100,80,10,36,43,0,95,8,80,9,36,52,0,
		106,87,32,32,9,83,69,76,69,67,84,32,9,105,
		100,95,114,101,103,105,111,110,44,10,32,32,9,9,
		9,9,9,110,111,109,98,114,101,44,10,9,9,9,
		9,9,9,109,121,95,114,101,99,110,111,32,9,9,
		10,32,32,9,70,82,79,77,32,37,49,32,10,32,
		32,9,79,82,68,69,82,32,66,89,32,110,111,109,
		98,114,101,10,0,80,6,36,54,0,176,3,0,95,
		6,48,4,0,176,5,0,12,0,112,0,12,2,80,
		6,36,56,0,48,6,0,48,7,0,176,5,0,12,
		0,112,0,95,6,112,1,80,2,36,58,0,48,8,
		0,176,9,0,12,0,106,7,84,65,72,79,77,65,
		0,121,92,245,100,120,100,100,100,100,100,100,100,100,
		100,100,100,100,100,112,18,80,5,36,60,0,48,8,
		0,176,10,0,12,0,100,100,100,100,106,16,84,65,
		66,76,65,32,82,69,71,73,79,78,69,83,32,0,
		106,8,120,98,114,111,119,115,101,0,100,9,100,100,
		100,100,100,9,48,11,0,176,5,0,12,0,112,0,
		92,12,1,95,5,100,100,100,9,100,106,5,111,68,
		108,103,0,100,100,100,112,25,80,3,36,65,0,106,
		10,105,100,95,114,101,103,105,111,110,0,106,12,78,
		117,109,46,32,82,101,103,105,111,110,0,100,92,100,
		121,4,5,0,106,7,110,111,109,98,114,101,0,106,
		7,78,111,109,98,114,101,0,100,93,144,1,121,4,
		5,0,4,2,0,80,7,36,71,0,176,12,0,95,
		3,121,121,100,100,100,100,100,100,100,100,100,100,100,
		100,100,9,95,2,100,9,100,9,92,100,120,9,100,
		95,7,4,1,0,100,100,120,9,9,120,100,100,100,
		100,9,9,106,5,111,66,114,119,0,100,12,41,80,
		4,36,73,0,95,4,143,36,74,0,144,13,0,112,
		0,73,36,75,0,144,14,0,89,27,0,1,0,1,
		0,9,0,95,1,92,13,8,28,11,48,15,0,95,
		255,112,0,25,3,100,6,112,1,73,36,76,0,144,
		16,0,89,27,0,1,0,3,0,4,0,2,0,6,
		0,176,17,0,95,255,95,254,95,253,95,1,12,4,
		6,112,1,73,36,77,0,144,18,0,48,19,0,95,
		4,112,0,112,1,73,36,78,0,144,20,0,95,10,
		112,1,73,145,36,81,0,48,21,0,176,22,0,12,
		0,92,101,100,95,3,100,100,100,9,100,9,9,100,
		112,11,73,36,82,0,48,23,0,95,4,48,21,0,
		176,22,0,12,0,92,102,89,17,0,0,0,1,0,
		4,0,48,24,0,95,255,112,0,6,95,3,106,3,
		64,33,0,93,128,0,97,255,255,0,0,9,100,9,
		9,100,112,11,112,1,73,36,87,0,48,25,0,95,
		3,48,26,0,95,3,112,0,48,27,0,95,3,112,
		0,48,28,0,95,3,112,0,120,89,51,0,1,0,
		1,0,12,0,95,255,28,5,120,25,36,176,29,0,
		106,14,68,101,115,101,97,32,83,97,108,105,114,32,
		63,0,48,30,0,176,5,0,12,0,112,0,12,2,
		165,80,255,6,100,89,42,0,1,0,5,0,3,0,
		4,0,2,0,11,0,12,0,176,31,0,95,255,20,
		1,176,32,0,95,255,95,254,95,253,96,252,255,96,
		251,255,12,5,6,48,33,0,95,3,112,0,100,100,
		100,100,96,3,0,100,112,14,73,36,89,0,176,34,
		0,95,5,12,1,28,10,48,2,0,95,5,112,0,
		73,100,80,5,36,91,0,48,2,0,95,2,112,0,
		73,36,93,0,95,11,106,10,105,100,95,114,101,103,
		105,111,110,0,1,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( CREABUTONBAR )
{
	static const HB_BYTE pcode[] =
	{
		13,1,5,36,100,0,48,8,0,176,35,0,12,0,
		95,1,92,70,92,70,9,100,100,9,120,9,100,100,
		100,100,100,9,100,9,9,112,18,80,6,36,105,0,
		48,36,0,176,37,0,12,0,48,11,0,176,5,0,
		12,0,112,0,92,2,1,100,100,100,100,89,35,0,
		1,0,2,0,3,0,2,0,176,38,0,95,255,120,
		20,2,48,39,0,95,254,112,0,73,48,40,0,95,
		254,112,0,6,9,95,6,9,100,100,9,100,106,59,
		40,32,76,101,97,68,97,116,111,115,40,32,111,81,
		114,121,44,32,46,84,46,32,41,44,32,111,66,114,
		119,58,82,101,102,114,101,115,104,40,41,44,32,111,
		66,114,119,58,83,101,116,102,111,99,117,115,40,41,
		32,41,0,100,106,6,78,117,101,118,111,0,100,100,
		100,100,100,100,100,100,9,100,100,100,112,28,73,36,
		111,0,48,36,0,176,37,0,12,0,48,11,0,176,
		5,0,12,0,112,0,92,3,1,100,100,100,100,89,
		35,0,1,0,2,0,3,0,2,0,176,38,0,95,
		255,9,20,2,48,39,0,95,254,112,0,73,48,40,
		0,95,254,112,0,6,120,95,6,9,100,100,9,100,
		106,59,40,32,76,101,97,68,97,116,111,115,40,32,
		111,81,114,121,44,32,46,70,46,32,41,44,32,111,
		66,114,119,58,82,101,102,114,101,115,104,40,41,44,
		32,111,66,114,119,58,83,101,116,102,111,99,117,115,
		40,41,32,41,0,100,106,10,77,111,100,105,102,105,
		99,97,114,0,100,100,100,100,100,100,100,100,9,100,
		100,100,112,28,73,36,117,0,48,36,0,176,37,0,
		12,0,48,11,0,176,5,0,12,0,112,0,92,4,
		1,100,100,100,100,89,34,0,1,0,2,0,3,0,
		2,0,176,41,0,95,255,20,1,48,39,0,95,254,
		112,0,73,48,40,0,95,254,112,0,6,120,95,6,
		9,100,100,9,100,106,55,40,32,68,101,108,82,101,
		99,111,114,100,40,32,111,81,114,121,32,41,44,32,
		111,66,114,119,58,82,101,102,114,101,115,104,40,41,
		44,32,111,66,114,119,58,83,101,116,102,111,99,117,
		115,40,41,32,41,0,100,106,9,69,108,105,109,105,
		110,97,114,0,100,100,100,100,100,100,100,100,9,100,
		100,100,112,28,73,36,123,0,48,36,0,176,37,0,
		12,0,48,11,0,176,5,0,12,0,112,0,92,15,
		1,100,100,100,100,89,25,0,1,0,1,0,2,0,
		48,42,0,95,255,112,0,73,48,40,0,95,255,112,
		0,6,120,95,6,9,100,100,9,100,106,34,40,32,
		111,66,114,119,58,71,111,84,111,112,40,41,44,32,
		111,66,114,119,58,83,101,116,102,111,99,117,115,40,
		41,32,41,0,100,106,7,73,110,105,99,105,111,0,
		100,100,100,100,100,100,100,100,9,100,100,100,112,28,
		73,36,128,0,48,36,0,176,37,0,12,0,48,11,
		0,176,5,0,12,0,112,0,92,16,1,100,100,100,
		100,89,25,0,1,0,1,0,2,0,48,43,0,95,
		255,112,0,73,48,40,0,95,255,112,0,6,9,95,
		6,9,100,100,9,100,106,37,40,32,111,66,114,119,
		58,71,111,66,111,116,116,111,109,40,41,44,32,111,
		66,114,119,58,83,101,116,102,111,99,117,115,40,41,
		32,41,0,100,106,6,70,105,110,97,108,0,100,100,
		100,100,100,100,100,100,9,100,100,100,112,28,73,36,
		134,0,48,36,0,176,37,0,12,0,48,11,0,176,
		5,0,12,0,112,0,92,17,1,100,100,100,100,89,
		44,0,1,0,1,0,2,0,48,44,0,95,255,106,
		17,82,101,112,111,114,116,101,32,82,101,103,105,111,
		110,101,115,0,112,1,73,48,40,0,95,255,112,0,
		6,120,95,6,9,100,100,9,100,106,53,40,32,111,
		66,114,119,58,82,101,112,111,114,116,40,34,82,101,
		112,111,114,116,101,32,82,101,103,105,111,110,101,115,
		34,41,44,32,111,66,114,119,58,83,101,116,102,111,
		99,117,115,40,41,32,41,0,100,106,9,73,109,112,
		114,105,109,105,114,0,100,100,100,100,100,100,100,100,
		9,100,100,100,112,28,73,36,140,0,48,36,0,176,
		37,0,12,0,48,11,0,176,5,0,12,0,112,0,
		92,5,1,100,100,100,100,89,17,0,1,0,1,0,
		1,0,48,2,0,95,255,112,0,6,120,95,6,9,
		100,100,9,100,106,15,40,32,111,68,108,103,58,69,
		110,100,40,41,32,41,0,100,106,6,83,97,108,105,
		114,0,100,100,100,100,100,100,100,100,9,100,100,100,
		112,28,73,36,142,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( LEADATOS )
{
	static const HB_BYTE pcode[] =
	{
		13,8,2,36,151,0,176,45,0,92,2,12,1,80,
		5,36,152,0,176,45,0,92,2,12,1,80,6,36,
		153,0,177,0,0,80,7,36,154,0,9,80,8,36,
		155,0,9,80,9,36,156,0,95,2,28,21,106,15,
		65,71,82,69,71,65,82,32,82,69,71,73,79,78,
		0,25,20,106,16,77,79,68,73,70,73,67,65,32,
		82,69,71,73,79,78,0,80,10,36,158,0,95,2,
		28,9,176,46,0,12,0,25,9,48,1,0,95,1,
		112,0,95,7,106,10,105,100,95,114,101,103,105,111,
		110,0,2,36,159,0,95,2,28,11,176,47,0,92,
		60,12,1,25,9,48,48,0,95,1,112,0,95,7,
		106,7,110,111,109,98,114,101,0,2,36,161,0,48,
		8,0,176,9,0,12,0,106,7,84,65,72,79,77,
		65,0,121,92,244,100,120,100,100,100,100,100,100,100,
		100,100,100,100,100,100,112,18,80,4,36,163,0,48,
		8,0,176,10,0,12,0,100,100,100,100,95,10,106,
		9,82,69,71,73,79,78,69,83,0,100,9,100,100,
		100,100,100,9,100,95,4,100,100,100,9,100,106,5,
		111,68,108,103,0,100,100,100,112,25,80,3,36,165,
		0,48,21,0,176,49,0,12,0,93,160,15,100,95,
		3,100,100,100,100,112,7,73,36,167,0,48,21,0,
		176,22,0,12,0,92,100,90,14,106,9,67,111,100,
		105,103,111,32,58,0,6,95,3,100,100,100,9,100,
		9,9,100,112,11,73,36,168,0,48,21,0,176,22,
		0,12,0,92,101,90,14,106,9,78,111,109,98,114,
		101,32,58,0,6,95,3,100,100,100,9,100,9,9,
		100,112,11,73,36,171,0,48,21,0,176,50,0,12,
		0,93,200,0,89,54,0,1,0,1,0,7,0,176,
		51,0,12,0,121,8,28,19,95,255,106,10,105,100,
		95,114,101,103,105,111,110,0,1,25,20,95,1,165,
		95,255,106,10,105,100,95,114,101,103,105,111,110,0,
		2,6,95,3,100,100,100,100,100,100,100,100,9,90,
		4,9,6,100,9,9,100,100,100,100,100,100,106,15,
		104,91,34,105,100,95,114,101,103,105,111,110,34,93,
		0,100,100,100,100,100,100,100,112,30,95,5,122,2,
		36,174,0,48,21,0,176,50,0,12,0,93,201,0,
		89,48,0,1,0,1,0,7,0,176,51,0,12,0,
		121,8,28,16,95,255,106,7,110,111,109,98,114,101,
		0,1,25,17,95,1,165,95,255,106,7,110,111,109,
		98,114,101,0,2,6,95,3,100,106,3,64,33,0,
		100,100,100,100,100,100,9,100,100,9,9,100,100,100,
		100,100,100,106,12,104,91,34,110,111,109,98,114,101,
		34,93,0,100,100,100,100,100,100,100,112,30,95,5,
		92,2,2,36,181,0,48,21,0,176,37,0,12,0,
		93,44,1,100,100,100,100,100,89,27,0,1,0,3,
		0,9,0,8,0,3,0,120,80,255,120,80,254,48,
		2,0,95,253,112,0,6,95,3,9,100,9,100,106,
		7,71,114,97,98,97,114,0,100,100,100,100,106,7,
		67,69,78,84,69,82,0,9,100,100,9,9,176,52,
		0,12,0,9,112,25,95,6,122,2,36,188,0,48,
		21,0,176,37,0,12,0,93,45,1,100,100,100,100,
		100,89,27,0,1,0,3,0,9,0,8,0,3,0,
		9,80,255,120,80,254,48,2,0,95,253,112,0,6,
		95,3,9,100,9,100,106,8,67,97,110,99,101,108,
		97,0,100,100,100,100,106,7,67,69,78,84,69,82,
		0,9,100,100,9,9,176,53,0,12,0,9,112,25,
		95,6,92,2,2,36,189,0,48,54,0,95,6,92,
		2,1,120,112,1,73,36,193,0,48,25,0,95,3,
		48,26,0,95,3,112,0,48,27,0,95,3,112,0,
		48,28,0,95,3,112,0,9,89,12,0,1,0,1,
		0,8,0,95,255,6,100,89,17,0,1,0,1,0,
		3,0,176,31,0,95,255,12,1,6,48,33,0,95,
		3,112,0,100,100,100,100,96,3,0,100,112,14,73,
		36,195,0,95,9,28,113,36,196,0,95,2,28,55,
		36,197,0,176,55,0,106,11,80,114,111,99,101,115,
		97,110,100,111,0,106,7,69,115,112,101,114,101,0,
		89,21,0,0,0,2,0,7,0,1,0,176,56,0,
		95,255,95,254,12,2,6,20,3,25,53,36,199,0,
		176,55,0,106,11,80,114,111,99,101,115,97,110,100,
		111,0,106,7,69,115,112,101,114,101,0,89,21,0,
		0,0,2,0,7,0,1,0,176,57,0,95,255,95,
		254,12,2,6,20,3,36,203,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( NEWRECORD )
{
	static const HB_BYTE pcode[] =
	{
		13,0,2,36,211,0,48,58,0,48,7,0,176,5,
		0,12,0,112,0,48,4,0,176,5,0,12,0,112,
		0,106,10,105,100,95,114,101,103,105,111,110,0,95,
		1,106,10,105,100,95,114,101,103,105,111,110,0,1,
		106,7,110,111,109,98,114,101,0,95,1,106,7,110,
		111,109,98,114,101,0,1,177,2,0,112,2,73,36,
		213,0,48,59,0,95,2,112,0,73,36,215,0,100,
		110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( MODRECORD )
{
	static const HB_BYTE pcode[] =
	{
		13,0,2,36,223,0,48,60,0,48,7,0,176,5,
		0,12,0,112,0,48,4,0,176,5,0,12,0,112,
		0,106,7,110,111,109,98,114,101,0,95,1,106,7,
		110,111,109,98,114,101,0,1,177,1,0,106,12,109,
		121,95,114,101,99,110,111,32,61,32,0,176,61,0,
		48,62,0,95,2,112,0,12,1,72,112,3,73,36,
		225,0,48,59,0,95,2,112,0,73,36,227,0,100,
		110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( DELRECORD )
{
	static const HB_BYTE pcode[] =
	{
		13,0,1,36,233,0,48,63,0,95,1,112,0,121,
		15,29,180,0,36,234,0,176,29,0,106,27,69,115,
		116,97,32,83,101,103,117,114,111,32,100,101,32,69,
		108,105,109,105,110,97,114,32,13,10,0,48,48,0,
		95,1,112,0,72,48,30,0,176,5,0,12,0,112,
		0,12,2,28,122,36,235,0,48,64,0,48,7,0,
		176,5,0,12,0,112,0,48,4,0,176,5,0,12,
		0,112,0,106,12,109,121,95,114,101,99,110,111,32,
		61,32,0,176,61,0,48,62,0,95,1,112,0,12,
		1,72,112,2,31,54,36,236,0,176,65,0,106,30,
		78,111,32,115,101,32,112,117,100,111,32,66,111,114,
		114,97,114,32,101,108,32,82,101,103,105,115,116,114,
		111,0,48,30,0,176,5,0,12,0,112,0,20,2,
		25,13,36,238,0,48,59,0,95,1,112,0,73,36,
		243,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC( SIIDREGION )
{
	static const HB_BYTE pcode[] =
	{
		13,3,1,36,250,0,9,80,4,36,0,1,106,56,
		32,32,9,83,69,76,69,67,84,32,42,32,70,82,
		79,77,32,37,49,10,32,32,9,87,72,69,82,69,
		32,105,100,95,114,101,103,105,111,110,32,61,32,37,
		50,10,32,32,9,76,73,77,73,84,32,49,10,0,
		80,3,36,2,1,176,3,0,95,3,48,4,0,176,
		5,0,12,0,112,0,176,61,0,95,1,12,1,12,
		3,80,3,36,4,1,48,6,0,48,7,0,176,5,
		0,12,0,112,0,95,3,112,1,80,2,36,6,1,
		48,63,0,95,2,112,0,121,15,28,5,120,25,3,
		9,80,4,36,8,1,48,2,0,95,2,112,0,73,
		36,10,1,95,4,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_STATIC( LASTIDREGION )
{
	static const HB_BYTE pcode[] =
	{
		13,3,0,36,17,1,121,80,3,36,23,1,106,59,
		32,32,9,83,69,76,69,67,84,32,42,32,70,82,
		79,77,32,37,49,10,32,32,9,79,82,68,69,82,
		32,66,89,32,105,100,95,114,101,103,105,111,110,32,
		68,69,83,67,10,32,32,9,76,73,77,73,84,32,
		49,10,0,80,2,36,25,1,176,3,0,95,2,48,
		4,0,176,5,0,12,0,112,0,12,2,80,2,36,
		27,1,48,6,0,48,7,0,176,5,0,12,0,112,
		0,95,2,112,1,80,1,36,29,1,48,63,0,95,
		1,112,0,121,15,28,14,36,30,1,48,1,0,95,
		1,112,0,80,3,36,33,1,48,2,0,95,1,112,
		0,73,36,35,1,174,3,0,36,37,1,95,3,110,
		7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC( GETARRAYREGIONES )
{
	static const HB_BYTE pcode[] =
	{
		13,3,0,36,44,1,4,0,0,80,3,36,50,1,
		106,59,9,9,83,69,76,69,67,84,32,105,100,95,
		114,101,103,105,111,110,44,32,110,111,109,98,114,101,
		10,9,9,70,82,79,77,32,37,49,10,9,9,79,
		82,68,69,82,32,66,89,32,105,100,95,114,101,103,
		105,111,110,10,0,80,2,36,52,1,176,3,0,95,
		2,48,4,0,176,5,0,12,0,112,0,12,2,80,
		2,36,54,1,48,6,0,48,7,0,176,5,0,12,
		0,112,0,95,2,112,1,80,1,36,56,1,48,68,
		0,95,1,112,0,31,42,36,57,1,176,69,0,95,
		3,48,1,0,95,1,112,0,48,48,0,95,1,112,
		0,4,2,0,20,2,36,58,1,48,70,0,95,1,
		112,0,73,25,206,36,61,1,48,2,0,95,1,112,
		0,73,36,63,1,176,71,0,95,3,12,1,121,8,
		28,34,36,64,1,176,69,0,95,3,121,106,16,78,
		111,32,104,97,121,32,82,101,103,105,111,110,101,115,
		0,4,2,0,20,2,36,67,1,95,3,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC( GETNAMEREGION )
{
	static const HB_BYTE pcode[] =
	{
		13,3,1,36,74,1,106,1,0,80,4,36,81,1,
		106,71,9,9,83,69,76,69,67,84,32,105,100,95,
		114,101,103,105,111,110,44,32,110,111,109,98,114,101,
		10,9,9,70,82,79,77,32,37,49,10,9,9,87,
		72,69,82,69,32,105,100,95,114,101,103,105,111,110,
		32,61,32,37,50,10,9,9,76,73,77,73,84,32,
		49,10,0,80,3,36,83,1,176,3,0,95,3,48,
		4,0,176,5,0,12,0,112,0,176,61,0,95,1,
		12,1,12,3,80,3,36,85,1,48,6,0,48,7,
		0,176,5,0,12,0,112,0,95,3,112,1,80,2,
		36,87,1,48,63,0,95,2,112,0,121,15,28,14,
		36,88,1,48,48,0,95,2,112,0,80,4,36,91,
		1,48,2,0,95,2,112,0,73,36,93,1,95,4,
		110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC( CREAR_REGIONES )
{
	static const HB_BYTE pcode[] =
	{
		13,1,0,36,108,1,105,32,1,32,32,9,67,82,
		69,65,84,69,32,84,65,66,76,69,32,73,70,32,
		78,79,84,32,69,88,73,83,84,83,32,37,49,10,
		32,32,9,40,32,105,100,95,114,101,103,105,111,110,
		9,32,32,105,110,116,40,49,48,41,32,78,79,84,
		32,78,85,76,76,32,68,69,70,65,85,76,84,32,
		48,44,10,32,32,32,32,32,9,110,111,109,98,114,
		101,32,32,32,32,9,99,104,97,114,40,56,48,41,
		32,78,79,84,32,78,85,76,76,32,68,69,70,65,
		85,76,84,32,39,32,39,44,10,32,32,32,32,32,
		9,109,121,95,114,101,99,110,111,9,32,32,105,110,
		116,40,49,50,41,32,78,79,84,32,78,85,76,76,
		32,65,85,84,79,95,73,78,67,82,69,77,69,78,
		84,44,10,32,32,32,32,32,32,80,82,73,77,65,
		82,89,32,75,69,89,32,40,109,121,95,114,101,99,
		110,111,41,44,10,32,32,32,32,32,32,73,78,68,
		69,88,32,110,111,109,98,114,101,32,40,110,111,109,
		98,114,101,41,10,32,32,9,32,32,41,32,67,79,
		76,76,65,84,69,32,61,32,39,108,97,116,105,110,
		49,95,115,112,97,110,105,115,104,95,99,105,39,32,
		69,78,71,73,78,69,32,61,32,73,110,110,111,68,
		66,10,0,80,1,36,110,1,176,3,0,95,1,48,
		4,0,176,5,0,12,0,112,0,12,2,80,1,36,
		112,1,48,74,0,48,7,0,176,5,0,12,0,112,
		0,48,4,0,176,5,0,12,0,112,0,95,1,112,
		2,73,36,114,1,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_INITSTATICS()
{
	static const HB_BYTE pcode[] =
	{
		117,77,0,2,0,116,77,0,4,0,0,82,1,0,
		100,82,2,0,7
	};

	hb_vmExecute( pcode, symbols );
}

