################################################################################
# [Jul..7.2017.14:52:49]                                                       #
#                                 contabilidad                                 #
#                                                                              #
#                                          Tuesday 29 September 2020, 18:56:29 #
################################################################################

##### GROUPS ###################################################################

FG00=\
	main.obj\
	catalogo.obj\
	tvarpub.obj\
	empresas.obj\
	comunas.obj\
	regiones.obj\
	ciudades.obj\
	catalogo_maestro.obj\
	fhreport.obj\
	asiento_contable.obj\
	clientes.obj\
	provedores.obj\
	balance.obj\
	tipo_documento.obj\
	libros.obj

FG01=\
	Bitmaps.res\
	mysql.res\
	Resource1.res


##### BUILD ####################################################################

ALL: $(FG00) $(FG01)
	ILINK32 -Lc:\Bcc700\LIB\OBJ;c:\Bcc700\LIB;c:\Bcc700\LIB\PSDK;c:\Harbour\LIB\;c:\fwh\lib -Gn -M -m -s -Tpe -x -aa c0w32.obj $(FG00),..\contabilidad,,  c:\albeiro\source\lib\avcsish.lib c:\Dolphin\LIB\HARBOUR\BCC\dolphin.lib c:\Dolphin\LIB\mysql\omf\libmysql.lib      FiveH.lib FiveHC.lib  hbrtl.lib hbvm.lib gtgui.lib hblang.lib hbmacro.lib hbrdd.lib rddntx.lib rddcdx.lib rddfpt.lib hbsix.lib hbdebug.lib hbcommon.lib hbpp.lib hbwin.lib hbcplr.lib hbcpage.lib hbct.lib xhb.lib hbpcre.lib hbmzip.lib minizip.lib hbzlib.lib hbZiparc.lib png.lib hbzebra.lib hbhpdf.lib libhpdf.lib hbmisc.lib import32.lib cw32.lib ws2_32.lib odbc32.lib nddeapi.lib iphlpapi.lib msimg32.lib rasapi32.lib psapi.lib shell32.lib gdiplus.lib,,$(FG01)
	
	echo.
	echo Copiando Archivo C:\contabilidad\contabilidad.EXE a la Carpeta C:\sistema
	echo.
	COPY "C:\contabilidad\contabilidad.EXE" "C:\sistema"
	echo.

contabilidad.EXE: $(FG00) $(FG01)
	ILINK32 -Lc:\Bcc700\LIB\OBJ;c:\Bcc700\LIB;c:\Bcc700\LIB\PSDK;c:\Harbour\LIB\;c:\fwh\lib -Gn -M -m -s -Tpe -x -aa c0w32.obj $(FG00),..\contabilidad,,  c:\albeiro\source\lib\avcsish.lib c:\Dolphin\LIB\HARBOUR\BCC\dolphin.lib c:\Dolphin\LIB\mysql\omf\libmysql.lib      FiveH.lib FiveHC.lib  hbrtl.lib hbvm.lib gtgui.lib hblang.lib hbmacro.lib hbrdd.lib rddntx.lib rddcdx.lib rddfpt.lib hbsix.lib hbdebug.lib hbcommon.lib hbpp.lib hbwin.lib hbcplr.lib hbcpage.lib hbct.lib xhb.lib hbpcre.lib hbmzip.lib minizip.lib hbzlib.lib hbZiparc.lib png.lib hbzebra.lib hbhpdf.lib libhpdf.lib hbmisc.lib import32.lib cw32.lib ws2_32.lib odbc32.lib nddeapi.lib iphlpapi.lib msimg32.lib rasapi32.lib psapi.lib shell32.lib gdiplus.lib,,$(FG01)
	
	echo.
	echo Copiando Archivo C:\contabilidad\contabilidad.EXE a la Carpeta C:\sistema
	echo.
	COPY "C:\contabilidad\contabilidad.EXE" "C:\sistema"
	echo.


##### COMMANDS #################################################################

run:  contabilidad.EXE
	contabilidad.EXE

makef: 
	uestudio contabilidad.mak


##### COMPILE ##################################################################

INC001=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

main.c: C:\contabilidad\prg\main.prg $(INC001)
	harbour C:\contabilidad\prg\main.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /omain.c


INC002=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

catalogo.c: C:\contabilidad\prg\catalogo.prg $(INC002)
	harbour C:\contabilidad\prg\catalogo.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /ocatalogo.c


INC003=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

tvarpub.c: C:\contabilidad\prg\tvarpub.prg $(INC003)
	harbour C:\contabilidad\prg\tvarpub.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /otvarpub.c


Bitmaps.res: C:\contabilidad\res\Bitmaps.rc
	"C:\PellesC\Bin\porc.exe" /I "C:\PellesC\include" /I "C:\PellesC\include\win" /FoBitmaps.res C:\contabilidad\res\Bitmaps.rc


mysql.res: C:\contabilidad\res\mysql.rc
	"C:\PellesC\Bin\porc.exe" /I "C:\PellesC\include" /I "C:\PellesC\include\win" /Fomysql.res C:\contabilidad\res\mysql.rc


Resource1.res: C:\contabilidad\res\Resource1.rc
	"C:\PellesC\Bin\porc.exe" /I "C:\PellesC\include" /I "C:\PellesC\include\win" /FoResource1.res C:\contabilidad\res\Resource1.rc


INC004=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

empresas.c: C:\contabilidad\prg\empresas.prg $(INC004)
	harbour C:\contabilidad\prg\empresas.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /oempresas.c


INC005=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

comunas.c: C:\contabilidad\prg\comunas.prg $(INC005)
	harbour C:\contabilidad\prg\comunas.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /ocomunas.c


INC006=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

regiones.c: C:\contabilidad\prg\regiones.prg $(INC006)
	harbour C:\contabilidad\prg\regiones.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /oregiones.c


INC007=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

ciudades.c: C:\contabilidad\prg\ciudades.prg $(INC007)
	harbour C:\contabilidad\prg\ciudades.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /ociudades.c


INC008=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

catalogo_maestro.c: C:\contabilidad\prg\catalogo_maestro.prg $(INC008)
	harbour C:\contabilidad\prg\catalogo_maestro.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /ocatalogo_maestro.c


INC009=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

fhreport.c: C:\contabilidad\prg\fhreport.prg $(INC009)
	harbour C:\contabilidad\prg\fhreport.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /ofhreport.c


INC010=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\fwh\include\dtpicker.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

asiento_contable.c: C:\contabilidad\prg\asiento_contable.prg $(INC010)
	harbour C:\contabilidad\prg\asiento_contable.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /oasiento_contable.c


INC011=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

clientes.c: C:\contabilidad\prg\clientes.prg $(INC011)
	harbour C:\contabilidad\prg\clientes.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /oclientes.c


INC012=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

provedores.c: C:\contabilidad\prg\provedores.prg $(INC012)
	harbour C:\contabilidad\prg\provedores.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /oprovedores.c


INC013=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

balance.c: C:\contabilidad\prg\balance.prg $(INC013)
	harbour C:\contabilidad\prg\balance.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /obalance.c


INC014=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

tipo_documento.c: C:\contabilidad\prg\tipo_documento.prg $(INC014)
	harbour C:\contabilidad\prg\tipo_documento.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /otipo_documento.c


INC015=c:\fwh\include\fivewin.ch\
	c:\fwh\include\Dialog.ch\
	c:\fwh\include\Font.ch\
	c:\fwh\include\Ini.ch\
	c:\fwh\include\Menu.ch\
	c:\fwh\include\Print.ch\
	c:\fwh\include\memoedit.ch\
	c:\fwh\include\Colors.ch\
	c:\fwh\include\DLL.ch\
	c:\fwh\include\Folder.ch\
	c:\fwh\include\Objects.ch\
	c:\Harbour\INCLUDE\hbclass.ch\
	c:\Harbour\INCLUDE\hboo.ch\
	c:\fwh\include\obendcls.ch\
	c:\fwh\include\ODBC.ch\
	c:\fwh\include\DDE.ch\
	c:\fwh\include\Video.ch\
	c:\fwh\include\VKey.ch\
	c:\fwh\include\Tree.ch\
	c:\fwh\include\WinApi.ch\
	c:\fwh\include\FwMsgs.h\
	c:\fwh\include\xbrowse.ch\
	c:\Contabilidad\include\contabil.ch\
	c:\dolphin\include\tdolphin.ch\
	c:\Contabilidad\include\defines_fr3.ch

libros.c: C:\contabilidad\prg\libros.prg $(INC015)
	harbour C:\contabilidad\prg\libros.prg /n /gc0 /q /ic:\Harbour\INCLUDE /ic:\Contabilidad\include /ic:\fwh\include /w1 /olibros.c


main.obj: main.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -omain.obj main.c


catalogo.obj: catalogo.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -ocatalogo.obj catalogo.c


tvarpub.obj: tvarpub.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -otvarpub.obj tvarpub.c


empresas.obj: empresas.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -oempresas.obj empresas.c


comunas.obj: comunas.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -ocomunas.obj comunas.c


regiones.obj: regiones.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -oregiones.obj regiones.c


ciudades.obj: ciudades.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -ociudades.obj ciudades.c


catalogo_maestro.obj: catalogo_maestro.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -ocatalogo_maestro.obj catalogo_maestro.c


fhreport.obj: fhreport.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -ofhreport.obj fhreport.c


asiento_contable.obj: asiento_contable.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -oasiento_contable.obj asiento_contable.c


clientes.obj: clientes.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -oclientes.obj clientes.c


provedores.obj: provedores.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -oprovedores.obj provedores.c


balance.obj: balance.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -obalance.obj balance.c


tipo_documento.obj: tipo_documento.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -otipo_documento.obj tipo_documento.c


libros.obj: libros.c
	bcc32 -c -D__EXPORT__ -D_FROMCPP -DHB_PRG_TRACE -Ic:\Harbour\INCLUDE;c:\Bcc700\INCLUDE;;c:\Contabilidad\include;;c:\fwh\include  -olibros.obj libros.c



##### CLEAN ####################################################################

CLEAN:
	-@erase "main.c"
	-@erase "catalogo.c"
	-@erase "tvarpub.c"
	-@erase "Bitmaps.res"
	-@erase "mysql.res"
	-@erase "Resource1.res"
	-@erase "empresas.c"
	-@erase "comunas.c"
	-@erase "regiones.c"
	-@erase "ciudades.c"
	-@erase "catalogo_maestro.c"
	-@erase "fhreport.c"
	-@erase "asiento_contable.c"
	-@erase "clientes.c"
	-@erase "provedores.c"
	-@erase "balance.c"
	-@erase "tipo_documento.c"
	-@erase "libros.c"
	-@erase "main.obj"
	-@erase "catalogo.obj"
	-@erase "tvarpub.obj"
	-@erase "empresas.obj"
	-@erase "comunas.obj"
	-@erase "regiones.obj"
	-@erase "ciudades.obj"
	-@erase "catalogo_maestro.obj"
	-@erase "fhreport.obj"
	-@erase "asiento_contable.obj"
	-@erase "clientes.obj"
	-@erase "provedores.obj"
	-@erase "balance.obj"
	-@erase "tipo_documento.obj"
	-@erase "libros.obj"
	-@erase "contabilidad.EXE"

