/*
*  ASIENTO CONTABLE
*/

#include "fivewin.ch"
#include "dtpicker.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_CATALOGO 		=>	oApp():Catalogo
#xtranslate		TABLA_MAESTRO			=>	oApp():AsientoMaestro
#xtranslate		TABLA_LINEAS			=>	oApp():AsientoLIneas
#xtranslate		TABLA_TIPODOCUM 	=>	oApp():TipoDocum

//---------------------------------------

#define PIC_MONEY   "@E 999,999,999,999"

//---------------------------------------

Function Do_Asientos()
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local cTitle
  Local h     := {=>}  
  Local lExit := FALSE
    
  cTitle  = "ASIENTOS CONTABLES {"  
  cTitle += "EMPRESA " + StrZero(oApp():Session["id_empresa"],3)
  cTitle += ", " + AllTrim(oApp():Session["razon"])
  cTitle += ", " + AllTrim(oApp():Session["nombre"]) 
  cTitle += ", A�O : " + Str( oApp():Session["year"], 4 )
	cTitle += ", MES : " + StrZero( oApp():Session["month"], 2 )
	cTitle += " " + oApp():Session["namemonth"] + "}"  
    
  TEXT INTO cQuery
  	SELECT 	a.id_empresa, 	
  					a.numero, 			
						a.year,        
						a.month,       
						a.fecha,       
						a.tipo,
						CASE 
							WHEN a.tipo = 1 THEN "INGRESO"
							WHEN a.tipo = 2 THEN "EGRESO"
							WHEN a.tipo = 3 THEN "TRASPASO"
							WHEN a.tipo = 4 THEN "APERTURA"
							ELSE "NINGUNO"
						END AS nametipo,
						a.transaccion,
						CASE 
							WHEN a.transaccion = 1 THEN "COMPRAS"
							WHEN a.transaccion = 2 THEN "VENTAS"
							WHEN a.transaccion = 3 THEN "HONORARIOS"
							WHEN a.transaccion = 4 THEN "OTROS"
							ELSE "NINGUNO"
						END AS nametrans,		
						a.rut,			
						a.tiporut,     
						a.namerut,     
						a.id_docum,		
						b.nombre AS name_docum,
						a.descripcion, 
						a.my_recno 		
    FROM %1 a
    LEFT JOIN %2 b ON a.id_docum = b.id_docum
    WHERE a.id_empresa = %3 AND a.year = %4 AND a.month = %5
		ORDER BY a.numero
  ENDTEXT	
  
  cQuery := StrFormat(  cQuery, TABLA_MAESTRO,;
                                TABLA_TIPODOCUM,;
  															ClipValue2Sql(oApp():Session["id_empresa"]),;
  															ClipValue2Sql(oApp():Session["year"]),;
  															ClipValue2Sql(oApp():Session["month"]) )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE cTitle RESOURCE "xbrowse"; 
  	ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id_empresa" 	, "Empresa", Nil,  80, AL_RIGHT},; 
  	{ "numero"   		, "Asiento", Nil,  80, AL_RIGHT},;
  	{ "year"      	, "A�o" 	 , Nil,  80, AL_RIGHT},;
  	{ "month"  		  , "Mes"    , Nil,  60, AL_RIGHT},;
  	{ "nametipo"  	, "Tipo"   , Nil, 120, AL_LEFT },;
  	{ "nametrans"  	, "Trans"  , Nil, 120, AL_LEFT },;
  	{ "rut" 				, "Rut"		 , Nil, 100, AL_LEFT },;
  	{ "tiporut"			, "TipoRut", Nil, 100, AL_LEFT },;
  	{ "namerut"			, "Nombre" , Nil, 300, AL_LEFT },;
  	{ "name_docum"	, "Documento", Nil, 180, AL_LEFT },;
  	{ "descripcion" , "Descripcion", Nil, 300, AL_LEFT } ;
  }    
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bKeyDown     = {|nKey| if(nKey == VK_RETURN, Eval(oBrw:bLDblClick), Nil)}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= {|| Nil }
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()
  
Return Nil

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( AddAsiento( oQry ), oBrw:Refresh(), oBrw:Setfocus() )
  
  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_CLEAR ]; 
  	ACTION ( LoadAsiento( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Asientos"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
     
Return Nil

//---------------------------------------

static Function AddAsiento( oQry )
	Local obj := TAsiento():New( oQry, TRUE )
	
	obj:DialogAsiento()
	
Return Nil

//---------------------------------------

static Function LoadAsiento( oQry )
	Local obj := TAsiento():New( oQry, FALSE )
	
	obj:LoadDatos()	
	obj:DialogAsiento()
	
Return Nil

//---------------------------------------

CLASS TAsiento
	
	DATA lNueva
	DATA oQry
	
	DATA oDlg
	DATA oSay
	DATA oBrw
	DATA cTitle
	DATA aItems 
	DATA aIndTipo
	DATA aNomTipo
	DATA aIndTrans
	DATA aNomTrans
	DATA aIndDocum
	DATA aNomDocum
	DATA oGet   
	DATA hVar
	DATA oBtn
	DATA lExit  
	
	METHOD New( oQry, lNueva )
	METHOD DialogAsiento()
	METHOD CreaButtonBar()
	METHOD MsgBarDialog( cMsg )
	METHOD AddRow()
	METHOD InsertRow( aArray )
	METHOD DeleteRow()
	METHOD BuscaCuenta()
	METHOD ValidaCuenta( cCuenta )
	METHOD BuscaDescripcion( oCol, nKey )
	METHOD KeyDown( nKey )
	METHOD UpdateMonto( nCol, Value )
	METHOD ValidaMonto()
	METHOD GoColDebito()
	
	METHOD GrabaAsiento()
	METHOD GoBrabaAsiento()
	METHOD AddLineas()
	METHOD LoadDatos()
	METHOD LoadItems()
	
	METHOD EliminaAsiento()
	METHOD GoDeleteAsiento()
	
ENDCLASS 	

//---------------------------------------

METHOD New( oQry, lNueva ) CLASS TAsiento
	Local aTipos := ArrTipoAsiento()
	Local aTrans := ArrTipoTrans()
	Local aDocum := ArrTipoDocumento()
	
	::oQry	= oQry
	
	DEFAULT lNueva := TRUE
	::lNueva   	= lNueva
	
	::aItems   	= { BlankRow() }
	::aIndTipo 	= ArrTranspose(aTipos)[1]
	::aNomTipo 	= ArrTranspose(aTipos)[2]
	::aIndTrans = ArrTranspose(aTrans)[1]
	::aNomTrans = ArrTranspose(aTrans)[2]
	::aIndDocum = ArrTranspose(aDocum)[1]
	::aNomDocum = ArrTranspose(aDocum)[2]
	::oGet     	= Array(3)
	::oBtn		 	= {} 
	::lExit    	= FALSE
	
	::hVar	= { => }
	::hVar["numero"]			= 0
	::hVar["id_empresa"]	= oApp():Session["id_empresa"]
	::hVar["year"]				= oApp():Session["year"]
	::hVar["month"]				= oApp():Session["month"]
	::hVar["tipo"]				= ::aIndTipo[1]
	::hVar["nametipo"]		= ::aNomTipo[1]
	::hVar["trans"]       = ::aIndTrans[1]
	::hVar["nametrans"]   = ::aNomTrans[1]
	::hVar["rut"]         = Space(10)
	::hVar["namerut"]			= ""
	::hVar["tiporut"]			= "N" // C = Cliente, P = Provedor, N = Ninguuno
	::hVar["fecha"]				= Date()
	::hVar["id_docum"]    = ::aIndDocum[1]
	::hVar["namedocu"]    = ::aNomDocum[1]
	::hVar["descripcion"]	= Space(60)
	
	::cTitle  = "ASIENTO CONTABLE {"  
  ::cTitle += "EMPRESA " + StrZero(oApp():Session["id_empresa"],3)
  ::cTitle += ", " + AllTrim(oApp():Session["razon"])
  ::cTitle += ", " + AllTrim(oApp():Session["nombre"]) 
  ::cTitle += ", A�O : " + Str( oApp():Session["year"], 4 )
	::cTitle += ", MES : " + StrZero( oApp():Session["month"], 2 )
	::cTitle += " " + oApp():Session["namemonth"] + "}"
	
Return Self

//---------------------------------------

METHOD DialogAsiento() CLASS TAsiento
	Local oFont	
	Local aCols
	Local oRut
	Local oName
	Local oSelf := Self
	
	::oGet = Array(7)
		
	aCols := { ;
		{ _CUENTA, 		"Cuenta", 		"@R 9-9-9-9-9-9", 120, AL_LEFT  },;
		{ _NOMBRE, 		"Nombre", 		Nil, 							350, AL_LEFT  },;
		{ _DEBE, 	 		"Debe", 	 		PIC_MONEY, 				100, AL_RIGHT },;
		{ _HABER,  		"Haber",  		PIC_MONEY, 				100, AL_RIGHT },;
		{ _DOCUMENTO, "Documento",  Nil, 							120, AL_LEFT  };
	}	
		
	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG ::oDlg TITLE ::cTitle RESOURCE "ASIENTO_CONTABLE" FONT oFont
	
  REDEFINE SAY ID 100 OF ::oDlg PROMPT "TIPO :"	
	REDEFINE SAY ID 101 OF ::oDlg PROMPT "TRANSACCION :"
	REDEFINE SAY oRut ID 102 OF ::oDlg PROMPT "PROVEDOR :"
	REDEFINE SAY ID 103 OF ::oDlg PROMPT "DESCRIPCION:"
	REDEFINE SAY ID 104 OF ::oDlg PROMPT "NUMERO :"			
	REDEFINE SAY ID 105 OF ::oDlg PROMPT "FECHA :"					
	REDEFINE SAY ID 106 OF ::oDlg PROMPT "TIPO DOCUM :"	
	REDEFINE SAY oName ID 107 OF ::oDlg PROMPT ::hVar["namerut"]
    
  REDEFINE COMBOBOX ::oGet[1] VAR ::hVar["nametipo"] ITEMS ::aNomTipo ID 200 OF ::oDlg;
  	WHEN ::lNueva	
  	::oGet[1]:bChange = <|| 
  	                      ::oGet[2]:enable()
  	                      ::oGet[3]:enable()
  												::hVar["tipo"] = ::aIndTipo[ ::oGet[1]:nAt ]
  												if ::hVar["tipo"] == 4
  												   ::hVar["trans"] 		 = 4
  												   ::hVar["nametrans"] = ::aNomTrans[4]
  												   ::oGet[2]:Refresh()
  												   ::oGet[2]:disable()
  												   Eval( ::oGet[2]:bChange )
  												   ::oGet[4]:Setfocus()
  												endif   
  												Return Nil
  											>	
  	  
  REDEFINE COMBOBOX ::oGet[2] VAR ::hVar["nametrans"] ITEMS ::aNomTrans ID 201 OF ::oDlg;
  	WHEN ::lNueva .and. !(::hVar["tipo"] == 4)
  	::oGet[2]:bChange = <|| 
  												::hVar["trans"] = ::aIndTrans[ ::oGet[2]:nAt ]
  												
  												::oGet[3]:enable()	
  												do case
  												case ::hVar["trans"] == 1  
  												   ::hVar["tiporut"] = "P"
  												   oRut:SetText("PROVEDOR :")
  												case ::hVar["trans"] == 2  
  												   ::hVar["tiporut"] = "C"   
  												   oRut:SetText("CLIENTE :")
  												case ::hVar["trans"] == 3  
  												   ::hVar["tiporut"] = "P"
  												   oRut:SetText("PROVEDOR :")   
  												case ::hVar["trans"] == 4  
  												   ::hVar["tiporut"] = "N"
  												   oRut:SetText("SIN RUT :")
  												   ::oGet[3]:disable()	
  												endcase         
  												
  												Return Nil
  											>	
    
  REDEFINE GET ::oGet[3] VAR ::hVar["rut"] ID 202 OF ::oDlg; 
  	PICTURE "@!";
  	BITMAP aBitmap[ BMP16_SEARCH ];
  	WHEN !( ::hVar["tiporut"] == "N" ) .and. ::lNueva	
  	::oGet[3]:bValid = <|oGet|
  											Local value   := oGet:varGet()
  											Local cuenta  := ""
  											Local namecta := ""
  											
  											if ::hVar["tiporut"] == "C"
  												 if !SiRutCliente(value)
  												 	  MsgStop("Rut Cliente NO Existe", Name_Empresa)
  												    oName:SetText("")
  												    Return FALSE
  											   endif					
  											   ::hVar["namerut"] = GetNameRutCliente(value)
  											   cuenta  := GetCuentaCliente( value )
  											   namecta := GetNameCuentaCatalogo( cuenta )
  											   oName:SetText( ::hVar["namerut"] )
  											elseif ::hVar["tiporut"] == "P"
  												 if !SiRutProvedor(value)
  												 	  MsgStop("Rut Provedor NO Existe", Name_Empresa)
  												    oName:SetText("")
  												    Return FALSE
  											   endif					
  											   ::hVar["namerut"] = GetNameRutProvedor(value) 
  											   cuenta  := GetCuentaProvedor( value )
  											   namecta := GetNameCuentaCatalogo( cuenta )
  											   oName:SetText( ::hVar["namerut"] )
  											endif   
  											
  											// ::InsertRow( { cuenta, namecta, 0, 0 } ) por si acaso
  											
  											Return TRUE
  										 >	
  										 
		::oGet[3]:bAction = <|oGet|
												 if ::hVar["tiporut"] == "C" 	
		                     	  ::hVar["rut"] = Clientes()
		                     else
		                     		::hVar["rut"] = Provedores()
		                     endif	  
												 oGet:varPut( ::hVar["rut"] )
												 oGet:refresh()
												 Eval(oGet:bValid, oGet)
												 Return Nil
												> 
  
  REDEFINE GET ::oGet[4] VAR ::hVar["descripcion"] ID 203 OF ::oDlg PICTURE "@!";
  	WHEN ::lNueva
  	
  REDEFINE GET ::oGet[5] VAR ::hVar["numero"] ID 204 OF ::oDlg PICTURE "99999999";
  	WHEN FALSE
  
  REDEFINE DTPICKER ::oGet[6] VAR ::hVar["fecha"] ID 205 OF ::oDlg;
  	WHEN ::lNueva	
  	::oGet[6]:bValid = <|oGet|
  											Local nMonth := Month(oGet:varGet())
  											Local nYear  := Year(oGet:varGet())
  	                   	if ( nMonth != oApp():Session["month"] ) 
  	                   	   MsgStop("El mes debe ser " + oApp():Session["namemonth"])
  	                   	   Return FALSE
  	                   	elseif ( nYear != oApp():Session["year"] ) 
  	                   	   MsgStop("El A�o debe ser " + Str(oApp():Session["year"]))
  	                   	   Return FALSE
  	                   	endif
  	                   	Return TRUE
  	                   > 	  
  	                     	
  REDEFINE COMBOBOX ::oGet[7] VAR ::hVar["namedocu"] ITEMS ::aNomDocum ID 206 OF ::oDlg;
  	WHEN ::lNueva	
  	::oGet[7]:bChange = <|| 
  												::hVar["id_docum"] = ::aIndDocum[ ::oGet[7]:nAt ]
  												Return Nil
  											>	
    
  REDEFINE XBROWSE ::oBrw ID 800 OF ::oDlg;
  	DATASOURCE ::aItems;
    COLUMNS aCols;
    LINES CELL FASTEDIT FOOTERS
      
  WITH OBJECT ::oBrw  
  		    
    WITH OBJECT :Cuenta
    	// :bClrEdit  = { || { CLR_BLACK, CLR_YELLOW } } *** Para Poner colores de Get       
    	:nEditType  	= if(::lNueva, EDIT_GET_BUTTON , EDIT_NONE)
    	:bEditBlock 	= {|| ::BuscaCuenta() }
    	:bEditValid 	= {|oGet, oCol| if(::ValidaCuenta(oGet:Value()), TRUE, FALSE) }
    	:bOnPreEdit		= {|| ::MsgBarDialog("CTRL+B = Buscar")}
    	:bOnPostEdit 	= {|o,v,k| ::GoColDebito(), ::MsgBarDialog("")} 
    	:cEditPicture = "@R 9-9-9-9-9-9"
			:addResource( aBitmap[ BMP16_SEARCH ] )
			:nBtnBmp 			= 1  
		END
    
  	WITH OBJECT :Nombre
  		if ::lNueva
  			 :nEditType    = EDIT_BUTTON
      	 :bEditBlock   = {|r,c,o,k| ::BuscaDescripcion(o,k) }
      	 :cEditPicture = "@!"
      else
      	 :nEditType    = EDIT_NONE
      endif
      :cFooter = "TOTALES"
    END
    
    WITH OBJECT :Debe
    	if ::lNueva
    		 :nEditType 	= EDIT_GET
    		 :bOnChange		= {|o| ::ValidaMonto(_DEBE) }
    		 :bOnPostEdit = {|o,v,k| ::UpdateMonto(v), ::oBrw:GoToCol(5) }
    	else
    	   :nEditType   = EDIT_NONE
    	endif	 
    	:nFooterType 		= AGGR_TOTAL
    	:cFooterPicture = PIC_MONEY
    	:nFootStrAlign  = AL_RIGHT
    END
    
    WITH OBJECT :Haber
      if ::lNueva
    	   :nEditType 	= EDIT_GET
    	   :bOnChange   = {|o| ::ValidaMonto(_HABER) }
    	   :bOnPostEdit = {|o,v,k| ::UpdateMonto(v), ::oBrw:GoToCol(5) }
    	else
    	   :nEditType   = EDIT_NONE
    	endif	 
    	:nFooterType 		= AGGR_TOTAL
    	:cFooterPicture = PIC_MONEY
    	:nFootStrAlign  = AL_RIGHT
    END
    
    WITH OBJECT :Documento
    	if ::lNueva
    	   :nEditType 	= EDIT_GET
    	else
    	   :nEditType   = EDIT_NONE
    	endif  
    END	 
    
    :MyConfig()
    :ShowNumRow( "Nro", 40 )
    :nStretchCol 	= STRETCHCOL_LAST
    :nFreeze      = 2
    :bKeyDown 		= {|nKey| ::KeyDown(nKey)}
    :bPastEof     = {|| if(!Empty(::oBrw:aRow[_CUENTA]), (::AddRow(), ::oBrw:GoToCol(_CUENTA)), Nil)}
    // :bKeyChar := { |n| If( n == VK_RETURN, 0, nil ) } not move with VK_RETURN
    :MakeTotals()
     
  END  
  
  AEval( ::oDlg:aControls, {|o| o:bKeyDown := {|nKey| ::KeyDown(nKey)} } )
    
  ::oDlg:bInit = <|| 
                    DisableSysMenuDlg(oSelf:oDlg)
                    oSelf:CreaButtonBar()
                    SET MSGBAR OF oSelf:oDlg TO "" 2015 FONT oFont 
										oSelf:oDlg:oMsgBar:Disable()  
										Return Nil
								 >	

	ACTIVATE DIALOG ::oDlg;
		CENTER;
		VALID oSelf:lExit
		
Return Nil		
  
//---------------------------------------

METHOD CreaButtonBar() CLASS TAsiento
  Local oBar
  
  ::oBtn := Array(5)
  
  DEFINE BUTTONBAR oBar OF ::oDlg 2010 SIZE 70, 70
  
  DEFINE BUTTON ::oBtn[1] OF oBar; 
  	PROMPT "F2 Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];
  	ACTION ( ::AddRow() );
  	WHEN ::lNueva 
  	
  DEFINE BUTTON ::oBtn[2] OF oBar; 
  	PROMPT "F3 Borrar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ];
  	ACTION ( ::DeleteRow() );
  	WHEN ::lNueva;
  	GROUP		
  	
  DEFINE BUTTON ::oBtn[3] OF oBar; 
  	PROMPT "F5 Grabar"; 
  	RESOURCE aBitmap[ BMP32_SAVE ];
  	ACTION ( ::GrabaAsiento() );
  	WHEN ::lNueva;
  	GROUP
    
  DEFINE BUTTON ::oBtn[4] OF oBar; 
  	PROMPT "F6 Eliminar"; 
  	RESOURCE aBitmap[ BMP32_CLEAR ];
  	ACTION ( ::EliminaAsiento() );
  	GROUP;
  	WHEN !::lNueva
    
  DEFINE BUTTON ::oBtn[5] OF oBar; 
  	PROMPT "Esc Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ];
  	ACTION ( ::lExit := TRUE, ::oDlg:End() );
  	GROUP
  	::oBtn[5]:lCancel = TRUE
  	
Return Nil

//--------------------------------------

METHOD LoadDatos() CLASS TAsiento
		
	::hVar["numero"]			= ::oQry:	numero
	::hVar["id_empresa"]	= oApp():Session["id_empresa"]
	::hVar["year"]				= oApp():Session["year"]
	::hVar["month"]				= oApp():Session["month"]
	::hVar["tipo"]				= ::oQry:tipo
	::hVar["nametipo"]		= ::aNomTipo[::oQry:tipo]
	::hVar["trans"]       = ::oQry:transaccion
	::hVar["nametrans"]   = ::aNomTrans[::oQry:transaccion]
	::hVar["rut"]         = ::oQry:rut
	::hVar["namerut"]			= ::oQry:namerut
	::hVar["tiporut"]			= ::oQry:tiporut
	::hVar["fecha"]				= ::oQry:fecha
	::hVar["id_docum"]    = ::oQry:id_docum
	::hVar["namedocu"]    = GetNameTipoDocum(::oQry:id_docum, TRUE)
	::hVar["descripcion"]	= ::oQry:descripcion
		
	MsgProgress("Leyendo Items", "Espere ...", {|| ::LoadItems()})
				
Return Nil

//--------------------------------------

METHOD LoadItems() CLASS TAsiento
	Local oQry
	Local cQuery
	
	::aItems = {}
	
	TEXT INTO cQuery
		SELECT * 
		FROM %1
		WHERE numero = %2
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_LINEAS, ::oQry:numero )
	
	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
		AAdd( ::aItems, { oQry:cuenta,; 
										 	oQry:namecuenta,; 
										 	oQry:debe,; 
										 	oQry:haber,; 
										 	oQry:documento } )
		oQry:skip()
	enddo
	
	oQry:End()
		
Return Nil	

//--------------------------------------

METHOD MsgBarDialog( cMsg ) CLASS TAsiento

	DEFAULT cMsg := ""

  ::oDlg:oMsgBar:cMsgDef  = cMsg
  ::oDlg:oMsgBar:nClrText = CLR_HRED
	::oDlg:oMsgBar:Refresh()
		  	    
Return Nil

//-------------------------------------------//

METHOD KeyDown( nKey ) CLASS TAsiento
	
	if GetKeyState(VK_CONTROL)
  	 if Upper(Chr(nKey)) = "B" .and. ::oBrw:nColSel == 1
  	    Eval( ::oBrw:aCols[1]:bEditBlock )
  	 endif
  elseif GetKeyState(VK_ESCAPE)
     if MsgYesNo("Salir del Asiento ?", Name_Empresa)
        Eval( ::oBtn[5]:bAction)
     endif
  else    	 
  	 do case
    		case nKey == VK_F2
    	 		if Eval( ::oBtn[1]:bWhen )
    	 			 Eval( ::oBtn[1]:bAction )
    	 		endif  
    	 		   
       	case nKey == VK_F3 .OR. nKey == VK_DELETE
       		if Eval( ::oBtn[2]:bWhen )
       			 Eval( ::oBtn[2]:bAction )
       		endif		
       			
       	case nKey == VK_F5
    	 		if Eval( ::oBtn[3]:bWhen )
    	 			 Eval( ::oBtn[3]:bAction )
    	 		endif  	
    	 		
    	 	case nKey == VK_F6
    	 		if Eval( ::oBtn[4]:bWhen )
    	 			 Eval( ::oBtn[4]:bAction )
    	 		endif  		
     endcase
  endif		
     
Return Nil

//---------------------------------------

METHOD BuscaCuenta() CLASS TAsiento

	::ValidaCuenta( Catalogo( TRUE ) )
	
Return Nil	

//--------------------------------------

METHOD ValidaCuenta( cCuenta ) CLASS TAsiento
    
  DEFAULT cCuenta := ""
          
  if !SiCuentaCatalogo( cCuenta )
  	 MsgStop("Cuenta " + cCuenta + " NO Existe", Name_Empresa )
     Return FALSE	
  elseif !SiCuentaImputable( cCuenta )
     MsgStop("Cuenta " + cCuenta + " NO es Imputable", Name_Empresa )
     Return FALSE	   
  endif
      
  ::InsertRow( { cCuenta, GetNameCuentaCatalogo( cCuenta ), 0, 0 } )
  	
  ::GoColDebito()	
    
Return TRUE

//--------------------------------------

METHOD AddRow() CLASS TAsiento

  AAdd( ::oBrw:aArrayData, BlankRow() )
  ::oBrw:Refresh()
  ::oBrw:GoBottom()
  ::oBrw:GoToCol(1)
  
Return Nil

//--------------------------------------

METHOD InsertRow( aArray ) CLASS TAsiento
  
  DEFAULT aArray := BlankRow()
    
  AEval( aArray, {|a,n| ::oBrw:aArrayData[::oBrw:nArrayAt,n] := a } )
    
  ::oBrw:Refresh()
    
Return Nil

//--------------------------------------

METHOD DeleteRow() CLASS TAsiento

  if ::oBrw:nLen > 1
     ADel( ::oBrw:aArrayData, ::oBrw:nArrayAt )
     ASize( ::oBrw:aArrayData, ::oBrw:nLen - 1 )
  else
     ::InsertRow( BlankRow() )
  endif
  
  ::oBrw:Refresh()
    
Return Nil

//--------------------------------------

METHOD ValidaMonto( nCol ) CLASS TAsiento
    
  if  nCol == 3 
	   if ::oBrw:aRow[_HABER] > 0
	      MsgStop("Ya Asigno un monto en HABER", Name_Empresa)
	      ::oBrw:aRow[nCol] = 0
				::oBrw:Refresh()
				::oBrw:MakeTotals()
	   endif
	elseif nCol == 4 
	   if ::oBrw:aRow[_DEBE] > 0
	      MsgStop("Ya Asigno un monto en DEBE", Name_Empresa)
	      ::oBrw:aRow[nCol] = 0
				::oBrw:Refresh()
				::oBrw:MakeTotals()
	   endif
	endif    
	
	::oBrw:Setfocus()
	  
Return TRUE 

//--------------------------------------

METHOD UpdateMonto( Value ) CLASS TAsiento
	
	::oBrw:aRow[::oBrw:nColSel] = Value
	::oBrw:RefreshCurrent()
	::oBrw:MakeTotals()
		
Return TRUE

//--------------------------------------

METHOD GoColDebito() CLASS TAsiento

  ::oBrw:nColSel = 3
  ::oBrw:RefreshCurrent() 
  ::oBrw:setFocus()
  
Return Nil

//--------------------------------------

METHOD BuscaDescripcion( oCol, nKey ) CLASS TAsiento
  Local oQry, cQuery
	Local oDlg, oBrw
	Local aCols
	Local oCodi  := ::oBrw:aCols[1]
	Local lEnter := FALSE
		
	if nKey != Nil .and. nKey >= 32	
		 oCol:VarPut( Chr(nKey) )
	endif	
	
  cQuery := "SELECT cuenta, nombre FROM " + TABLA_CATALOGO
  oQry   := oSrv:CreateQuery( cQuery )
  
  aCols := { { "nombre", "Nombre", Nil, oCol:nWidth-20, AL_LEFT } } 
  
  DEFINE DIALOG oDlg SIZE oCol:nWidth, 250 PIXEL TRUEPIXEL
  
  @ 0,0 XBROWSE oBrw SIZE 0,0 PIXEL OF oDlg; 
  	DATASOURCE oQry;
  	COLUMNS aCols
    
  WITH OBJECT oBrw
    :MyConfig( FALSE, FALSE )
    :Cargo       = cQuery
    :lHScroll    = .f.
    
    :bLDblClick  = <|| 
    								lEnter := TRUE
    								oCol:VarPut( oBrw:aCols[1]:Value )
    								oCodi:value = oQry:cuenta
    								oDlg:End()
    								Return Nil
    							 >	
    
    :bKeyChar    = <|k|
                    if k == VK_RETURN 
                      lEnter := TRUE
    								  oCol:VarPut( oBrw:aCols[1]:Value )
    								  oCodi:value = oQry:cuenta
    								  oDlg:End()
    								  Return 0
    								endif
    								UpdateDescripcion( oCol, oQry, oBrw, k)
    								Return 0
    							 >	
    							 
    :bKeyDown    = <|k| 
    								if k == VK_ESCAPE
    									 oCol:VarPut("")
    									 oCodi:value = ""
    								   oDlg:End()
    								endif  
    								Return 0
    							>	 
    							
    :CreateFromCode()
  END

  oDlg:bInit = {|| oCol:AnchorToCell(oDlg), DisableTitle(oDlg)}	
    
  ACTIVATE DIALOG oDlg; 
  	CENTERED
  	
  oQry:End()	
  
  if lEnter 
     ::ValidaCuenta( oCodi:value )
  endif

Return nil

//--------------------------------------------//

static Function UpdateDescripcion( oCol, oQry, oBrw, nKey )
  Local cSeek
  Local value := oCol:VarGet()
  
  do case   	
  case nKey >= 32 
     cSeek := value + Upper(Chr(nKey))
     DisplayDataQuery( cSeek, oQry, oBrw )
  case nKey == VK_BACK  	
  	 cSeek := Left(value, Len(value)-1) 
     DisplayDataQuery( cSeek, oQry, oBrw )
  endcase 
  
  oCol:VarPut( cSeek )
  
Return Nil

//--------------------------------------------//

static Function DisplayDataQuery( cSeek, oQry, oBrw )
	Local cQuery := oBrw:Cargo
		
	if !Empty( cSeek )
		 cQuery += " WHERE nombre LIKE '%" + AllTrim(cSeek) + "%'"
     oQry:cQuery = cQuery
          
     oQry:ReQuery()
     oQry:GoTop()
     	  	 
     oBrw:SetDolphin( oQry )  
     oBrw:Refresh()
  endif
    
Return Nil

//---------------------------------------

static Function BlankRow()
	Local aArray := Array(5)
	
	aArray[ _CUENTA ] 		:= Space(10)
	aArray[ _NOMBRE ]			:= Space(50)		
	aArray[ _DEBE   ]			:= 0
	aArray[ _HABER  ] 		:= 0
	aArray[ _DOCUMENTO ] 	:= Space(20)
		
Return aArray

//---------------------------------------

static Function ArrTipoAsiento()
Return  { { 1, "1= INGRESO" 	} ,;
					{ 2, "2= EGRESO" 		} ,;
					{ 3, "3= TRASPASO" 	} ,;	
					{ 4, "4= APERTURA" 	} ;
	      }
	      
//---------------------------------------

static Function ArrTipoTrans()
Return  { { 1, "1= COMPRAS" 	 } ,;
					{ 2, "2= VENTAS" 		 } ,;
					{ 3, "3= HONORARIOS" } ,;	
					{ 4, "4= OTROS" 	   } ;
	      }	   
	      
//---------------------------------------

METHOD GrabaAsiento() CLASS TAsiento
	Local lSucess := TRUE
	
	if Empty(::aItems[1,_CUENTA])
		 MsgStop("El primer Item esta Vacio", Name_Empresa)
		 lSucess := FALSE
	endif
	
	if ( Len(::aItems) % 2 ) != 0
		 MsgStop("Los Items del Asiento deben ser pares", Name_Empresa)
		 lSucess := FALSE
	endif

  if ::oBrw:Debe:nTotal != ::oBrw:Haber:nTotal
     MsgStop("Los totales Debe y Haber Deben ser iguales ", Name_Empresa)
		 lSucess := FALSE
	endif
	
	if lSucess
	   if MsgYesNo("Graba el Asiento Contable ?", Name_Empresa)
			  ::hVar["numero"] := GetNumeroAsiento()
				MsgProgress("Grabando Asiento Contable", "Espere ...", {|| ::GoBrabaAsiento() }  ) 
				::oQry:requery()						   
				Eval( ::oBtn[5]:bAction )
		 endif
	endif	
		
Return Nil

//---------------------------------------

METHOD GoBrabaAsiento() CLASS TAsiento
	
	oSrv:insertHash(  TABLA_MAESTRO,;
										{ "id_empresa"  => ::hVar["id_empresa"],;	
										  "numero" 			=> ::hVar["numero"],;	
											"year"        => ::hVar["year"],; 
											"month"       => ::hVar["month"],; 
											"fecha"       => ::hVar["fecha"],; 
											"tipo"				=> ::hVar["tipo"],;	
											"transaccion"	=> ::hVar["trans"],;	
											"rut" 				=> ::hVar["rut"],;	
											"tiporut"     => ::hVar["tiporut"],; 
											"namerut"     => ::hVar["namerut"],; 
											"id_docum"		=> ::hVar["id_docum"],;	
											"descripcion" => ::hVar["descripcion"] })	      
	
	::AddLineas()
	
Return Nil

//---------------------------------------

METHOD AddLineas() CLASS TAsiento
	Local i     
	Local nLen := Len(::aItems)
	      
	for i:= 1 to nLen   
	  if !Empty( ::aItems[i,_CUENTA] )
			oSrv:insertHash(  TABLA_LINEAS,;
											  { "numero"			=> ::hVar["numero"],;	
												  "cuenta"      => ::aItems[i,_CUENTA],; 
												  "namecuenta"  => ::aItems[i,_NOMBRE],; 
												  "debe"        => ::aItems[i,_DEBE ],; 
												  "haber"				=> ::aItems[i,_HABER],;
												  "documento" 	=> ::aItems[i,_DOCUMENTO] })	
			UpdateBalance( ::aItems[i], "NEW" )									        
		endif										  
	next i												
											
Return Nil												

//---------------------------------------

static Function GetNumeroAsiento()
  Local oError
	Local nNumero := 0

	TRY
  	oSrv:BeginTransaction()
  	nNumero := GetLastRecno()
    oSrv:CommitTransaction()
  CATCH oError
    oSrv:RollBack()
    oSrv:ShowError( oError )
  END
	
Return nNumero

//---------------------------------------

METHOD EliminaAsiento() CLASS TAsiento
	
	if MsgYesNo("Elimina el Asiento ?", Name_Empresa)
		 MsgProgress("Eliminando Asiento", "Espere ...", {|| ::GoDeleteAsiento()})	 
		 ::oQry:requery()						   
		 Eval( ::oBtn[5]:bAction )  
	endif
	
Return Nil

//---------------------------------------

METHOD GoDeleteAsiento() CLASS TAsiento
	Local cQuery
	Local oQry
	
	/** Lee Lineas Asiento y Actualiza el Balance **/
	TEXT INTO cQuery
		SELECT numero, namecuenta, cuenta, debe, haber
		FROM %1
		WHERE numero = %2
	ENDTEXT	
	
	cQuery := StrFormat( cQuery, TABLA_LINEAS, ClipValue2Sql(::hVar["numero"]) )
	
	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
	  UpdateBalance( { oQry:cuenta, oQry:namecuenta, oQry:debe, oQry:haber }, "DELETE" )
		oQry:skip()
	enddo
	
	oQry:End()
	/** final **/
	
	/** Elimina Asiento Maestro **/
	TEXT INTO cQuery
		DELETE FROM %1
		WHERE numero = %2
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_MAESTRO, ClipValue2Sql(::hVar["numero"]) )
	
	oSrv:Execute( cQuery )
	/** final **/
	
	/** Elimina Lineas Asiento **/
	TEXT INTO cQuery
		DELETE FROM %1
		WHERE numero = %2
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_LINEAS, ClipValue2Sql(::hVar["numero"]) )
	
	oSrv:Execute( cQuery )
	/** final **/
	
Return Nil	

//---------------------------------------

static Function GetLastRecno()
	Local cQuery
	Local oQry
	Local numero := 0
	
	TEXT INTO cQuery
		SELECT my_recno
		FROM %1
		ORDER BY my_recno DESC
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_MAESTRO ) 
	
	oQry = oSrv:CreateQuery( cQuery )
	
	if oQry:RecCount() > 0
	   numero = oQry:my_recno
	endif   
	
	oQry:End()
	
	numero++
	
Return numero
	      
//---------------------------------------

Function Crear_AsientoMaestro()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_empresa    int(10) NOT NULL DEFAULT 0,
  	  numero 				int(10) NOT NULL DEFAULT 0,
  		year          int(4) NOT NULL DEFAULT 0,
  		month         int(4) NOT NULL DEFAULT 0,
  		fecha         date,
  		tipo					int(2) NOT NULL DEFAULT 0,
  		transaccion		int(2) NOT NULL DEFAULT 0,
     	rut 					char(30) NOT NULL DEFAULT ' ',
     	tiporut       char(1) NOT NULL DEFAULT ' ',
     	namerut       char(80) NOT NULL DEFAULT ' ',
     	id_docum			int(2) NOT NULL DEFAULT 0,
     	descripcion   char(80) NOT NULL DEFAULT ' ',
     	my_recno 			int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX numero (numero),
      INDEX id_empresa (id_empresa),
      INDEX year (year),
      INDEX fecha (fecha),
      INDEX rut (rut)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_MAESTRO )
  
  oSrv:VerifyTable( TABLA_MAESTRO, cStruct ) 	
          
Return Nil	         

//---------------------------------------

Function Crear_AsientoLineas()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( numero 				int(10) NOT NULL DEFAULT 0,
  		cuenta				char(6) NOT NULL DEFAULT ' ',
  		namecuenta    char(60) NOT NULL DEFAULT ' ',
  		debe          decimal(14,2) NOT NULL DEFAULT 0,
  		haber					decimal(14,2) NOT NULL DEFAULT 0,
  		documento			char(40) NOT NULL DEFAULT ' ',
  		my_recno 			int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX numero (numero),
      INDEX cuenta (cuenta)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_LINEAS )
  
  oSrv:VerifyTable( TABLA_LINEAS, cStruct ) 	
          
Return Nil	         

// FINAL  	 