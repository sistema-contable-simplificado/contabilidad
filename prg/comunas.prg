// CATALOGO

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_COMUNAS  =>	oApp():Comunas
#xtranslate		TABLA_REGIONES =>	oApp():Regiones
#xtranslate		TABLA_CIUDADES =>	oApp():Ciudades

//---------------------------------------

Function Comunas( lBuscar )
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local bBlock
  Local bKey
  Local bClick
  Local h     := {=>}  
  Local lExit := FALSE
    
  h["id_comuna"] := 0
  
  DEFAULT lBuscar := FALSE
  
  bBlock := {|| h["id_comuna"]:= oQry:id_comuna, lExit:= TRUE, oDlg:End() }
  if lBuscar
     bClick := bBlock
     bKey   := bBlock
  else
     bClick	:= Nil
  	 bKey 	:= bBlock
  endif  
  
  TEXT INTO cQuery
  	SELECT 	a.id_comuna,
  					a.id_ciudad,
						a.nombre,        
						b.id_region,
						b.nombre AS nameciudad,      
						c.nombre AS nameregion,    	
						a.my_recno 		
  	FROM %1 a
  	LEFT JOIN %2 b ON a.id_ciudad = b.id_ciudad
  	LEFT JOIN %3 c ON b.id_region = c.id_region
  	ORDER BY a.id_comuna
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TABLA_COMUNAS,;
  														 TABLA_CIUDADES,;
  														 TABLA_REGIONES )	 
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE "TABLA COMUNAS" RESOURCE "xbrowse"; 
  	ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id_comuna" , "No. Comuna", Nil, 100, AL_LEFT },; 
  	{ "nombre"    , "Comuna"    , Nil, 200, AL_LEFT },;
  	{ "id_region" , "No. Region", Nil, 100, AL_LEFT },;
  	{ "nameregion", "Region"    , Nil, 200, AL_LEFT },;
  	{ "id_ciudad" , "No. Ciudad", Nil, 100, AL_LEFT },;
  	{ "nameciudad", "Ciudad"    , Nil, 200, AL_LEFT };
  }    
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyDown     = {|nKey| if(nKey==VK_RETURN, Eval(bKey), Nil)}
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= bClick
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @h, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()

Return h["id_comuna"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, h, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70

  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() )

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( LeaDatos( oQry, FALSE ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Reporte Comunas"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
    
Return Nil

//---------------------------------------

static Function LeaDatos( oQry, lNew )
	Local idComuna, idCiudad, idRegion
	Local nameComuna, nameCiudad, nameRegion 
	Local aNomCiudad, aReturn, aIndCiudad
	Local nPos
	Local oDlg
	Local oFont
	Local oSay			:= Array(2)
	Local oGet 			:= Array(4)
	Local oBtn    	:= Array(2)
	Local h       	:= { => }
	Local lExit   	:= FALSE
	Local lProcess	:= FALSE
	Local cTitle  	:= if(lNew, "AGREGAR COMUNAS", "MODIFICA UNA COMUNA")
	
	aReturn    := GetArrayCiudades()
	aNomCiudad := ArrTranspose(aReturn)[2]
	aIndCiudad := ArrTranspose(aReturn)[1]
	
	if lNew
	  idComuna	 := LastIdComuna()
		idCiudad   := aIndCiudad[1]
		idRegion   := GetRegionFromCiudad(idCiudad)
		nameCiudad := GetNameCiudad(idCiudad)
		nameComuna := Space(50)
		nameRegion := GetNameRegion(idRegion)     
	else
	  idComuna	 := oQry:id_comuna
		idCiudad   := oQry:id_ciudad
		idRegion   := oQry:id_region
		nameCiudad := GetNameCiudad(oQry:id_ciudad)
		nameComuna := oQry:nombre
		nameRegion := oQry:nameregion
	endif
		
	h["id_comuna"]   := idComuna
	h["id_ciudad"]   := idCiudad
	h["id_region"]   := idRegion
	h["ciudad"] 	 	 := nameCiudad
 	h["nombre"]      := nameComuna
	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "COMUNAS" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "No.Comuna :"
	REDEFINE SAY ID 101 OF oDlg PROMPT "No.Region :" 
	REDEFINE SAY ID 102 OF oDlg PROMPT "No.Ciudad :" 
	REDEFINE SAY ID 103 OF oDlg PROMPT "Nombre :"
	
	REDEFINE SAY oSay[1] ID 104 OF oDlg PROMPT nameRegion
	REDEFINE SAY oSay[2] ID 105 OF oDlg PROMPT StrZero(h["id_ciudad"],3)
  	 	
	REDEFINE GET oGet[1] VAR h["id_comuna"] ID 200 OF oDlg;
		WHEN FALSE
				          
	REDEFINE COMBOBOX oGet[2] VAR h["ciudad"]	ITEMS aNomCiudad ID 201 OF oDlg
		 oGet[2]:bChange = <||
		 										h["id_ciudad"] := aIndCiudad[ oGet[2]:nAt ]
		 										h["id_region"] := GetRegionFromCiudad( h["id_ciudad"] )
		 										oSay[1]:SetText( GetNameRegion( h["id_region"] ) )
		                    oSay[2]:SetText( StrZero(h["id_ciudad"],3) )
  		          				Return TRUE
  		          			 >	   		         
		
  REDEFINE GET oGet[3] VAR h["nombre"] ID 202 OF oDlg;
  	PICTURE "@!"
		
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
	
	ACTIVATE DIALOG oDlg;
		VALID lExit;
		ON INIT ( DisableSysMenuDlg(oDlg) )
		
  if lProcess
   	 if lNew
   	 	  MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_COMUNAS,;
  									{ "id_comuna"  		=> h["id_comuna" ],;
  										"id_ciudad"     => h["id_ciudad" ],;
  										"nombre"   		  => h["nombre"    ]} )
  									
  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_COMUNAS ,;
  									{ "id_ciudad"     => h["id_ciudad" ] ,;
  										"nombre"     	  => h["nombre"    ]},;
  	 									"my_recno = " +ClipValue2SQL(oQry:my_recno) )
  										
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )


  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_COMUNAS, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

Function SiIdComuna( id )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE id_comuna = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_COMUNAS, ClipValue2SQL(id) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

static Function LastIdComuna()
  Local oQry
  Local cQuery
  Local id := 0
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	ORDER BY id_comuna DESC
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_COMUNAS)	
    
  oQry = oSrv:CreateQuery( cQuery )

	if ( oQry:RecCount() > 0 )
  	id := oQry:id_comuna
	endif  	
  
  oQry:End()
   
  id++
  
Return id

//---------------------------------------

Function GetArrayComunas( id_ciudad )
	Local oQry
	lOCAL cQuery
	Local aArray := {}
	
	if id_ciudad == Nil
		TEXT INTO cQuery
			SELECT 	a.id_comuna, 
							a.id_ciudad, 
							b.id_region, 
							a.nombre
			FROM %1 a
			LEFT JOIN %2 b ON a.id_ciudad = b.id_ciudad
			ORDER BY id_comuna
		ENDTEXT
	else
		TEXT INTO cQuery
		  SELECT 	a.id_comuna, 
							a.id_ciudad, 
							b.id_region, 
							a.nombre
			FROM %1 a
			LEFT JOIN %2 b ON a.id_ciudad = b.id_ciudad
			WHERE id_ciudad = %3
			ORDER BY id_comuna
		ENDTEXT
	endif	
	
	cQuery := StrFormat( cQuery, TABLA_COMUNAS,; 
															 TABLA_CIUDADES,; 
															 ClipValue2SQL(id_ciudad) )

	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
		AAdd( aArray, { oQry:id_comuna, oQry:id_ciudad, oQry:id_region, oQry:nombre } )
		oQry:Skip()
	enddo
	
	oQry:End()
	
	if Len(aArray) == 0
		 AAdd( aArray, { 0, 0, 0, "No hay Comunas" } )
	endif
	
Return aArray	

//---------------------------------------

Function Crear_Comunas()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_comuna	  int(10) NOT NULL DEFAULT 0,
     	id_ciudad   int(10) NOT NULL DEFAULT 0,	
     	nombre    	char(80) NOT NULL DEFAULT ' ',
     	my_recno 		int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX id_comuna(id_comuna),
      INDEX nombre (nombre)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_COMUNAS )
  
  oSrv:VerifyTable( TABLA_COMUNAS, cStruct ) 	
          
Return Nil

// FINAL