// empresas

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_PROVEDOR     =>	oApp():Provedor
#xtranslate		TABLA_CATALOGO 		=>	oApp():Catalogo
#xtranslate		TABLA_REGIONES 		=>	oApp():Regiones
#xtranslate		TABLA_CIUDADES 		=>	oApp():Ciudades
#xtranslate		TABLA_COMUNAS  		=>	oApp():Comunas

//---------------------------------------

static bBlock
  
//---------------------------------------


Function Provedores( lBuscar )
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local bKey
  Local bClick
  Local cTitle := "PROVEDORES DE LA EMPRESA : " + StrZero(oApp():Session["id_empresa"],3) 
  Local h      := {=>}  
  Local lExit  := FALSE
    
  h["rut"] := Space(10)
  
  DEFAULT lBuscar := FALSE
  
  bBlock := {|| h["rut"]:= oQry:rut, lExit:= TRUE, oDlg:End() }
  if lBuscar
     bClick := bBlock
     bKey   := bBlock
  else
     bClick	:= Nil
  	 bKey 	:= bBlock
  endif  
			
  TEXT INTO cQuery
  	SELECT 	a.id, 	
						a.rut, 				
						a.nombre,    	
						a.id_empresa,
						a.cuenta,
						e.nombre AS namecuenta,
						a.direccion1,	
						a.direccion2,	
						a.id_region,		
						b.nombre AS nameregion,
						a.id_ciudad,		
						c.nombre AS nameciudad,
						a.id_comuna,		
						c.nombre AS namecomuna,
						a.telefono,		
						a.celular,			
						a.email,				
						a.my_recno    
		FROM %1 a
		LEFT JOIN %2 b ON a.id_region = b.id_region
		LEFT JOIN %3 c ON a.id_ciudad = c.id_ciudad
		LEFT JOIN %4 d ON a.id_comuna = d.id_comuna
		LEFT JOIN %5 e ON a.cuenta = e.cuenta
  	ORDER BY a.id
  ENDTEXT	
  
  cQuery := StrFormat(  cQuery, TABLA_PROVEDOR,;
   															TABLA_REGIONES,;
   															TABLA_CIUDADES,;
   															TABLA_COMUNAS,;
   															TABLA_CATALOGO )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE cTitle RESOURCE "xbrowse"; 
  	ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id" 					, "Codigo"	  	, Nil,  80, AL_LEFT },; 
  	{ "id_empresa"  , "Empresa" 		, Nil, 100, AL_LEFT },;
  	{ "rut"      		, "Rut" 	  		, Nil, 100, AL_LEFT },;
  	{ "nombre"  		, "Nombre"    	, Nil, 300, AL_LEFT },;
  	{ "cuenta"  		, "Cuenta"    	, Nil, 100, AL_LEFT },;
  	{ "nameregion"	, "Region" 			, Nil, 120, AL_LEFT },; 
  	{ "nameciudad"	, "Ciudad"    	, Nil, 120, AL_LEFT },; 
   	{ "namecomuna"	, "Comuna"			, Nil, 120, AL_LEFT },;
   	{ "email"  	  	, "Email"				, Nil, 120, AL_LEFT } ;
  }    
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyDown     = {|nKey| if(nKey==VK_RETURN, Eval(bKey), Nil)}
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bKeyDown     = {|nKey| if(nKey == VK_RETURN, Eval(oBrw:bLDblClick), Nil)}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= {|| h["rut"] := oQry:rut, lExit:= TRUE, oDlg:End() }
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()
  
Return h["rut"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70
  
  DEFINE BUTTON OF oBar; 
  	PROMPT "Seleccionar"; 
  	RESOURCE aBitmap[ BMP32_YES ];
  	ACTION ( Eval(bBlock) )
  	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() );
    GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( if(oQry:RecCount()>0,LeaDatos(oQry, FALSE),Nil), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( Fr3_Provedor( FR3_SHOW ), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
     
Return Nil

//---------------------------------------

// http://forums.fivetechsupport.com/viewtopic.php?f=6&t=31223

static Function LeaDatos( oQry, lNew )
	Local oDlg
	Local oFont
	Local nPos
	Local oSay				:= Array(4)
	Local oGet 				:= Array(13)
	Local oBtn    		:= Array(2)
	Local h       		:= { => }
	Local aComunas  	:= GetArrayComunas()
	Local aNomComuna  := ArrTranspose(aComunas)[4] 
	Local aIndComuna  := ArrTranspose(aComunas)[1] 
	Local aIndCiudad  := ArrTranspose(aComunas)[2] 
	Local aIndRegion  := ArrTranspose(aComunas)[3]
	Local lExit   		:= FALSE
	Local lProcess		:= FALSE
	Local cTitle  		:= if(lNew, "AGREGAR PROVEDOR", "MODIFICA PROVEDOR")
		
	h["id"]   		 := if(lNew, LastIdCodigo(), oQry:id)
	h["id_empresa"]:= if(lNew, oApp():Session["id_empresa"], oQry:id_empresa)
	h["rut"]    	 := if(lNew, Space(30), oQry:rut)
	h["nombre"] 	 := if(lNew, Space(60), oQry:nombre)
	h["cuenta"]    := if(lNew, Space(10), oQry:cuenta)
	h["namecuenta"]:= if(lNew, "", oQry:namecuenta)
	h["dir1"] 		 := if(lNew, Space(80), oQry:direccion1)
 	h["dir2"] 		 := if(lNew, Space(80), oQry:direccion2)
 	h["idcomuna"]  := if(lNew, aIndComuna[1], oQry:id_comuna)
 	 	
 	if lNew
 		h["comuna"] := aNomComuna[1]
 	else	
 		nPos := AScan(aIndComuna, oQry:id_comuna)
 		if nPos > 0
 		   h["comuna"] := aNomComuna[nPos]
 		else
 		   h["comuna"] := aNomComuna[1]
 		endif
 	endif	   
 	
 	h["idregion"]  := if(lNew, aIndRegion[1], oQry:id_region)
 	h["region"] 	 := GetNameRegion(h["idregion"])
 	h["idciudad"]  := if(lNew, aIndCiudad[1], oQry:id_ciudad)
 	h["ciudad"] 	 := GetNameCiudad(h["idciudad"])
 	
 	h["tel"]    	 := if(lNew, Space(30), oQry:telefono)
 	h["cel"]  		 := if(lNew, Space(30), oQry:celular)
 	h["email"]  	 := if(lNew, Space(40), oQry:email)
 	 	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "CLIENTE_PROVEDOR" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "ID :"	
	REDEFINE SAY ID 101 OF oDlg PROMPT "EMPRESA :"	
  REDEFINE SAY ID 102 OF oDlg PROMPT "RUT :"						
  REDEFINE SAY ID 103 OF oDlg PROMPT "NOMBRE :"
  REDEFINE SAY ID 104 OF oDlg PROMPT "CUENTA :"
  REDEFINE SAY ID 105 OF oDlg PROMPT "DIRECCION 1:"	
  REDEFINE SAY ID 106 OF oDlg PROMPT "DIRECCION 2:"	
  REDEFINE SAY ID 107 OF oDlg PROMPT "REGION :"			
  REDEFINE SAY ID 108 OF oDlg PROMPT "CIUDAD :"			
  REDEFINE SAY ID 109 OF oDlg PROMPT "COMUNA :"			
  REDEFINE SAY ID 110 OF oDlg PROMPT "TELEFONO :"		
  REDEFINE SAY ID 111 OF oDlg PROMPT "CELULAR:"
  REDEFINE SAY ID 112 OF oDlg PROMPT "EMAIL :"
  REDEFINE SAY oSay[1] ID 113 OF oDlg PROMPT h["namecuenta"]
  REDEFINE SAY oSay[2] ID 114 OF oDlg PROMPT StrZero( h["idciudad"], 3 )
  REDEFINE SAY oSay[3] ID 115 OF oDlg PROMPT StrZero( h["idcomuna"], 3 )
  REDEFINE SAY oSay[4] ID 116 OF oDlg PROMPT StrZero( h["idcomuna"], 3 )
  		 	
	REDEFINE GET oGet[1] VAR h["id"] ID 200 OF oDlg;
		WHEN lNew
		oGet[1]:bValid = <|oGet| 
		                  if SiIdProvedor(oGet:varget()) 
												MsgStop("Empresa YA Existe", Name_Empresa)
												Return FALSE
											endif	
											Return TRUE
										>	
		 	
	REDEFINE GET oGet[2] VAR h["id_empresa"] ID 201 OF oDlg;
		WHEN FALSE
		
	REDEFINE GET oGet[3] VAR h["rut"]	ID 202 OF oDlg;
		PICTURE "@!"
		oGet[3]:cargo  = h["rut"]
		oGet[3]:bValid = <|oGet|
		                  Local value := oGet:varget()
		                  
		                  if oGet:cargo == value
		                     Return TRUE
		                  endif   
		                  
	  									if SiRutProvedor( value )
	  									   MsgStop("RUT Ya Existe", Name_Empresa)
	  										 Return FALSE
	  									else
	  									   if Valida_RUT( @value )
	  									      oGet:varPut(value)
	  									      oGet:Refresh()
	  									      Return TRUE
	  									   else
	  									      MsgStop("Error en Digito de control " + Right(value,1), Name_Empresa)
	  									      Return FALSE   
	  									   endif
	  									endif
	  									
	  									Return TRUE
	  								>	
										
	REDEFINE GET oGet[4] VAR h["nombre"]	ID 203 OF oDlg PICTURE "@!"
	
	REDEFINE GET oGet[5] VAR h["cuenta"]	ID 204 OF oDlg;
		PICTURE "@R 9-9-9-9-9-9";
		BITMAP aBitmap[ BMP16_SEARCH ]
		oGet[5]:bValid = <|oGet|
		                    Local cuenta := oGet:varGet()
		                    Local lOk    := TRUE
		                    
												if !SiCuentaCatalogo( cuenta )
												   MsgStop("Cuenta contable No Existe", Name_Empresa)
												   h["namecuenta"] := ""
												   lOk := FALSE
												else
													 h["namecuenta"] := GetNameCuentaCatalogo( Cuenta )   
												endif
												oSay[1]:SetText( h["namecuenta"] )
												
												Return lOk
											>	
											
		oGet[5]:bAction = <||
		                    oGet[5]:varPut( Catalogo(TRUE) )
		                    oGet[5]:Refresh()
		                    oSay[1]:SetText( GetNameCuentaCatalogo(oGet[5]:varGet()) )
												Return TRUE
											>												
	
	REDEFINE GET oGet[6] VAR h["dir1"] 		ID 205 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[7] VAR h["dir2"] 		ID 206 OF oDlg PICTURE "@!"
	
	REDEFINE GET oGet[8] VAR h["region"] ID 207 OF oDlg WHEN FALSE
	REDEFINE GET oGet[9] VAR h["ciudad"] ID 208 OF oDlg WHEN FALSE
	
	REDEFINE COMBOBOX oGet[10] VAR h["comuna"] ITEMS aNomComuna ID 209 OF oDlg
		oGet[10]:bChange = <||
		                     Local item := oGet[10]:nAt
		                     
		                     h["idregion"] := aIndRegion[item]
		                     h["idciudad"] := aIndCiudad[item] 
		                     h["idcomuna"] := aIndComuna[item] 
		                     
		                     oSay[2]:SetText( StrZero(h["idregion"],3) )
		                     oGet[8]:varPut( GetNameRegion(h["idregion"]) )
		                     oGet[8]:Refresh()
		                     
		                     oSay[3]:SetText( StrZero(h["idciudad"],3) )
		                     oGet[9]:varPut( GetNameCiudad(h["idciudad"]) )
		                     oGet[9]:Refresh()
		                     
		                     oSay[4]:SetText( StrZero(h["idcomuna"],3) )
		                     
		                     Return TRUE
		                    > 
		                     		
  REDEFINE GET oGet[11] VAR h["tel"] 		ID 210 OF oDlg
	REDEFINE GET oGet[12] VAR h["cel"] 		ID 211 OF oDlg
	REDEFINE GET oGet[13] VAR h["email"] 	ID 212 OF oDlg
	
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
    
  oDlg:bInit = <||
    							DisableSysMenuDlg(oDlg)
    							if lNew
    								 oGet[1]:Setfocus()
    							else	 
	                   oGet[2]:Setfocus()
	                endif
	                Return Nil
	               >
	                   
	ACTIVATE DIALOG oDlg;
		VALID lExit
		
  if lProcess
   	 if lNew
   	 	  MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_PROVEDOR,;
  									{ "id" 	 				 => h["id"],;
											"id_empresa"   => h["id_empresa"],;
											"rut" 				 => h["rut"],;
											"nombre"    	 => h["nombre"],;
											"cuenta"       => h["cuenta"],;
											"direccion1"	 => h["dir1"],;
											"direccion2"	 => h["dir2"],;
											"id_region"		 => h["idregion"],;
											"id_ciudad"		 => h["idciudad"],;
											"id_comuna"		 => h["idcomuna"],;
											"telefono"		 => h["tel"],;
											"celular"			 => h["cel"],;
											"email"				 => h["email"] } )
  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_PROVEDOR,;
  									{ "nombre"    	 => h["nombre"],;
  									  "cuenta"       => h["cuenta"],;
											"direccion1"	 => h["dir1"],;
											"direccion2"	 => h["dir2"],;
											"id_region"		 => h["idregion"],;
											"id_ciudad"		 => h["idciudad"],;
											"id_comuna"		 => h["idcomuna"],;
											"telefono"		 => h["tel"],;
											"celular"			 => h["cel"],;
											"email"				 => h["email"] },;
  									"my_recno = " + ClipValue2SQL(oQry:my_recno) )
  										
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )
  
  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_PROVEDOR, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

Function SiIdProvedor( id )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT id, rut FROM %1
  	WHERE id = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_PROVEDOR, ClipValue2SQL(id) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

Function SiRutProvedor( rut )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT id, rut FROM %1
  	WHERE rut = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_PROVEDOR, ClipValue2SQL(rut) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

Function GetNameRutProvedor( rut )
  Local oQry
  Local cQuery
  Local cName := ""
  
  TEXT INTO cQuery
  	SELECT id, rut, nombre FROM %1
  	WHERE rut = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_PROVEDOR, ClipValue2SQL(rut) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  if oQry:RecCount() > 0
  	 cName := oQry:nombre
  endif
  
  oQry:End()
   
Return cName

//---------------------------------------

Function GetCuentaProvedor( rut )
  Local oQry
  Local cQuery
  Local cuenta := ""
  
  TEXT INTO cQuery
  	SELECT id, rut, cuenta FROM %1
  	WHERE rut = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_PROVEDOR, ClipValue2SQL(rut) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  if oQry:RecCount() > 0
  	 cuenta := oQry:cuenta
  endif	 
  
  oQry:End()
   
Return cuenta

//---------------------------------------

static Function LastIdCodigo()
  Local oQry
  Local cQuery
  Local id := 0
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	ORDER BY id DESC
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_PROVEDOR)	
    
  oQry = oSrv:CreateQuery( cQuery )

	if ( oQry:RecCount() > 0 )
  	id := oQry:id
	endif  	
  
  oQry:End()
   
  id++
  
Return id

//---------------------------------------

Function Fr3_Provedor( lDesigner )
  Local oFr
  Local oQry
  Local cQuery
  Local cFileFr3 := PATH_FR3 + "provedor.fr3"
    
  DEFAULT lDesigner := FR3_SHOW

	TEXT INTO cQuery
  	SELECT * FROM %1
  	ORDER BY nombre
  ENDTEXT
  
  cQuery := StrFormat( cQuery, TABLA_PROVEDOR )
    
  oQry = oSrv:CreateQuery( cQuery )            
  
  oFr:= FrReportManager():New( FILE_DLL )
  oFr:LoadLangRes( FILE_XML )
  oFr:SetIcon( ICONFR3 )
  oFr:SetTitle( MSGTITULO_FASTREPORT )
  
  oFr:SetUserDataSet( "provedor",;
  										MySqlFields( oQry ),;
                      {|| oQry:GoTop()},; 
                      {|| oQry:Skip(1)},; 
                      {|| oQry:Skip(-1)},;
                      {|| oQry:Eof() },;
                      {|aField| oQry:FieldGet( aField )} ) 
    
  if( File(cFileFr3), oFr:LoadFromFile(cFileFr3), oFr:SetFileName(cFileFr3) )
  
  // Inclusion de Variables
  oFr:AddVariable( "Mis variables", "EMPRESA", "'"+Name_Empresa+"'")
  oFr:AddVariable( "Mis variables", "FECHA", "'"+DToC(Date())+"'" )

  // Inclusion de Funciones
  AddFunctionsFrm3( oFr )
    
  if lDesigner
     oFr:DesignReport()
     oFr:SaveToFile( cFileFr3 )
  else
     oFr:SetEventHandler("Report", "OnProgressStart",; 
                         {|x,y| MsgProgress("Espere Por favor...", "Creando Reporte")})
     oFr:ShowReport()
  endif

  oFr:ClearDataSets()
  oFr:DestroyFR()

  oQry:End()
  
Return Nil

//---------------------------------------

Function Crear_Provedores()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id						int(10) NOT NULL DEFAULT 0,
  		id_empresa		int(4) NOT NULL DEFAULT 0,
     	rut 					char(30) NOT NULL DEFAULT ' ',
     	nombre    		char(40) NOT NULL DEFAULT ' ',
     	cuenta        char(6) NOT NULL DEFAULT ' ', 
     	direccion1		char(80) NOT NULL DEFAULT ' ',
     	direccion2		char(80) NOT NULL DEFAULT ' ',
     	id_region			int(10) NOT NULL DEFAULT 0,
     	id_ciudad			int(10) NOT NULL DEFAULT 0,
     	id_comuna			int(10) NOT NULL DEFAULT 0,
     	telefono			char(30) NOT NULL DEFAULT ' ',
     	celular				char(30) NOT NULL DEFAULT ' ',
     	email					char(40) NOT NULL DEFAULT ' ',
     	my_recno 			int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX id (id),
      INDEX rut (rut),
      INDEX cuenta (cuenta)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_PROVEDOR )
  
  oSrv:VerifyTable( TABLA_PROVEDOR, cStruct ) 	
          
Return Nil

// FINAL