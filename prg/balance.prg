#include  "fivewin.ch"
#include  "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_BALANCE 	=>	oApp():Balance
#xtranslate		TABLA_CATALOGO  =>	oApp():Catalogo
#xtranslate		TABLA_MAESTRO		=>	oApp():AsientoMaestro
#xtranslate		TABLA_LINEAS		=>	oApp():AsientoLIneas
#xtranslate		TABLA_TIPODOCUM =>	oApp():TipoDocum

//---------------------------------------

Function UpdateBalance( aDatos, cType )
	Local cQuery
	Local oQry
	Local cMonth
	Local lExiste
	Local lProcess
	local aSaldos  := { 0, 0 }
	
	DEFAULT cType := "NEW"
		
	TEXT INTO cQuery 
		SELECT count(*) AS numregis
		FROM %1 
		WHERE id_empresa = %2 AND cuenta = %3 AND year = %4
		LIMIT 1
	ENDTEXT 
	
	cQuery := StrFormat( cQuery, TABLA_BALANCE,; 
															 ClipValue2Sql(oApp():Session["id_empresa"]),;
															 ClipValue2Sql(aDatos[_CUENTA]),;
															 ClipValue2Sql(oApp():Session["year"]) )
	
	oQry = oSrv:CreateQuery( cQuery )
	if oQry:RecCount() > 0
		 lExiste := if(oQry:numregis > 0, TRUE, FALSE)
	endif	 	
	oQry:End()
	
	cMonth := StrZero(oApp():Session["month"],2)
		
	// Nuevo Asiento Contable	
	if cType == "NEW"	
		
		if lExiste	
		
			TEXT INTO cQuery
	  		UPDATE %1 
	  		SET deb_%9 = deb_%9 + $7 , 
	  		    hab_%9 = hab_%9 + $8 
    		WHERE id_empresa = $2 AND year = $3 AND cuenta = $5
    		LIMIT 1
    	ENDTEXT	
    
  	else
  
      aSaldos := GetSaldosFinales( aDatos ) 
  
  		TEXT INTO cQuery 
    		INSERT INTO %1 
    		SET id_empresa = $2, 
    		    year    = $3, 
    		    month   = $4, 
    		    cuenta  = $5, 
    		    nombre  = $6, 
    	    	deb_ini = $10, 
    	    	hab_ini = $11, 
    	    	deb_%9  = $7, 
    	    	hab_%9  = $8 
    	ENDTEXT 
    
  	endif  
  	
  	lProcess := TRUE
  
  // Elimina Asiento Contable		
  else
  
    if lExiste	
		
			TEXT INTO cQuery
	  		UPDATE %1 
	  		SET deb_ini = deb_ini - $7 , 
	  		    hab_ini = hab_ini - $8 ,  
	  		    deb_%9  = deb_%9 - $7 , 
	  		    hab_%9  = hab_%9 - $8 
    		WHERE id_empresa = $2 AND year = $3 AND cuenta = $5
    		LIMIT 1
    	ENDTEXT		
    	
    	lProcess := TRUE
    	
    else
    
      lProcess := FALSE	
    	
    endif	
    	
  endif  	
	
	// Procesa el query
	if lProcess
	
		 cQuery := BindParams( cQuery, TABLA_BALANCE,;                // 1 
															 		 oApp():Session["id_empresa"],;	// 2 
															 		 oApp():Session["year"],;				// 3 
															 		 oApp():Session["month"],;			// 4 
															 		 aDatos[_CUENTA],;							// 5 
															 		 aDatos[_NOMBRE],;							// 6 
															 		 aDatos[_DEBE],;								// 7 
															 		 aDatos[_HABER],;								// 8 
															 		 cMonth,; 											// 9 
															 		 aSaldos[1],; 									// 10 
															 		 aSaldos[2] ) 									// 11 
															 		
		 oSrv:Execute( cQuery )															 		
			
	endif		
			
Return Nil

//---------------------------------------

static function GetSaldosFinales( aDatos )
	Local cQuery
	Local oQry
	Local YearPrev := oApp():Session["year"] - 1
	Local aSaldos  := { 0, 0 }
	
	TEXT INTO cQuery
		SELECT id_empresa, year, cuenta, deb_12, hab_12
		FROM %1
		WHERE id_empresa = %2 AND cuenta = %3 AND year = %4
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_BALANCE,; 
															 ClipValue2Sql(oApp():Session["id_empresa"]),;
															 ClipValue2Sql(aDatos[_CUENTA]),;
															 ClipValue2Sql(YearPrev) )  
															 
  oQry = oSrv:CreateQuery( cQuery )
  
  if oQry:RecCount() > 0														 
  	 aSaldos := { oQry:deb_12, oQry:hab_12 }	
  endif
	
	oQry:End()
	
Return aSaldos

//---------------------------------------

Function Fr3_LibroBalance( lDesigner )
  Local oFr
  Local oQry
  Local cQuery
  Local cNameRut
  Local cFileFr3 := PATH_FR3 + "balance.fr3"
      
  cNameRut := AllTrim(oApp():Session["nombre"]) + " " + ;
  						AllTrim(oApp():Session["paterno"]) + " " + ;
  						AllTrim(oApp():Session["materno"]) 
    
  oSrv:Execute( "SET @debe  := 0;" )
	oSrv:Execute( "SET @haber := 0;" )
	oSrv:Execute( "SET @result:= 0;" )
	oSrv:Execute( "SET @nivel := 0;" )
	
	TEXT INTO cQuery
			SELECT  a.cuenta,
  			  		a.nombre,
  			  		@nivel := (SELECT nivel 
    	     							 FROM %2 
    	     							 WHERE a.cuenta = cuenta
    	     							 LIMIT 1) AS nivel,
              @debe := (SELECT SUM(deb_ini+deb_01+deb_02+deb_03+deb_04+deb_05+deb_06+
                                   deb_07+deb_08+deb_09+deb_10+deb_11+deb_12)
												FROM %1
												WHERE cuenta = a.cuenta AND year = %3
												GROUP BY cuenta) AS sumdebe,
    	    		@haber := (SELECT SUM(hab_ini+hab_01+hab_02+hab_03+hab_04+hab_05+hab_06+
    	    		                      hab_07+hab_08+hab_09+hab_10+hab_11+hab_12)
                         FROM %1
                         WHERE cuenta = a.cuenta AND year = %3
												 GROUP BY cuenta) AS sumhaber,
							@result := @debe - @haber AS saldo,
							if(@result>0, @result, 0) AS deudor,
    	    		if(@result>0, 0, if(@result>0, @result, @result*-1)) AS acreedor,
							if(@nivel=1, if(@result>0, @result, @result*-1), 0) AS activo,
							if(@nivel=2, if(@result>0, @result, @result*-1), 0) AS pasivo,
							if(@nivel=3, if(@result>0, @result, @result*-1), 0) AS perdidas,
							if(@nivel=4, if(@result>0, @result, @result*-1), 0) AS ganancias					 
			FROM %1 AS a
			WHERE a.year = %3
			GROUP BY a.cuenta;
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TABLA_BALANCE,;
                               TABLA_CATALOGO,;
                               ClipValue2Sql(oApp():Session["year"]) )
    
  oQry = oSrv:CreateQuery( cQuery, lDesigner )
            
  oFr:= FrReportManager():New( FILE_DLL )
  oFr:LoadLangRes( FILE_XML )
  oFr:SetIcon( ICONFR3 )
  oFr:SetTitle( MSGTITULO_FASTREPORT )
      
  // Maestro Asiento
  oFr:SetUserDataSet( "balance",;
  										MySqlFields( oQry ),;
                      {|| oQry:GoTop()},; 
                      {|| oQry:Skip(1)},; 
                      {|| oQry:Skip(-1)},;
                      {|| oQry:Eof() },;
                      {|aField| oQry:FieldGet( aField )} ) 
      	                    
  if( File(cFileFr3), oFr:LoadFromFile(cFileFr3), oFr:SetFileName(cFileFr3) )                      
    
  // Inclusion de Variables
  oFr:AddVariable( "Mis variables", "EMPRESA_NOMBRE", "'"+oApp():Session["razon"]+"'" )
  oFr:AddVariable( "Mis variables", "RUT", "'"+oApp():Session["rut"]+"'" )
  oFr:AddVariable( "Mis variables", "NOMBRE", "'"+cNameRut+"'" )
  oFr:AddVariable( "Mis variables", "ID_EMPRESA", oApp():Session["id_empresa"] )
  oFr:AddVariable( "Mis variables", "YEAR", oApp():Session["year"] )
  oFr:AddVariable( "Mis variables", "MONTH", oApp():Session["month"] )
  oFr:AddVariable( "Mis variables", "GIRO", "'"+oApp():Session["giro"]+"'" )
  oFr:AddVariable( "Mis variables", "FECHA", Date() )
  
  // Inclusion de Funciones
  AddFunctionsFrm3( oFr ) 
  
  if lDesigner
     oFr:DesignReport()
     oFr:SaveToFile( cFileFr3 )
  else
     oFr:SetEventHandler("Report", "OnProgressStart", {|x, y| MsgProgress("Espere Por favor...", "Creando Reporte")})
     oFr:ShowReport()
  endif

  oFr:ClearDataSets()
  oFr:DestroyFR()
  
  oQry:End()
  
Return Nil

//---------------------------------------

Function Crear_Balance()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_empresa	int(4) NOT NULL DEFAULT 0,
  		year        int(4) NOT NULL DEFAULT 0,
  		month       int(4) NOT NULL DEFAULT 0,
  		cuenta      char(6) NOT NULL DEFAULT '',
     	nombre    	char(80) NOT NULL DEFAULT ' ',
     	deb_ini     decimal(14,2) NOT NULL DEFAULT 0,
     	hab_ini     decimal(14,2) NOT NULL DEFAULT 0,
     	deb_01      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_01      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_02      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_02      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_03      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_03      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_04      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_04      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_05      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_05      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_06      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_06      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_07      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_07      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_08      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_08      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_09      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_09      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_10      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_10      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_11      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_11      decimal(14,2) NOT NULL DEFAULT 0,
     	deb_12      decimal(14,2) NOT NULL DEFAULT 0,
     	hab_12      decimal(14,2) NOT NULL DEFAULT 0,
     	my_recno 		int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX id_empresa (id_empresa),
      INDEX cuenta (cuenta),
      INDEX nombre (nombre)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_BALANCE )
  
  oSrv:VerifyTable( TABLA_BALANCE, cStruct ) 	
          
Return Nil

// FINAL