// CATALOGO

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_TIPODOCUM =>	oApp():TipoDocum

//---------------------------------------

Function Tipo_Documento()
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local h     := {=>}  
  Local lExit := FALSE
    
  h["tipo"] := 0
  
  TEXT INTO cQuery
  	SELECT 	id_docum,
  					nombre,
						my_recno 		
  	FROM %1 
  	ORDER BY id_docum
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TABLA_TIPODOCUM )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE "TABLA TIPO DOCUMENTO";
  	RESOURCE "xbrowse" ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id_docum", "Tipo"  , Nil, 100, AL_RIGHT },; 
    { "nombre",   "Nombre", Nil, 400, AL_LEFT  } ;
   }    
 
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= {|| h["tipo"]:= oQry:id_docum, lExit:= TRUE, oDlg:End() }
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @h, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()

Return h["tipo"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, h, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70

  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() )

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( LeaDatos( oQry, FALSE ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Reporte Regiones"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
  
Return Nil

//---------------------------------------

// http://forums.fivetechsupport.com/viewtopic.php?f=6&t=31223

static Function LeaDatos( oQry, lNew )
	Local oDlg
	Local oFont
	Local oGet 		:= Array(2)
	Local oBtn    := Array(2)
	Local h       := { => }
	Local lExit   := FALSE
	Local lProcess:= FALSE
	Local cTitle  := if(lNew, "AGREGAR TIPO", "MODIFICA TIPO")
	
	h["tipo"] 	:= if(lNew, 0, oQry:id_docum)
	h["nombre"] := if(lNew, Space(60), oQry:nombre)
	 	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "REGIONES" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "Tipo   :"
	REDEFINE SAY ID 101 OF oDlg PROMPT "Nombre :"
	 	
	REDEFINE GET oGet[1] VAR h["tipo"] ID 200 OF oDlg;
	  PICTURE "99999"
		oGet[1]:bValid = <|oGet|
											if SiTipoDocum( oGet:varGet() )
											   MsgStop("Tipo de Documento YA Existe", Name_Empresa)
											   Return FALSE
											endif
											Return TRUE
										>	
		
	REDEFINE GET oGet[2] VAR h["nombre"]	  ID 201 OF oDlg;
		PICTURE "@!"
    	
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
	
	ACTIVATE DIALOG oDlg;
		VALID lExit;
		ON INIT ( DisableSysMenuDlg(oDlg) )
		
  if lProcess
   	 if lNew
   	    MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_TIPODOCUM,;
  									{ "id_docum"  => h["tipo" ],;
  							  		"nombre"  	=> h["nombre"] } )
  			  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_TIPODOCUM ,;
  									{ "id_docum"  => h["tipo" ],;
  										"nombre"    => h["nombre"] } ,;
  								   	"my_recno = " +ClipValue2SQL(oQry:my_recno) )
  										  
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )
  
  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_TIPODOCUM, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

Function SiTipoDocum( tipo )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE id_docum = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_TIPODOCUM, ClipValue2SQL(tipo) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

Function ArrTipoDocumento()
	Local oQry
	lOCAL cQuery
	Local aArray := {}
	
	TEXT INTO cQuery
		SELECT  id_docum, 
						nombre,
						CONCAT(LPAD(id_docum,3,'0'),'=',nombre) AS item
		FROM %1
		ORDER BY id_docum ASC
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_TIPODOCUM )

	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
		AAdd( aArray, { oQry:id_docum, oQry:item } )
		oQry:Skip()
	enddo
	
	oQry:End()
	
	if Len(aArray) == 0
		 AAdd( aArray, { 0, "000=No hay Tipos" } )
	endif
	
Return aArray	

//---------------------------------------

Function GetNameTipoDocum( id, lConcat )
	Local oQry
	Local cQuery
	Local cName := ""
	
	DEFAULT lConcat := FALSE
		
	TEXT INTO  cQuery
		SELECT id_docum, 
					 nombre,
					 CONCAT(LPAD(id_docum,3,'0'),'=',nombre) AS item
		FROM %1
		WHERE id_docum = %2
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery,TABLA_TIPODOCUM, ClipValue2Sql(id) )

	oQry = oSrv:CreateQuery( cQuery )
	
	if oQry:RecCount() > 0
	  if lConcat
	  	 cName := oQry:item	 
		else
			 cName := oQry:nombre	   
		endif	  
	endif
	
	oQry:End()
	
Return cName

//---------------------------------------

Function Crear_TipoDocumento()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_docum	int(10) NOT NULL DEFAULT 0,
     	nombre    char(80) NOT NULL DEFAULT ' ',
     	my_recno	int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX nombre (nombre)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_TIPODOCUM )
  
  oSrv:VerifyTable( TABLA_TIPODOCUM, cStruct ) 	
          
Return Nil

// FINAL