// CATALOGO

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_CIUDADES =>	oApp():Ciudades
#xtranslate		TABLA_REGIONES =>	oApp():Regiones

//---------------------------------------

static bBlock

//---------------------------------------

Function Ciudades(  lBuscar )
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local bKey
  Local bClick
  Local h     := {=>}  
  Local lExit := FALSE
    
  h["id_ciudad"] := ""
  
  DEFAULT lBuscar := FALSE
  
  bBlock := {|| h["id_ciudad"]:= oQry:id_ciudad, lExit:= TRUE, oDlg:End() }
  if lBuscar
     bClick := bBlock
     bKey   := bBlock
  else
     bClick	:= Nil
  	 bKey 	:= bBlock
  endif  
    
  TEXT INTO cQuery
  	SELECT 	a.id_ciudad,
						a.id_region,        
						a.nombre AS nameciudad, 
						b.nombre AS nameregion,    	
						a.my_recno 		
  	FROM %1 AS a 
  	LEFT JOIN %2 AS b ON a.id_region = b.id_region
  	ORDER BY a.id_region
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TABLA_CIUDADES, TABLA_REGIONES )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE "TABLA CIUDADES" RESOURCE "xbrowse"; 
  	ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id_ciudad" , "No. Ciudad"	 , Nil, 100, AL_LEFT },; 
  	{ "nameciudad", "Nombre Ciudad", Nil, 400, AL_LEFT },;
  	{ "id_region" , "No.Region" 	 , Nil, 100, AL_LEFT },;
   	{ "nameregion", "Nombre Region", Nil, 400, AL_LEFT };
    }    
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyDown     = {|nKey| if(nKey==VK_RETURN, Eval(bKey), Nil)}
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= bClick
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @h, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()

Return h["id_ciudad"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, h, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70

  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() )

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( LeaDatos( oQry, FALSE ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Reporte Ciudades"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
     
Return Nil

//---------------------------------------

// http://forums.fivetechsupport.com/viewtopic.php?f=6&t=31223

static Function LeaDatos( oQry, lNew )
	Local aReturn, aNomRegiones, aIndRegiones
	Local oDlg
	Local oFont
	Local oSay
	Local oGet 			:= Array(3)
	Local oBtn    	:= Array(2)
	Local h       	:= { => }
	Local lExit   	:= FALSE
	Local lProcess	:= FALSE
	Local cTitle  	:= if(lNew, "AGREGAR UNA CIUDAD", "MODIFICA UNA CIUDAD")
	
	aReturn 		 := GetArrayRegiones()
	aNomRegiones := ArrTranspose(aReturn)[2]
	aIndRegiones := ArrTranspose(aReturn)[1]
	
	h["id_ciudad"] := if(lNew, LastIdCiudad(), oQry:id_ciudad)
	h["id_region"] := if(lNew, aIndRegiones[1], oQry:id_region)
	h["region"] 	 := if(lNew, aNomRegiones[1], GetNameRegion(oQry:id_region, TRUE))
 	h["nombre"]    := if(lNew, Space(60), oQry:nameciudad)
	 	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "CIUDADES" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "Codigo :"
	REDEFINE SAY ID 101 OF oDlg PROMPT "Region :" 
	REDEFINE SAY ID 102 OF oDlg PROMPT "Nombre :" 
	
	REDEFINE SAY oSay ID 103 OF oDlg PROMPT StrZero(h["id_region"],3) 
		 	
	REDEFINE GET oGet[1] VAR h["id_ciudad"] ID 200 OF oDlg;
		WHEN FALSE
		
  REDEFINE COMBOBOX oGet[2] VAR h["region"]	ITEMS aNomRegiones ID 201 OF oDlg
  	oGet[2]:bChange = <|| 
  	  								 h["id_region"] := aIndRegiones[ oGet[2]:nAt ]
  		            		 oSay:SetText( StrZero(h["id_region"],3) ) 
  		            		 Return TRUE
  		            		>
		
  REDEFINE GET oGet[3] VAR h["nombre"] ID 202 OF oDlg;
  	PICTURE "@!"
	
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
	
	ACTIVATE DIALOG oDlg;
		VALID lExit;
		ON INIT ( DisableSysMenuDlg(oDlg) )
		
  if lProcess
   	 if lNew
   	 	  MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_CIUDADES,;
  									{ "id_ciudad"  		=> h["id_ciudad" ],;
  										"id_region"   	=> h["id_region"    ],;
  									  "nombre"     		=> h["nombre"  ] } )
  								  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_CIUDADES ,;
  									{ "id_ciudad"     => h["id_ciudad"],;
  										"id_region"     => h["id_region" ],;
  										"nombre"   	  	=> h["nombre"] },;
  									  "my_recno = " +ClipValue2SQL(oQry:my_recno) )
  										
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )
  
  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_CIUDADES, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

Function SiIdCiudad( id )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE id_ciudad = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_CIUDADES, ClipValue2SQL(id) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

static Function LastIdCiudad()
  Local oQry
  Local cQuery
  Local id := 0
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	ORDER BY id_ciudad DESC
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_CIUDADES)	
    
  oQry = oSrv:CreateQuery( cQuery )

	if ( oQry:RecCount() > 0 )
  	id := oQry:id_ciudad
	endif  	
  
  oQry:End()
   
  id++
  
Return id

//---------------------------------------

Function GetArrayCiudades( id_region )
	Local oQry
	lOCAL cQuery
	Local aArray := {}
	
	if id_region == Nil
		TEXT INTO cQuery
			SELECT id_ciudad, id_region, nombre
			FROM %1
			ORDER BY id_ciudad
		ENDTEXT
	else
		TEXT INTO cQuery
			SELECT id_ciudad, id_region, nombre
			FROM %1
			WHERE id_region = %2
			ORDER BY id_ciudad
		ENDTEXT
	endif	
	
	cQuery := StrFormat( cQuery, TABLA_CIUDADES, ClipValue2SQL(id_region) )

	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
		AAdd( aArray, { oQry:id_ciudad, oQry:nombre } )
		oQry:Skip()
	enddo
	
	oQry:End()
	
	if Len(aArray) == 0
		 AAdd( aArray, { 0, "No hay Ciudades" } )
	endif
	
Return aArray	

//---------------------------------------

Function GetRegionFromCiudad( id_ciudad )
	Local oQry
	Local cQuery
	Local id_region := 0
	
	TEXT INTO cQuery
		SELECT id_ciudad, id_region
		FROM %1
		WHERE id_ciudad = %2
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_CIUDADES, ClipValue2SQL(id_ciudad) )
	
	oQry = oSrv:CreateQuery( cQuery )
	
	if oQry:RecCount() > 0
		id_region := oQry:id_region
	endif
	
	oQry:End()	
		
Return id_region

//---------------------------------------

Function GetNameCiudad( id )
	Local oQry
	Local cQuery
	Local cName := ""
		
	TEXT INTO  cQuery
		SELECT id_ciudad, nombre
		FROM %1
		WHERE id_ciudad = %2
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery,TABLA_CIUDADES, ClipValue2Sql(id) )

	oQry = oSrv:CreateQuery( cQuery )
	
	if oQry:RecCount() > 0
		cName := oQry:nombre	
	endif
	
	oQry:End()
	
Return cName

//---------------------------------------

Function Crear_Ciudades()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_ciudad 	  int(10) NOT NULL DEFAULT 0,
     	id_region     int(10) NOT NULL DEFAULT 0,
     	nombre        char(60) NOT NULL DEFAULT '',
     	my_recno 			int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX id_ciudad(id_ciudad),
      INDEX nombre (nombre)
   ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_CIUDADES )
  
  oSrv:VerifyTable( TABLA_CIUDADES, cStruct ) 	
       
Return Nil

// FINAL