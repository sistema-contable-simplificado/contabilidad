// empresas

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_EMPRESAS 		=>	oApp():Empresas
#xtranslate		TABLA_TIPOEMPRESA =>	oApp():TipoEmpresa
#xtranslate		TABLA_REGIONES 		=>	oApp():Regiones
#xtranslate		TABLA_CIUDADES 		=>	oApp():Ciudades
#xtranslate		TABLA_COMUNAS  		=>	oApp():Comunas

//---------------------------------------

Function Do_Empresas()
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local h     := {=>}  
  Local lExit := FALSE
    
  h["id_empresa"] := 0
			
  TEXT INTO cQuery
  	SELECT 	a.id_empresa, 	
						a.id_tipo,
						e.nombre AS nametipo,        
						a.rut, 				
						a.razon_social,
						a.nombre,    	
						a.paterno,			
						a.materno,			
						a.direccion1,	
						a.direccion2,	
						a.id_region,		
						b.nombre AS nameregion,
						a.id_ciudad,		
						c.nombre AS nameciudad,
						a.id_comuna,		
						c.nombre AS namecomuna,
						a.telefono,		
						a.celular,			
						a.casilla,
						a.cod_giro,		
						a.nom_giro,		
						a.email,			
						a.year,
						a.month,	
						a.my_recno    
		FROM %1 a
		LEFT JOIN %2 b ON a.id_region = b.id_region
		LEFT JOIN %3 c ON a.id_ciudad = c.id_ciudad
		LEFT JOIN %4 d ON a.id_comuna = d.id_comuna 
		LEFT JOIN %5 e ON a.id_tipo   = e.id_tipo
  	ORDER BY a.id_empresa
  ENDTEXT	
  
  cQuery := StrFormat(  cQuery, TABLA_EMPRESAS,;
   															TABLA_REGIONES,;
   															TABLA_CIUDADES,;
   															TABLA_COMUNAS,;
   															TABLA_TIPOEMPRESA )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE "EMPRESAS" RESOURCE "xbrowse"; 
  	ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id_empresa" 	, "Empresa"	  	, Nil,  80, AL_LEFT },; 
  	{ "nametipo"   	, "Tipo" 		  	, Nil, 100, AL_LEFT },;
  	{ "rut"      		, "Rut" 	  		, Nil, 100, AL_LEFT },;
  	{ "nombre"  		, "Nombre"    	, Nil, 300, AL_LEFT },;
  	{ "razon_social", "Razon Social", Nil, 300, AL_LEFT },;
  	{ "nameregion"	, "Region" 			, Nil, 120, AL_LEFT },; 
  	{ "nameciudad"	, "Ciudad"    	, Nil, 120, AL_LEFT },; 
   	{ "namecomuna"	, "Comuna"			, Nil, 120, AL_LEFT },;
   	{ "email"  	  	, "Email"				, Nil, 120, AL_LEFT },;
   	{ "year"  	  	, "A�o"				  , Nil, 100, AL_RIGHT},;
   	{ "month"  	  	, "Mes"				  , Nil,  60, AL_RIGHT} ;
  }    
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bKeyDown     = {|nKey| if(nKey == VK_RETURN, Eval(oBrw:bLDblClick), Nil)}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= {|| SetSession(oQry, @h), lExit:= TRUE, oDlg:End() }
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()
  
Return h["id_empresa"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70
  
  DEFINE BUTTON OF oBar; 
  	PROMPT "Seleccionar"; 
  	RESOURCE aBitmap[ BMP32_YES ];
  	ACTION ( Eval(oBrw:bLDblClick) )
  	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() );
    GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( if(oQry:RecCount()>0,LeaDatos(oQry, FALSE),Nil), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Reporte Catalogo"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
     
Return Nil

//---------------------------------------

static Function SetSession( oQry, h )
	
	oApp():Session["year"]				= oQry:year
	oApp():Session["month"]				= oQry:month	
	oApp():Session["namemonth"]   = Upper(MonthC(oApp():Session["month"]))
	oApp():Session["id_empresa"] 	= oQry:id_empresa
	oApp():Session["nomtipo"]     = oQry:nametipo
  oApp():Session["activa"]     	= TRUE
	oApp():Session["rut"]				 	= oQry:rut
  oApp():Session["razon"]			 	= oQry:razon_social
  oApp():Session["nombre"]		 	= oQry:nombre
  oApp():Session["paterno"]		 	= oQry:paterno
  oApp():Session["materno"]		 	= oQry:materno
  oApp():Session["dir1"]				= oQry:direccion1
  oApp():Session["dir2"]				= oQry:direccion2
  oApp():Session["region"]			= oQry:nameregion
	oApp():Session["ciudad"]			= oQry:nameciudad
	oApp():Session["comuna"]			= oQry:namecomuna
	oApp():Session["telefono"]		= oQry:telefono		
	oApp():Session["celular"]			= oQry:celular			
	oApp():Session["casilla"]			= oQry:casilla
	oApp():Session["codgiro"]			= oQry:cod_giro
	oApp():Session["nomgiro"]			= oQry:nom_giro		
	oApp():Session["email"]				= oQry:email	
	oApp():Session["giro"]				= oQry:nom_giro
		
	oApp():SetTitle()	
	
	h["id_empresa"] := oQry:id_empresa
  
Return Nil  

//---------------------------------------

static Function Ejercicio()
	Local oDlg
	Local oFont
	Local oBtn
	Local nYear  := oApp():Session["year"]
	Local nMonth :=	oApp():Session["month"]
	Local oGet   := Array(2)
	Local lExit  := FALSE
	
	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE "EJERCICIO" RESOURCE "EJERCICIO" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "A�O TRABAJO :"
	REDEFINE SAY ID 101 OF oDlg PROMPT "MES TRABAJO :"
	
	REDEFINE GET oGet[1] VAR nYear 	ID 200 OF oDlg PICTURE "9999"
	REDEFINE GET oGet[2] VAR nMonth ID 201 OF oDlg PICTURE "99"
		oGet[2]:bValid = <|oGet|
											 Local mes := oGet:varGet()
		                   if mes < 1 .or. mes > 12
		                      MsgStop("Mes debe estar entre [1..12]")
		                      Return FALSE
		                   endif
		                   Return TRUE
		                  >   
		                  
	REDEFINE BTNBMP oBtn ID 300 OF oDlg;
		PROMPT "Acepta";
		CENTER;
		NOROUND;
		ACTION ( lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()		                   
		                  
	ACTIVATE DIALOG oDlg;
		VALID lExit;
		ON INIT ( DisableSysmenuDlg(oDlg) )
			
Return { nYear, nMonth }
	
//---------------------------------------

// http://forums.fivetechsupport.com/viewtopic.php?f=6&t=31223

static Function LeaDatos( oQry, lNew )
	Local oDlg
	Local oFont
	Local oSay1, oSay2, oSay3 
	Local nPos
	Local oGet 				:= Array(20)
	Local oBtn    		:= Array(2)
	Local h       		:= { => }
	Local aTipos    	:= ArrTipoEmpresa()
	Local aNomTipos 	:= ArrTranspose(aTipos)[2]
	Local aIndTipos 	:= ArrTranspose(aTipos)[1]
	Local aComunas  	:= GetArrayComunas()
	Local aNomComuna  := ArrTranspose(aComunas)[4] 
	Local aIndComuna  := ArrTranspose(aComunas)[1] 
	Local aIndCiudad  := ArrTranspose(aComunas)[2] 
	Local aIndRegion  := ArrTranspose(aComunas)[3]
	Local aMonths     := ArrMonths()
	Local aIndMonth   := ArrTranspose(aMonths)[1] 
	Local aNomMonth   := ArrTranspose(aMonths)[2]
	Local cMonth      := aNomMonth[1]
	Local lExit   		:= FALSE
	Local lProcess		:= FALSE
	Local cTitle  		:= if(lNew, "AGREGAR EMPRESA", "MODIFICA EMPRESA")
		
	h["id"]   		 := if(lNew, LastIdEmpresa(), oQry:id_empresa)
	h["tipo"] 		 := if(lNew, aIndTipos[1], oQry:id_tipo)
	H["nomtipo"]	 := if(lNew, aNomTipos[1], aNomTipos[oQry:id_tipo])
	h["rut"]    	 := if(lNew, Space(30), oQry:rut)
	h["razon"]  	 := if(lNew, Space(60), oQry:razon_social)
 	h["nombre"] 	 := if(lNew, Space(60), oQry:nombre)
	h["paterno"]	 := if(lNew, Space(60), oQry:paterno)
	h["materno"]	 := if(lNew, Space(60), oQry:materno)
 	h["dir1"] 		 := if(lNew, Space(80), oQry:direccion1)
 	h["dir2"] 		 := if(lNew, Space(80), oQry:direccion2)
 	h["idcomuna"]  := if(lNew, aIndComuna[1], oQry:id_comuna)
 	h["year"]			 := if(lNew, Year(Date()), oQry:year)
 	h["month"]		 := if(lNew, Month(Date()), oQry:month)
 	
 	cMonth := if(lNew, aNomMonth[1], aNomMonth[oQry:month])
 	 	
 	if lNew
 		h["comuna"] := aNomComuna[1]
 	else	
 		nPos := AScan(aIndComuna, oQry:id_comuna)
 		if nPos > 0
 		   h["comuna"] := aNomComuna[nPos]
 		else
 		   h["comuna"] := aNomComuna[1]
 		endif
 	endif	   
 	
 	h["idregion"]  := if(lNew, aIndRegion[1], oQry:id_region)
 	h["region"] 	 := GetNameRegion(h["idregion"])
 	h["idciudad"]  := if(lNew, aIndCiudad[1], oQry:id_ciudad)
 	h["ciudad"] 	 := GetNameCiudad(h["idciudad"])
 	
 	h["tel"]    	 := if(lNew, Space(30), oQry:telefono)
 	h["cel"]  		 := if(lNew, Space(30), oQry:celular)
 	h["casilla"]	 := if(lNew, Space(80), oQry:casilla)
 	h["codgiro"]	 := if(lNew, Space(20), oQry:cod_giro)
 	h["nomgiro"]	 := if(lNew, Space(80), oQry:nom_giro)
 	h["email"]  	 := if(lNew, Space(40), oQry:email)
 	 	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "EMPRESAS" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "EMPRESA"	
	REDEFINE SAY ID 101 OF oDlg PROMPT "TIPO EMPRESA"	
  REDEFINE SAY ID 102 OF oDlg PROMPT "RUT"						
  REDEFINE SAY ID 103 OF oDlg PROMPT "NOMBRE"
  REDEFINE SAY ID 104 OF oDlg PROMPT "PATERNO"				
  REDEFINE SAY ID 105 OF oDlg PROMPT "MATERNO"
  REDEFINE SAY ID 106 OF oDlg PROMPT "RAZON SOCIAL:"	
  REDEFINE SAY ID 107 OF oDlg PROMPT "DIRECCION 1:"	
  REDEFINE SAY ID 108 OF oDlg PROMPT "DIRECCION 2:"	
  REDEFINE SAY ID 109 OF oDlg PROMPT "REGION :"			
  REDEFINE SAY ID 110 OF oDlg PROMPT "CIUDAD :"			
  REDEFINE SAY ID 111 OF oDlg PROMPT "COMUNA :"			
  REDEFINE SAY ID 112 OF oDlg PROMPT "TELEFONO :"		
  REDEFINE SAY ID 113 OF oDlg PROMPT "CELULAR:"
  REDEFINE SAY ID 114 OF oDlg PROMPT "CASILLA :"			
  REDEFINE SAY ID 115 OF oDlg PROMPT "GIRO :"				
  REDEFINE SAY ID 116 OF oDlg PROMPT "EMAIL :"
  REDEFINE SAY oSay1 ID 117 OF oDlg PROMPT StrZero( h["idregion"], 3 )
  REDEFINE SAY oSay2 ID 118 OF oDlg PROMPT StrZero( h["idciudad"], 3 )
  REDEFINE SAY oSay3 ID 119 OF oDlg PROMPT StrZero( h["idcomuna"], 3 )
  REDEFINE SAY ID 120 OF oDlg PROMPT "A�O TRABAJO :"
  REDEFINE SAY ID 121 OF oDlg PROMPT "MES TRABAJO :"
  		 	
	REDEFINE GET oGet[1] VAR h["id"] ID 200 OF oDlg;
		WHEN lNew
		oGet[1]:bValid = <|oGet| 
		                  if SiIdEmpresa(oGet:varget()) 
												MsgStop("Empresa YA Existe", Name_Empresa)
												Return FALSE
											endif	
											Return TRUE
										>	
		 	
	REDEFINE COMBOBOX oGet[2] VAR h["nomtipo"] ITEMS aNomTipos ID 201 OF oDlg;
		ON CHANGE ( h["tipo"] := aNomTipos[ oGet[2]:nAt ] )
		
	REDEFINE GET oGet[3] VAR h["rut"]	ID 202 OF oDlg;
		PICTURE "@!"
		oGet[3]:bValid = <|oGet|
		                  Local value := oGet:varget()
	  									if SiRutEmpresa( oGet:varget() )
	  									   MsgStop("RUT Ya Existe", Name_Empresa)
	  										 Return FALSE
	  									else
	  									   if Valida_RUT( @value )
	  									      oGet:varPut(value)
	  									      oGet:Refresh()
	  									      Return TRUE
	  									   else
	  									      MsgStop("Error en Digito de control " + Right(value,1), Name_Empresa)
	  									      Return FALSE   
	  									   endif
	  									endif
	  									Return TRUE
	  								>		 
	  										  								
	REDEFINE GET oGet[4] VAR h["nombre"]	ID 203 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[5] VAR h["paterno"]	ID 204 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[6] VAR h["materno"] ID 205 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[7] VAR h["razon"] 	ID 206 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[8] VAR h["dir1"] 		ID 207 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[9] VAR h["dir2"] 		ID 208 OF oDlg PICTURE "@!"
	
	REDEFINE GET oGet[10] VAR h["region"] ID 209 OF oDlg WHEN FALSE
	REDEFINE GET oGet[11] VAR h["ciudad"] ID 210 OF oDlg WHEN FALSE
	
	REDEFINE COMBOBOX oGet[12] VAR h["comuna"] ITEMS aNomComuna ID 211 OF oDlg
		oGet[12]:bChange = <||
		                     Local item := oGet[12]:nAt
		                     
		                     h["idregion"] := aIndRegion[item]
		                     h["idciudad"] := aIndCiudad[item] 
		                     h["idcomuna"] := aIndComuna[item] 
		                     
		                     oSay1:SetText( StrZero(h["idregion"],3) )
		                     oGet[10]:varPut( GetNameRegion(h["idregion"]) )
		                     oGet[10]:Refresh()
		                     
		                     oSay2:SetText( StrZero(h["idciudad"],3) )
		                     oGet[11]:varPut( GetNameCiudad(h["idciudad"]) )
		                     oGet[11]:Refresh()
		                     
		                     oSay3:SetText( StrZero(h["idcomuna"],3) )
		                     
		                     Return TRUE
		                    > 
		                     		
  REDEFINE GET oGet[13] VAR h["tel"] 			ID 212 OF oDlg
	REDEFINE GET oGet[14] VAR h["cel"] 			ID 213 OF oDlg
	REDEFINE GET oGet[15] VAR h["casilla"] 	ID 214 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[16] VAR h["codgiro"] 	ID 215 OF oDlg
	REDEFINE GET oGet[17] VAR h["nomgiro"] 	ID 216 OF oDlg PICTURE "@!"
	REDEFINE GET oGet[18] VAR h["email"] 		ID 217 OF oDlg
	
	REDEFINE GET oGet[19] VAR h["year"] 		ID 218 OF oDlg
	
	REDEFINE COMBOBOX oGet[20] VAR cMonth ITEMS aNomMonth ID 219 OF oDlg;	
		ON CHANGE ( h["month"] := aIndMonth[ oGet[20]:nAt ] )
	
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
    
  oDlg:bInit = <||
    							DisableSysMenuDlg(oDlg)
    							if lNew
    								 oGet[1]:Setfocus()
    							else	 
	                   oGet[2]:Setfocus()
	                endif
	                Return Nil
	               >
	                   
	ACTIVATE DIALOG oDlg;
		VALID lExit
		
  if lProcess
   	 if lNew
   	 	  MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_EMPRESAS,;
  									{ "id_empresa" 	 => h["id"],;
											"id_tipo"      => h["tipo"],;
											"rut" 				 => h["rut"],;
											"razon_social" => h["razon"],;
											"nombre"    	 => h["nombre"],;
											"paterno"			 => h["paterno"],;
											"materno"			 => h["materno"],;
											"direccion1"	 => h["dir1"],;
											"direccion2"	 => h["dir2"],;
											"id_region"		 => h["idregion"],;
											"id_ciudad"		 => h["idciudad"],;
											"id_comuna"		 => h["idcomuna"],;
											"telefono"		 => h["tel"],;
											"celular"			 => h["cel"],;
											"casilla"    	 => h["casilla"],;
											"cod_giro"		 => h["codgiro"],;
											"nom_giro"		 => h["nomgiro"],;
											"email"				 => h["email"],;
											"year"				 => h["year"],;
											"month"				 => h["month"] } )
  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_EMPRESAS ,;
  									{ "id_empresa" 	 => h["id"],;
											"id_tipo"      => h["tipo"],;
											"rut" 				 => h["rut"],;
											"razon_social" => h["razon"],;
											"nombre"    	 => h["nombre"],;
											"paterno"			 => h["paterno"],;
											"materno"			 => h["materno"],;
											"direccion1"	 => h["dir1"],;
											"direccion2"	 => h["dir2"],;
											"id_region"		 => h["idregion"],;
											"id_ciudad"		 => h["idciudad"],;
											"id_comuna"		 => h["idcomuna"],;
											"telefono"		 => h["tel"],;
											"celular"			 => h["cel"],;
											"casilla"    	 => h["casilla"],;
											"cod_giro"		 => h["codgiro"],;
											"nom_giro"		 => h["nomgiro"],;
											"email"				 => h["email"],;
											"year"				 => h["year"],;
											"month"				 => h["month"] },;
  									"my_recno = " + ClipValue2SQL(oQry:my_recno) )
  										
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )
  
  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_EMPRESAS, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

Function Valida_RUT( value )
  Local i
  Local dig 
  Local rut
  Local lenRut
	Local aMul, iMul
	Local suma, resto, resto1, resto2
  Local lOk := TRUE
	
	value := AllTrim(value)
	
	// valida longitud 1..9
	if Len(value) < 1 .or. Len(value) > 9 
	   MsgStop("Longitud debe ser de 9", Name_Empresa)
	   Return FALSE
	endif
	
	// agrega '0' a la cadena
	if Len(value) < 9
	   value := StrZero(Val(value),9)	   
	endif
	
	// valida el contenido
	for i := 1 to Len(value)
	  if !( SubStr(value,i,1) $ "0123456789K" )
	     lOk := FALSE
	     exit
	  endif 
	next i
	
	if !lOk
		MsgStop("Contenido debe ser numerico [0..9,K]", Name_Empresa)
	  Return FALSE
	endif  
	
	// valida el ultimo digito
	suma 	 := 0
	dig    := Right(value,1)
	rut    := Left(value,8)
	lenRut := Len(rut)
	aMul 	 := { 2, 3, 4, 5, 6, 7, 2, 3 }
	iMul   := 1
	
	for i := lenRut to 1 step -1
		suma += Val(SubStr(rut,i,1))*aMul[iMul]
		iMul ++
  next i
	
	resto		:= Int(suma/11)
	resto1	:= suma-(resto*11)
	resto2	:= 11-resto1
  
  if resto2 == 11
     if dig == "0"
	   	  lOk := TRUE
     endif
	elseif resto2 == 10
	 	 if dig == "K"
	   	  lOk := TRUE
	 	 endif
	elseif Val(dig) == resto2
     lOk := TRUE
  else
     lOk := FALSE   
  endif
				
Return lOk

//---------------------------------------

Function SiIdEmpresa( id )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE id_empresa = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_EMPRESAS, ClipValue2SQL(id) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

Function SiRutEmpresa( rut )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE rut = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_EMPRESAS, ClipValue2SQL(rut) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

static Function LastIdEmpresa()
  Local oQry
  Local cQuery
  Local id := 0
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	ORDER BY id_empresa DESC
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_EMPRESAS )	
    
  oQry = oSrv:CreateQuery( cQuery )

	if ( oQry:RecCount() > 0 )
  	id := oQry:id_empresa
	endif  	
  
  oQry:End()
   
  id++
  
Return id

//---------------------------------------

Function ArrTipoEmpresa()
	Local oQry
	Local cQuery
	Local aArray := {}
		
	TEXT INTO cQuery
		SELECT id_tipo, 
		       CONCAT(LPAD(id_tipo,3,'0'),"-",nombre) AS nametipo
		FROM %1
    ORDER BY id_tipo		
  ENDTEXT  
	
	cQuery := StrFormat( cQuery, TABLA_TIPOEMPRESA )
	
	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
		AAdd( aArray, { oQry:id_tipo, oQry:nametipo } )
		oQry:Skip()
	enddo
		
	oQry:End()
		
Return aArray

//---------------------------------------

Function ArrMonths()
Return { {  1, "01=ENERO" 	},;
				 {  2, "02=FEBRERO" },;
				 {  3, "03=MARZO" 	},;
				 {  4, "04=ABRIL" 	},;
				 {  5, "05=MAYO" 		},;
				 {  6, "06=JUNIO" 	},;
				 {  7, "07=JULIO" 	},;
				 {  8, "08=AGOSTO" 	},;
				 {  9, "09=SEPTIEMBRE" },;
				 { 10, "10=OCTUBRE" 	 },;
				 { 11, "11=NOVIEMBRE"  },;
				 { 12, "12=DICIEMBRE"  } }

//---------------------------------------

static Function CheckTiposEmpresa()
	Local oQry
	Local cQuery := "SELECT * FROM " + TABLA_TIPOEMPRESA
	
	oQry = oSrv:CreateQuery(cQuery)
	
	if oQry:RecCount() == 0
		 oSrv:InsertHash( TABLA_TIPOEMPRESA, { "id_tipo" => 1, "nombre" => "PERSONA NATURAL" } )
		 oSrv:InsertHash( TABLA_TIPOEMPRESA, { "id_tipo" => 2, "nombre" => "SOCIEDAD LIMITADA" } )
		 oSrv:InsertHash( TABLA_TIPOEMPRESA, { "id_tipo" => 3, "nombre" => "SPA" } )
		 oSrv:InsertHash( TABLA_TIPOEMPRESA, { "id_tipo" => 4, "nombre" => "IERL" } )
		 oSrv:InsertHash( TABLA_TIPOEMPRESA, { "id_tipo" => 5, "nombre" => "SOCIEDAD ANONIMA" } )
	endif
	
	oQry:End()
	
Return Nil	
					
//---------------------------------------

Function Crear_Empresas()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_empresa 		int(10) NOT NULL DEFAULT 0,
  		id_tipo       int(4) NOT NULL DEFAULT 0,
     	rut 					char(30) NOT NULL DEFAULT ' ',
     	razon_social	char(60) NOT NULL DEFAULT ' ',
     	nombre    		char(40) NOT NULL DEFAULT ' ',
     	paterno				char(40) NOT NULL DEFAULT ' ',
     	materno				char(40) NOT NULL DEFAULT ' ',
     	direccion1		char(80) NOT NULL DEFAULT ' ',
     	direccion2		char(80) NOT NULL DEFAULT ' ',
     	id_region			int(10) NOT NULL DEFAULT 0,
     	id_ciudad			int(10) NOT NULL DEFAULT 0,
     	id_comuna			int(10) NOT NULL DEFAULT 0,
     	telefono			char(30) NOT NULL DEFAULT ' ',
     	celular				char(30) NOT NULL DEFAULT ' ',
     	casilla    		char(80) NOT NULL DEFAULT ' ',
     	cod_giro			char(30) NOT NULL DEFAULT ' ',
     	nom_giro			char(80) NOT NULL DEFAULT ' ',
     	email					char(40) NOT NULL DEFAULT ' ',
     	year        	int(4) NOT NULL DEFAULT %2,
  		month       	int(4) NOT NULL DEFAULT %3,
     	my_recno 			int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX id_empresa (id_empresa),
      INDEX rut (rut)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_EMPRESAS,;
  															 ClipValue2SQL(Year(Date())),;
  															 ClipValue2SQL(Month(Date())) )
  
  oSrv:VerifyTable( TABLA_EMPRESAS, cStruct ) 	
          
Return Nil

//---------------------------------------

Function Crear_TipoEmpresa()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_tipo			int(4) NOT NULL DEFAULT 0,
  		nombre			char(40) NOT NULL DEFAULT ' ',
     	my_recno 		int(4) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX id_tipo (id_tipo)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_TIPOEMPRESA )
  
  oSrv:VerifyTable( TABLA_TIPOEMPRESA, cStruct ) 	
  
  CheckTiposEmpresa()
            
Return Nil

// FINAL