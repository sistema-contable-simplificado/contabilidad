#include "fivewin.ch"
#include "contabil.ch"

//-----------------------------------------

Function AddFunctionsFrm3( oFr )  

  WITH OBJECT oFr
  	:SetEventHandler( "Report", "OnUserFunction", {|FName, aFParams| CallUserFunction(FName, aFParams)} )
  	:AddFunction( "Function CMONTH( cFecha : String ):Variant", GRUPOAVCSIS, "Devuelve Mes del String de una Fecha" )
  	:AddFunction( "Function DTOC( Fecha : TDate ):Variant", GRUPOAVCSIS, "Convierte String una Fecha" )
  	:AddFunction( "Function DATEMONTH( Fecha : TDate ):Variant", GRUPOAVCSIS, "Devuelve el Mes en String" )
  	:AddFunction( "Function NAMEMONTH( Month : Integer ):Variant", GRUPOAVCSIS, "Devuelve el Mes en String" )
  	:AddFunction( "Function NUMTOCHAR( Numero : Double, nLength: Variant = EmptyVar, nDecimals: Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Convierte Numero a String Con MillaRes" )
  	:AddFunction( "Function STR( Numero : Double, nLength: Variant = EmptyVar, nDecimals: Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Convierte Numero a String" )
  	:AddFunction( "Function STRZERO( Numero : Double, nLength: Variant = EmptyVar, nDecimals: Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Pone Zeros a la Izquierda" )
  	:AddFunction( "Function IF_VALOR( Valor : Boolean ):Variant", GRUPOAVCSIS, "Retorna SI / NO" )
  	:AddFunction( "Function SI( Valor: Boolean, TrueValor : Variant, FalseValor : Variant ):Variant", GRUPOAVCSIS, "Retorna un valor Variant segun si es Falso o Verdadero" )
  	:AddFunction( "Function ALLTRIM( cString : String ):Variant", GRUPOAVCSIS, "Elimina Espacios en Blanco a ambos lados de la Cadena" )
  	:AddFunction( "Function RTRIM( cString : String ):Variant", GRUPOAVCSIS, "Elimina Espacios en Blanco a la Derecha de la Cadena" )
  	:AddFunction( "Function SUBSTR( cString : String, nPos : Variant = EmptyVar, nLen: Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Sustrae una Subcadena de Una Cadena" )
  	:AddFunction( "Function CHARTOSIGNO( Numero : Double, nLength: Variant = EmptyVar, nDecimals: Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Retorna String con Signo del Numero" )
  	:AddFunction( "Function EXEC_IF( Valor: Boolean, ValorSi : Variant = EmptyVar, ValorNo: Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Evalua y retorna segun Si o No" )
  	:AddFunction( "Function VAR2STR( Variable : String, Cadena : Variant = EmptyVar, Posicion : Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Suma Variable a String" )
  	:AddFunction( "Function LEFT( cString : String, nLen : Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Sustrae una Subcadena desde la Izquierda" )
  	:AddFunction( "Function RIGHT( cString : String, nLen : Variant = EmptyVar ):Variant", GRUPOAVCSIS, "Sustrae una Subcadena desde la Derecha" )
  	:AddFunction( "Function NUM2TXT( Monto : Double ):Variant", GRUPOAVCSIS, "Retorna el Monto en Letras" )
  	:AddFunction( "Function YEAR( Fecha : TDate ):Variant", GRUPOAVCSIS, "Retorna el A�o" )
  	:AddFunction( "Function MONTH( Fecha : TDate ):Variant", GRUPOAVCSIS, "Retorna el Mes" )
  	:AddFunction( "Function DAY( Fecha : TDate ):Variant", GRUPOAVCSIS, "Retorna el Dia" )
  	:AddFunction( "Function AMPM( cHora : String ):Variant", GRUPOAVCSIS, "Retorna La Hora en Formato Am/Pm" )
  	:AddFunction( "Function TRANSFORM( cString : String, cPicture : String):Variant", GRUPOAVCSIS, "Retorna String Formateado" )
  END
  
Return Nil

//-----------------------------------------

Function CallUserFunction( FName, aFParams )
  Local Res
 
  // Los parametros los toma como un Array de Caracteres
  // Por lo tanto el : aFParams[1] es el array,  
  // 									 aFParams[1,1] es valor del primer item del array o sea el parametro enviado
    
  do case
  
  	case ( FName == "AMPM" ) 
    	if ValType( aFParams[1,1] ) == "C"
         Res:= AmPM( aFParams[1,1] )
      endif
            
    case ( FName == "CMONTH" ) 
    	if ValType( aFParams[1,1] ) == "C"
         Res:= CMonth( CTOD( aFParams[1,1] ) )
      endif
       
    case ( FName == "DATEMONTH" )
      if ValType( aFParams[1,1] ) == "D"
         Res:= MonthC( Month( aFParams[1,1] ) )
      endif 
      
    case ( FName == "NAMEMONTH" )
      if ValType( aFParams[1,1] ) == "N"
         Res:= MonthC( aFParams[1,1] )
      endif   
           
    case ( FName == "DTOC" )
      if ValType( aFParams[1,1] ) == "D"
         Res:= DToC( aFParams[1,1] ) 
      endif      
       
    case ( FName == "NUMTOCHAR" )
      if ValType( aFParams[1,1] ) == "N"
         Res:= NumToChar( aFParams[1,1], aFParams[2], aFParams[3] ) 
      endif      
       
    case ( FName == "STR" )
      if ValType( aFParams[1,1] ) == "N"
         Res:= Str( aFParams[1,1], aFParams[2], aFParams[3] ) 
      endif         
       
    case ( FName == "STRZERO" )
      if ValType( aFParams[1,1] ) == "N"
         Res:= StrZero( aFParams[1,1], aFParams[2], aFParams[3] ) 
      endif         
          
    case ( FName == "IF_VALOR" )   
  		if ValType( aFParams[1,1] ) == "L"
         Res:= iif( aFParams[1,1], "SI", "NO" ) 
      endif         
      
    case ( FName == "SI" )   
  		if ValType( aFParams[1,1] ) == "L"
  		   Res:= iif( aFParams[1,1], aFParams[2], aFParams[3] ) 
      endif       
      
    case ( FName == "ALLTRIM" )   
  		if ValType( aFParams[1,1] ) == "C"
         Res:= AllTrim( aFParams[1,1] ) 
      endif         
      
    case ( FName == "RTRIM" )   
  		if ValType( aFParams[1,1] ) == "C"
         Res:= RTrim( aFParams[1,1] ) 
      endif           
      
    case ( FName == "SUBSTR" )
      if ValType( aFParams[1,1] ) == "C"
         Res:= SubStr( aFParams[1,1], aFParams[2], aFParams[3] ) 
      endif               
      
    case ( FName == "CHARTOSIGNO" )
      if ValType( aFParams[1,1] ) == "N"
         Res:= NumToChar( aFParams[1,1], aFParams[2], aFParams[3] ) 
         if aFParams[1,1] > 0
            Res:= "+"+LTrim(Res)
         endif
      endif
      
    case ( FName == "EXEC_IF" )
      if ValType( aFParams[1,1] ) == "L"
         Res:= iif( aFParams[1,1], Eval(MakeBlock(aFParams[2])), Eval(MakeBlock(aFParams[3])) ) 
      endif                       
    
    case ( FName == "VAR2STR" )
      if ValType( aFParams[1,1] ) == "C"
         Res:= Var2Str( aFParams[1,1], aFParams[2], aFParams[3] )       
      else
         res:= "Error Var2Str"   
      endif
          
    case ( FName == "LEFT" )
      if ValType( aFParams[1,1] ) == "C"
         Res:= Left( aFParams[1,1], aFParams[2] )       
      endif   
      
    case ( FName == "RIGHT" )
      if ValType( aFParams[1,1] ) == "C"
         Res:= Right( aFParams[1,1], aFParams[2] )       
      endif       
      
    case ( FName == "NUM2TXT" )  
    	if ValType( aFParams[1,1] ) == "N"
         Res:= Num2Txt( aFParams[1,1] )  
      endif   
    
    case ( FName == "YEAR" )  
    	if ValType( aFParams[1,1] ) == "D"
         Res:= Year( aFParams[1,1] )  
      endif     
      
    case ( FName == "MONTH" )  
    	if ValType( aFParams[1,1] ) == "D"
         Res:= Month( aFParams[1,1] )  
      endif     
      
    case ( FName == "DAY" )  
    	if ValType( aFParams[1,1] ) == "D"
         Res:= Day( aFParams[1,1] )  
      endif         
    
    case ( FName == "TRANSFORM" )  
    	if ValType( aFParams[1,1] ) == "C"  
    		 Res := Transform( aFParams[1,1], aFParams[2] )
    	endif	 
        
    otherwise; Res:= "ERROR LA FUNCION NO EXISTE" + FName
    
  endcase
  
Return Res

//--------------------------------------------//

static Function Var2Str( cVar, cStr1, cStr2 )
  Local cResult
  
  if !Empty(cStr2) .and. Upper(cStr2) == "I"
     cResult:= cStr1 + cVar
  else
     cResult:= cVar + cStr1
  endif
  
Return cResult

//--------------------------------------------//

static Function MakeBlock( cData )
  Local bBlock
  
  if hb_isArray(cData)
     cData:= cData[1]
  endif
     
  if hb_isString(cData)
     bBlock:= {|| cData}
  else
     bBlock:= {|| "" }
  endif
  
Return bBlock

// FINAL
