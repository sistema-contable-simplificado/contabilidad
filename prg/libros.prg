#include  "fivewin.ch"
#include  "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_BALANCE 	=>	oApp():Balance
#xtranslate		TABLA_MAESTRO		=>	oApp():AsientoMaestro
#xtranslate		TABLA_LINEAS		=>	oApp():AsientoLIneas
#xtranslate		TABLA_TIPODOCUM =>	oApp():TipoDocum
#xtranslate		TABLA_BALANCE 	=>	oApp():Balance

//---------------------------------------

#xtranslate		VISTA_LIBRO_MAYOR  =>  "view_libro_mayor"
#xtranslate		TEMP_LIBRO_MAYOR   =>  "temp_libro_mayor"

//---------------------------------------

Function Fr3_LibroDiarioBorrador( lDesigner )
  Local oFr
  Local cQuery
  Local oQry
  Local obj
  Local nCol
  Local nAt
  Local a, h, b
  Local cNameRut
  Local cFileFr3 := PATH_FR3 + "libro_diario_borrador.fr3"
    
  cNameRut := AllTrim(oApp():Session["nombre"]) + " " + ;
  						AllTrim(oApp():Session["paterno"]) + " " + ;
  						AllTrim(oApp():Session["materno"]) 
    
  DEFAULT lDesigner := FR3_DESIGNER
  
  TEXT INTO cQuery
  	SELECT 	a.id_empresa, 	
  					a.numero, 			
						a.year,        
						a.month,       
						a.fecha,       
						a.tipo,
						CASE 
							WHEN a.tipo = 1 THEN "INGRESO"
							WHEN a.tipo = 2 THEN "EGRESO"
							WHEN a.tipo = 3 THEN "TRASPASO"
							WHEN a.tipo = 4 THEN "APERTURA"
							ELSE "NINGUNO"
						END AS nametipo,
						a.transaccion,
						CASE 
							WHEN a.transaccion = 1 THEN "COMPRAS"
							WHEN a.transaccion = 2 THEN "VENTAS"
							WHEN a.transaccion = 3 THEN "HONORARIOS"
							WHEN a.transaccion = 4 THEN "OTROS"
							ELSE "NINGUNO"
						END AS nametrans,		
						a.rut,			
						a.tiporut,     
						a.namerut,     
						a.id_docum,		
						b.nombre AS name_docum,
						a.descripcion, 
						a.my_recno 		
    FROM %1 a
    LEFT JOIN %2 b ON a.id_docum = b.id_docum
    WHERE a.id_empresa = %3 AND a.year = %4 AND a.month = %5 
		ORDER BY a.numero
  ENDTEXT	
  
  cQuery := StrFormat(  cQuery, TABLA_MAESTRO,;
                                TABLA_TIPODOCUM,;
  															ClipValue2Sql(oApp():Session["id_empresa"]),;
  															ClipValue2Sql(oApp():Session["year"]),;
  															ClipValue2Sql(oApp():Session["month"]) )
    
  oQry = oSrv:CreateQuery( cQuery )
            
  oFr:= FrReportManager():New( FILE_DLL )
  oFr:LoadLangRes( FILE_XML )
  oFr:SetIcon( ICONFR3 )
  oFr:SetTitle( MSGTITULO_FASTREPORT )
      
  // Maestro Asiento
  oFr:SetUserDataSet( "Maestro",;
  										MySqlFields( oQry ),;
                      {|| oQry:GoTop()},; 
                      {|| oQry:Skip(1)},; 
                      {|| oQry:Skip(-1)},;
                      {|| oQry:Eof() },;
                      {|aField| oQry:FieldGet( aField )} ) 
                      
            
  // Lineas Asiento
  h := {	"numero"		=> 1,;
          "cuenta"		=> 2,;
  				"nombre"  	=> 3,;
  				"debe"  		=> 4,;
  				"haber"   	=> 5,;
  				"documento" => 6 }                    
	        
  b := {|numero| a := ArrayLineas(numero), oFr:Resync( "Maestro" )}                    
  
  oFr:SetUserDataSet( "Lineas",; 
                      "numero;cuenta;nombre;debe;haber;documento", ;
                     	{|| nAt:= 1, Eval(b, oQry:numero) },;
                     	{|| nAt++, Eval(b, oQry:numero) },;
                     	{|| nAt--, Eval(b, oQry:numero) },;
                     	{|| nAt > Len( a ) },;
                     	{|cFld| nCol := iif(hb_hHasKey(h,cFld), hb_hGet(h,cFld),1), a[nAt,nCol] } )                      
  	                    
  if( File(cFileFr3), oFr:LoadFromFile(cFileFr3), oFr:SetFileName(cFileFr3) )                      
    
  // Inclusion de Variables
  oFr:AddVariable( "Mis variables", "EMPRESA_NOMBRE", "'"+oApp():Session["razon"]+"'" )
  oFr:AddVariable( "Mis variables", "RUT", "'"+oApp():Session["rut"]+"'" )
  oFr:AddVariable( "Mis variables", "NOMBRE", "'"+cNameRut+"'" )
  oFr:AddVariable( "Mis variables", "ID_EMPRESA", oApp():Session["id_empresa"] )
  oFr:AddVariable( "Mis variables", "YEAR", oApp():Session["year"] )
  oFr:AddVariable( "Mis variables", "MONTH", oApp():Session["month"] )
  oFr:AddVariable( "Mis variables", "FECHA", Date() )
  
  // Inclusion de Funciones
  AddFunctionsFrm3( oFr ) 
  
  if lDesigner
     oFr:DesignReport()
     oFr:SaveToFile( cFileFr3 )
  else
     oFr:SetEventHandler("Report", "OnProgressStart", {|x, y| MsgProgress("Espere Por favor...", "Creando Reporte")})
     oFr:ShowReport()
  endif

  oFr:ClearDataSets()
  oFr:DestroyFR()
  
  oQry:End()
  
Return Nil

//---------------------------------------

Function Fr3_LibroDiarioOficial( lDesigner )
  Local oFr
  Local cQuery
  Local oQry
  Local nCol
  Local nAt
  Local a, h, b
  Local cNameRut
  Local cFileFr3 := PATH_FR3 + "libro_diario_oficial.fr3"
    
  cNameRut := AllTrim(oApp():Session["nombre"]) + " " + ;
  						AllTrim(oApp():Session["paterno"]) + " " + ;
  						AllTrim(oApp():Session["materno"]) 
    
  DEFAULT lDesigner := FR3_DESIGNER
  
  TEXT INTO cQuery
  	SELECT 	a.id_empresa, 	
  					a.numero, 			
						a.year,        
						a.month,       
						a.fecha,       
						a.tipo,
						CASE 
							WHEN a.tipo = 1 THEN "INGRESO"
							WHEN a.tipo = 2 THEN "EGRESO"
							WHEN a.tipo = 3 THEN "TRASPASO"
							WHEN a.tipo = 4 THEN "APERTURA"
							ELSE "NINGUNO"
						END AS nametipo,
						a.transaccion,
						CASE 
							WHEN a.transaccion = 1 THEN "COMPRAS"
							WHEN a.transaccion = 2 THEN "VENTAS"
							WHEN a.transaccion = 3 THEN "HONORARIOS"
							WHEN a.transaccion = 4 THEN "OTROS"
							ELSE "NINGUNO"
						END AS nametrans,		
						a.rut,			
						a.tiporut,     
						a.namerut,     
						a.id_docum,		
						b.nombre AS name_docum,
						a.descripcion, 
						a.my_recno 		
    FROM %1 a
    LEFT JOIN %2 b ON a.id_docum = b.id_docum
    WHERE a.id_empresa = %3 AND a.year = %4 AND a.month = %5 
		ORDER BY a.numero
  ENDTEXT	
  
  cQuery := StrFormat(  cQuery, TABLA_MAESTRO,;
                                TABLA_TIPODOCUM,;
  															ClipValue2Sql(oApp():Session["id_empresa"]),;
  															ClipValue2Sql(oApp():Session["year"]),;
  															ClipValue2Sql(oApp():Session["month"]) )
    
  oQry = oSrv:CreateQuery( cQuery )
            
  oFr:= FrReportManager():New( FILE_DLL )
  oFr:LoadLangRes( FILE_XML )
  oFr:SetIcon( ICONFR3 )
  oFr:SetTitle( MSGTITULO_FASTREPORT )
      
  // Maestro Asiento
  oFr:SetUserDataSet( "Maestro",;
  										MySqlFields( oQry ),;
                      {|| oQry:GoTop()},; 
                      {|| oQry:Skip(1)},; 
                      {|| oQry:Skip(-1)},;
                      {|| oQry:Eof() },;
                      {|aField| oQry:FieldGet( aField )} ) 
                      
            
  // Lineas Asiento
  h := {	"numero"		=> 1,;
          "cuenta"		=> 2,;
  				"nombre"  	=> 3,;
  				"debe"  		=> 4,;
  				"haber"   	=> 5,;
  				"documento" => 6 }                    
	        
  b := {|numero| a := ArrayLineas(numero), oFr:Resync( "Maestro" )}                    
  
  oFr:SetUserDataSet( "Lineas",; 
                      "numero;cuenta;nombre;debe;haber;documento", ;
                     	{|| nAt:= 1, Eval(b, oQry:numero) },;
                     	{|| nAt++, Eval(b, oQry:numero) },;
                     	{|| nAt--, Eval(b, oQry:numero) },;
                     	{|| nAt > Len( a ) },;
                     	{|cFld| nCol := iif(hb_hHasKey(h,cFld), hb_hGet(h,cFld),1), a[nAt,nCol] } )                      
  	                    
  if( File(cFileFr3), oFr:LoadFromFile(cFileFr3), oFr:SetFileName(cFileFr3) )                      
    
  // Inclusion de Variables
  oFr:AddVariable( "Mis variables", "EMPRESA_NOMBRE", "'"+oApp():Session["razon"]+"'" )
  oFr:AddVariable( "Mis variables", "RUT", "'"+oApp():Session["rut"]+"'" )
  oFr:AddVariable( "Mis variables", "NOMBRE", "'"+cNameRut+"'" )
  oFr:AddVariable( "Mis variables", "ID_EMPRESA", oApp():Session["id_empresa"] )
  oFr:AddVariable( "Mis variables", "YEAR", oApp():Session["year"] )
  oFr:AddVariable( "Mis variables", "MONTH", oApp():Session["month"] )
  oFr:AddVariable( "Mis variables", "FECHA", Date() )
  
  // Inclusion de Funciones
  AddFunctionsFrm3( oFr ) 
  
  if lDesigner
     oFr:DesignReport()
     oFr:SaveToFile( cFileFr3 )
  else
     oFr:SetEventHandler("Report", "OnProgressStart", {|x, y| MsgProgress("Espere Por favor...", "Creando Reporte")})
     oFr:ShowReport()
  endif

  oFr:ClearDataSets()
  oFr:DestroyFR()
  
  oQry:End()
  
Return Nil

//-------------------------------------

static Function ArrayLineas( numero )
  Local oQry
  Local cQuery
  Local aArray := {}
    
  TEXT INTO cQuery
  	SELECT numero, cuenta, namecuenta, debe, haber, documento 
  	FROM %1
  	WHERE numero = %2
  ENDTEXT
  
  cQuery := StrFormat( cQuery, TABLA_LINEAS, ClipValue2Sql(numero) )
  
  oQry = oSrv:CreateQuery( cQuery )
  
  while !oQry:Eof()
  	AAdd( aArray, { oQry:numero, ;
  									oQry:cuenta, ;
  									oQry:namecuenta, ;
  									oQry:debe, ;
  									oQry:haber, ;
  									oQry:documento } )
  	oQry:Skip()
  enddo
  
  oQry:End()
  
  if Len(aArray) == 0
  	 AAdd( aArray, { 0, "", "", 0, 0, "" } )
  endif 
							  
Return aArray

//---------------------------------------

Function Fr3_LibroMayor( cType, lDesigner )
  Local oFr
  Local oQry
  Local cQuery
  Local cNameRut
  Local cFileFr3 
  Local cuenta  := Space(10)
  Local bBlock  := Nil
      
  cNameRut := AllTrim(oApp():Session["nombre"]) + " " + ;
  						AllTrim(oApp():Session["paterno"]) + " " + ;
  						AllTrim(oApp():Session["materno"]) 
      
  DEFAULT cType 		:= "TODAS",;
          lDesigner := FR3_DESIGNER
         
	if cType == "TODAS"          
	
		cFileFr3 := PATH_FR3 + "libro_mayor.fr3"
		  	
  	MsgProgress("Creando Vista de Reporte",; 
  							"Espere ...",;
  							{|| CreateViewLibroMayor() , CreateTempLibroMayor()} )
  							
  else
  
  	cFileFr3 := PATH_FR3 + "libro_mayor_cuenta.fr3"
  
  	bBlock := <|oGet| 
  	           Local cuenta := Catalogo(TRUE)
  	           oGet:varPut(cuenta)
  	           oGet:refresh()
  	           Return Nil
  	          >
  
  	if MsgGetEx( "Libro Mayor por Cuenta", "Cuenta : ", @cuenta, Nil, aBitmap[ BMP32_PRINT ],; 
							   Nil, bBlock, aBitmap[ BMP16_SEARCH ], "@R 9-9-9-9-9-9")
  		 
  		 MsgProgress("Creando Vista de Reporte",; 
  								 "Espere ...",;
  								 {|| CreateViewLibroMayor(cuenta) , CreateTempLibroMayor()} )
  								 
  	else
  	
  	   Return Nil							 
  	
  	endif							 
  
  endif							
  
  TEXT INTO cQuery
  	SELECT  numero,
      			cuenta,					
						namecuenta,			
      			debe,						
						haber,						
      			documento,				
      			id_empresa,			
						fecha,						
						year,						
						month,						
						id_docum,				
      			namedocum,				
      			descripcion,			
      			tipo,						
      			nametipo,				
      			debe_anterior,		
      			haber_anterior,	
      			saldo_anterior,	
      			debe_actual,			
      			haber_actual,		
      			deudor,          
      			acreedor        
  	FROM %1 
  	ORDER BY cuenta, numero
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TEMP_LIBRO_MAYOR )
  
  oQry = oSrv:CreateQuery( cQuery )
            
  oFr:= FrReportManager():New( FILE_DLL )
  oFr:LoadLangRes( FILE_XML )
  oFr:SetIcon( ICONFR3 )
  oFr:SetTitle( MSGTITULO_FASTREPORT )
      
  // Maestro Asiento
  oFr:SetUserDataSet( "Libro",;
  										MySqlFields( oQry ),;
                      {|| oQry:GoTop()},; 
                      {|| oQry:Skip(1)},; 
                      {|| oQry:Skip(-1)},;
                      {|| oQry:Eof() },;
                      {|aField| oQry:FieldGet( aField )} ) 
      	                    
  if( File(cFileFr3), oFr:LoadFromFile(cFileFr3), oFr:SetFileName(cFileFr3) )                      
    
  // Inclusion de Variables
  oFr:AddVariable( "Mis variables", "EMPRESA_NOMBRE", "'"+oApp():Session["razon"]+"'" )
  oFr:AddVariable( "Mis variables", "RUT", "'"+oApp():Session["rut"]+"'" )
  oFr:AddVariable( "Mis variables", "NOMBRE", "'"+cNameRut+"'" )
  oFr:AddVariable( "Mis variables", "ID_EMPRESA", oApp():Session["id_empresa"] )
  oFr:AddVariable( "Mis variables", "YEAR", oApp():Session["year"] )
  oFr:AddVariable( "Mis variables", "MONTH", oApp():Session["month"] )
  oFr:AddVariable( "Mis variables", "GIRO", "'"+oApp():Session["giro"]+"'" )
  oFr:AddVariable( "Mis variables", "FECHA", Date() )
  
  // Inclusion de Funciones
  AddFunctionsFrm3( oFr ) 
  
  if lDesigner
     oFr:DesignReport()
     oFr:SaveToFile( cFileFr3 )
  else
     oFr:SetEventHandler("Report", "OnProgressStart", {|x, y| MsgProgress("Espere Por favor...", "Creando Reporte")})
     oFr:ShowReport()
  endif

  oFr:ClearDataSets()
  oFr:DestroyFR()
  
  oQry:End()
  
Return Nil

//---------------------------------------

static Function CreateViewLibroMayor( cCuenta )
	Local cQuery
	Local nMesAnt, cMesAnt
  Local nMes, cMes
  
  DEFAULT cCuenta := ""
  
  // Para Asignar los saldos del mes anterior
  nMes := oApp():Session["month"]
  cMes := Strzero(nMes,2) 						
  if nMes == 1		
     nMesAnt := 12
	   cMesAnt := Str(nMesAnt,2)  						
	else
	   nMesAnt := nMes - 1
	   cMesAnt := StrZero(nMesAnt,2)  						
	endif
	
	if Empty(cCuenta)
		
			TEXT INTO cQuery
				CREATE OR REPLACE VIEW %1 AS
  		  	SELECT  a.numero,
  		      			a.cuenta,
					  			a.namecuenta,
  		      			a.debe,
					  			a.haber,
  		      			a.documento,
  		      			b.id_empresa,
					  			b.fecha,
									b.year,
									b.month,
									b.id_docum,
  		      			CONCAT(LPAD(b.id_docum,3,'0'),'=',c.nombre) AS namedocum,
  		      			b.descripcion,
  		      			b.tipo,
  		      			CASE 
										WHEN b.tipo = 1 THEN "INGRESO"
										WHEN b.tipo = 2 THEN "EGRESO"
										WHEN b.tipo = 3 THEN "TRASPASO"
										WHEN b.tipo = 4 THEN "APERTURA"
										ELSE "NINGUNO"
									END AS nametipo,
  		      			d.deb_%10 AS debe_anterior,
  		      			d.hab_%10 AS haber_anterior,
  		      			d.deb_%9  AS debe_actual,
  		      			d.hab_%9  AS haber_actual
  		    FROM %2 AS a
					LEFT JOIN %3 AS b ON a.numero = b.numero
					LEFT JOIN %4 AS c ON b.id_docum = c.id_docum
					LEFT JOIN %5 AS d ON a.cuenta = d.cuenta
					WHERE b.id_empresa = $6 AND b.year = $7 AND b.month = $8
  		    ORDER BY a.cuenta, a.numero 
			ENDTEXT 
			
			cQuery := BindParams( cQuery, VISTA_LIBRO_MAYOR,;
																		TABLA_LINEAS,;     
																		TABLA_MAESTRO,;    
																		TABLA_TIPODOCUM,;  
																		TABLA_BALANCE,;    
			                              oApp():Session["id_empresa"],;   
  																	oApp():Session["year"],;
  																	oApp():Session["month"],;
  																	cMes,;				//  9 Mes Actual
  																	cMesAnt )			// 10 Mes Anterior	
  																	
	else 
	
		TEXT INTO cQuery
				CREATE OR REPLACE VIEW %1 AS
  		  	SELECT  a.numero,
  		      			a.cuenta,
					  			a.namecuenta,
  		      			a.debe,
					  			a.haber,
  		      			a.documento,
  		      			b.id_empresa,
					  			b.fecha,
									b.year,
									b.month,
									b.id_docum,
  		      			CONCAT(LPAD(b.id_docum,3,'0'),'=',c.nombre) AS namedocum,
  		      			b.descripcion,
  		      			b.tipo,
  		      			CASE 
										WHEN b.tipo = 1 THEN "INGRESO"
										WHEN b.tipo = 2 THEN "EGRESO"
										WHEN b.tipo = 3 THEN "TRASPASO"
										WHEN b.tipo = 4 THEN "APERTURA"
										ELSE "NINGUNO"
									END AS nametipo,
  		      			d.deb_%11 AS debe_anterior,
  		      			d.hab_%11 AS haber_anterior,
  		      			d.deb_%10  AS debe_actual,
  		      			d.hab_%10  AS haber_actual
  		    FROM %2 AS a
					LEFT JOIN %3 AS b ON a.numero = b.numero
					LEFT JOIN %4 AS c ON b.id_docum = c.id_docum
					LEFT JOIN %5 AS d ON a.cuenta = d.cuenta
					WHERE a.cuenta = $6 AND b.id_empresa = $7 AND b.year = $8 AND b.month = $9 
  		    ORDER BY a.cuenta, a.numero 
			ENDTEXT 
			
			cQuery := BindParams( cQuery, VISTA_LIBRO_MAYOR,;
																		TABLA_LINEAS,;     
																		TABLA_MAESTRO,;    
																		TABLA_TIPODOCUM,;  
																		TABLA_BALANCE,;    
																		cCuenta,;
			                              oApp():Session["id_empresa"],;   
  																	oApp():Session["year"],;
  																	oApp():Session["month"],;
  																	cMes,;				// 10 Mes Actual
  																	cMesAnt )			// 11 Mes Anterior	
	  																	
	endif	  																	
	
	oSrv:Execute( cQuery )

Return Nil

//---------------------------------------

static Function CreateTempLibroMayor()
	Local cQuery
  Local oQry
  Local cuenta
  Local item      := 0
  Local saldo			:= 0
  Local deudor		:= 0
  Local acreedor	:= 0
  
  oSrv:Execute( "TRUNCATE " + TEMP_LIBRO_MAYOR )
    
	TEXT INTO cQuery
    SELECT  numero,
       		 	cuenta,
			 			namecuenta,
       			debe,
			 			haber,
       			documento,
       			id_empresa,
			 			fecha,
			 			year,
			 			month,
			 			id_docum,
       			namedocum,
       			descripcion,
       			tipo,
       			nametipo,
       			debe_anterior,
       			haber_anterior,
       			(debe_anterior-haber_anterior) AS saldo_anterior,
       			debe_actual,
       			haber_actual
    FROM %1 
		ORDER BY cuenta
  ENDTEXT	
  
  cQuery := StrFormat(  cQuery, VISTA_LIBRO_MAYOR )
  
  oQry = oSrv:CreateQuery( cQuery )
  
  while !oQry:Eof()
    
    cuenta := oQry:cuenta
    saldo  := oQry:saldo_anterior
    
    while (oQry:cuenta == cuenta) .and. !oQry:Eof()
      
      deudor		:= 0
  		acreedor	:= 0
      item 			:= (saldo + oQry:debe - oQry:haber)
      
      if item > 0
         deudor   := item
      else
         acreedor := item   
      endif   
      
      saldo := item
      
      oSrv:InsertHash(  TEMP_LIBRO_MAYOR,; 
      									{ "numero"       		=> oQry:numero,;                                           
													"cuenta"       		=> oQry:cuenta,;                                           
													"namecuenta"   		=> oQry:namecuenta,;    
													"debe"         		=> oQry:debe,;                                             
													"haber"        		=> oQry:haber,;                                            
													"documento"    		=> oQry:documento,;                                        
													"id_empresa"   		=> oQry:id_empresa,;                                       
													"fecha"        		=> oQry:fecha,;                                            
													"year"         		=> oQry:year,;                                             
													"month"        		=> oQry:month,;                                            
													"id_docum"     		=> oQry:id_docum,;                                         
													"namedocum"    		=> oQry:namedocum,;                                        
													"descripcion"  		=> oQry:descripcion,;                                      
													"tipo"         		=> oQry:tipo,;                                             
													"nametipo"        => oQry:nametipo,;                                      
													"debe_anterior"   => oQry:debe_anterior,;                                 
													"haber_anterior"  => oQry:haber_anterior,;                                
													"saldo_anterior"  => oQry:saldo_anterior,;
													"debe_actual"     => oQry:debe_actual,;                                   
													"haber_actual"    => oQry:haber_actual,;
													"deudor"  				=> deudor,;                                
													"acreedor"  			=> acreedor } );                                  
			
			oQry:Skip()
			
		enddo
		
	enddo															
    
  oQry:End()
    
Return Nil

//---------------------------------------

Function Crear_TempLibroMayor()
  Local cQuery

	TEXT INTO cQuery
		CREATE TABLE IF NOT EXISTS %1
		( numero					int(10),
      cuenta					char(6),
			namecuenta			char(60),
      debe						decimal(12,2),
			haber						decimal(12,2),
      documento				char(60),
      id_empresa			int(10),
			fecha						date,
			year						int(4),
			month						int(4),
			id_docum				int(4),
      namedocum				char(60),
      descripcion			char(60),
      tipo						int(4),
      nametipo				char(60),
      debe_anterior		decimal(12,2),
      haber_anterior	decimal(12,2),
      saldo_anterior	decimal(12,2),
      debe_actual			decimal(12,2),
      haber_actual		decimal(12,2),
      deudor          decimal(12,2),
      acreedor        decimal(12,2)
    ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB   			
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TEMP_LIBRO_MAYOR)
  
  oSrv:VerifyTable( TEMP_LIBRO_MAYOR, cQuery ) 	
          
Return Nil

// FINAL
