#include "fivewin.ch"
#include "contabil.ch"

//----------------------------------------

CLASS TVariable

  DATA oServer
  DATA cDataBaseSQL
  DATA lSucess
  
  DATA oWnd
  DATA isLegal
	DATA fileAvcsis
	  
  DATA Path_Root READONLY  
  DATA cDirData  READONLY         
  DATA cDirFr3   READONLY     
	DATA cDirDll   READONLY  
	DATA cDirPdf   READONLY   
	
	DATA BrushColor
	
	DATA aBitmaps
		
	// Variable que controla la session
	DATA Session
	DATA Version
	
	// Variables de Archivos	
	DATA Empresas
	DATA Cliente
	DATA Provedor
	DATA CatalogoMaestro
	DATA Catalogo
	DATA TipoEmpresa
	DATA Balance
	DATA AsientoMaestro
	DATA AsientoLineas
	DATA Regiones
	DATA Ciudades
	DATA Comunas
	DATA TipoDocum
		
	DATA hCargo PROTECTED
		
  METHOD New()
  METHOD LoadBitMaps()
  METHOD LoadVarFiles()
  METHOD SetTitle() 
  
  METHOD AddVar( uKey, uVal ) INLINE ::hCargo[ uKey ] := uVal
  
  ERROR HANDLER OnError
   
ENDCLASS

//----------------------------------------

METHOD New() CLASS TVariable
  Local cDrive := hb_CurDrive()
      
  ::lSucess			 	=	TRUE	
  ::cDataBaseSQL 	= "database_contabilidad"
  ::oServer 			= TMySqlServer():New( ::cDataBaseSQL )
  
  if ! hb_IsObject( ::oServer:oSrv )
     ::oServer:DialogConnect()
     ::lSucess = FALSE
  endif   

	// Asignacion de variables de Session  
  ::Session = {=>}
	::Session["id_empresa"]	= 0
	::Session["nomtipo"]    = "NO"
	::Session["activa"]     = FALSE 
	::Session["rut"]				= "NO"
	::Session["razon"]			= "NO"
	::Session["nombre"]		 	= "Seleccione una empresa"
	::Session["paterno"]		= "NO"
	::Session["materno"]		= "NO"
	::Session["dir1"]				= ""
	::Session["dir2"]				= ""
	::Session["region"]			= ""
	::Session["ciudad"]			= ""
	::Session["comuna"]			= ""
	::Session["telefono"]		= ""
	::Session["celular"]		= ""
	::Session["casilla"]		= ""
	::Session["codgiro"]		= ""
	::Session["nomgiro"]		= ""
	::Session["email"]			= ""
	::Session["year"]				= Year(Date())
	::Session["month"]			= Month(Date())
	::Session["namemonth"]  = Upper(MonthC(::Session["month"]))
	
	::Version = "Contabilidad PYME, v1.0"
	
	::BrushColor  = RGB(174, 174, 174)
      
  ::Path_Root 	= cDrive + ":\" + CurDir() + "\"
  
  ::cDirData  = ::Path_Root + "data\"
  ::cDirFr3   = ::Path_Root + "fr3\"
	::cDirDll   = ::Path_Root + "dll\"
	::cDirPdf   = ::Path_Root + "pdf\"
		
	// Crea los Directorios
  Do_MakeDir( ::cDirData )
  Do_MakeDir( ::cDirFr3  )
  Do_MakeDir( ::cDirDll	 )
  Do_MakeDir( ::cDirPdf  )
    		
  // Carga los BitMaps e Icono del Sistema
  ::LoadBitMaps()
  
  // Nombre de archivos 
  ::LoadVarFiles()
  
  ::hCargo = {=>}
  HB_HSetCaseMatch( ::hCargo, .f. )
  HB_HSetAutoAdd(   ::hCargo, .t. )
    
return Self

//----------------------------------------

METHOD OnError(...) CLASS TVariable

   local uRet, e
   local cMessage    := __GetMessage()
   local aParams     := HB_AParams()
   local uParam1     := If( Empty( aParams ), nil, aParams[ 1 ] )
   local lAssign     := .f.

   if Left( cMessage, 1 ) == '_'
      lAssign     := .t.
      cMessage    := Substr( cMessage, 2 )
   endif

   if HB_HHasKey( ::hCargo, cMessage )
      if lAssign
         ::hCargo[ cMessage ] := uParam1
         uRet  := ::hCargo[ cMessage ]
      else
         if ValType( uRet := ::hCargo[ cMessage ] ) == 'B'
            AIns( aParams, 1, Self, .t. )
            uRet  := HB_ExecFromArray( uRet, aParams )
         endif
      endif
   else
       _ClsSetError( _GenError( If( lAssign, 1005, 1004 ), ::ClassName(), cMessage ) )
   endif

return uRet

//----------------------------------------

METHOD LoadBitMaps() CLASS TVariable

  ::aBitmaps := Array( LEN_BITMAPS )
	::aBitmaps[ BMP32_EMPRESA ]		:= "EMPRESA_32"
	::aBitmaps[ BMP32_NEW ]				:= "NEW_32"
	::aBitmaps[ BMP32_PLUS ]			:= "PLUS_32"
	::aBitmaps[ BMP32_EDIT ]			:= "EDIT_32"
	::aBitmaps[ BMP32_MINUS ]			:= "MINUS_32"
	::aBitmaps[ BMP32_EXIT ]			:= "EXIT_32"
	::aBitmaps[ BMP32_BACKGRND ]	:= "BACKGROUND"
	::aBitmaps[ BMP32_PROCESS ]		:= "PROCESS_32"
	::aBitmaps[ BMP32_CANCEL ]		:= "CANCEL_32"
	::aBitmaps[ BMP32_YES ]				:= "YES_32"
	::aBitmaps[ BMP32_RUN ]				:= "RUN_32"
	::aBitmaps[ BMP32_ICON ]			:= "AVCSIS"
	::aBitmaps[ BMP32_SEARCH ]		:= "SEARCH_32"
	::aBitmaps[ BMP32_INDEX ]			:= "INDEX_32"
	::aBitmaps[ BMP32_TOP ]				:= "TOP_32"
	::aBitmaps[ BMP32_BOTTOM ]		:= "BOTTOM_32"
	::aBitmaps[ BMP32_PRINT ]			:= "PRINT_32"
	::aBitmaps[ BMP32_REPORT ]		:= "REPORT_32"
	::aBitmaps[ BMP32_PEOPLE ]		:= "PEOPLE_32"
	::aBitmaps[ BMP32_USER ]			:= "USER_32"
	::aBitmaps[ BMP32_OPEN ]			:= "OPEN_32"
	::aBitmaps[ BMP32_MONEY ]			:= "MONEY_32"
	::aBitmaps[ BMP32_VENTAS ]		:= "VENTAS_32"
	::aBitmaps[ BMP32_EXCEL ]			:= "EXCEL_32"
	::aBitmaps[ BMP32_DBF ]				:= "DBF_32"
	::aBitmaps[ BMP32_COMPRAS ]		:= "SHOPPING_32"
	::aBitmaps[ BMP32_CATALOGO ]	:= "CATALOGO_32"
	::aBitmaps[ BMP32_DISABLE ]		:= "DISABLE_32"
	::aBitmaps[ BMP32_INVENTARIO ]:= "PRODUCTOS_32"
	::aBitmaps[ BMP32_PARTNER ]		:= "PARTNER_32"
	::aBitmaps[ BMP32_OFFER ]			:= "OFFER_32"
	::aBitmaps[ BMP32_SAVE ]			:= "SAVE_32"
	::aBitmaps[ BMP32_LLAVE ] 		:= "LLAVE_32"
	::aBitmaps[ BMP32_NOTES ] 		:= "NOTES_32"
	::aBitmaps[ BMP32_ADMIN ]			:= "ADMIN"
	::aBitmaps[ BMP32_BARCODE ]		:= "BARCODE"
	::aBitmaps[ BMP32_CREDITCARD ]:= "CREDIT_CARD"
	::aBitmaps[ BMP32_DRAWER ]		:= "DRAWER"
	::aBitmaps[ BMP32_LENTE ]			:= "LENTE"
	::aBitmaps[ BMP32_MONTURA ]		:= "MONTURA"
	::aBitmaps[ BMP32_ORDENLAB ]	:= "BARRICADE"
	::aBitmaps[ BMP32_DELETE ]		:= "DELETE_RECORD"
	::aBitmaps[ BMP32_SERVICIO ]	:= "SERVICIO"
	::aBitmaps[ BMP32_FINISH ]		:= "FINISH"
	::aBitmaps[ BMP32_BROWSE ]		:= "BROWSE"
	::aBitmaps[ BMP32_IMPORTAR ]	:= "IMPORTAR_32"
	::aBitmaps[ BMP32_CLEAR ]	    := "BORRAR"
	::aBitmaps[ BMP32_DESIGNER ]	:= "DESIGNER"
	::aBitmaps[ BMP32_MESA ]	    := "MESA"
	::aBitmaps[ BMP32_MESONERO ]	:= "MESONERO"
	::aBitmaps[ BMP32_FOLDERADD ] := "FOLDER_ADD"
	::aBitmaps[ BMP32_FOLDERLIS ] := "FOLDER_SEARCH"
	::aBitmaps[ BMP32_FOLDERCHK ] := "FOLDER_CHEKED"
	::aBitmaps[ BMP32_BALANZA ] 	:= "BALANZA"
	::aBitmaps[ BMP32_RETENCION ] := "RETENCION"
	::aBitmaps[ BMP32_COMPONENTES ] := "COMPONENTES"
	::aBitmaps[ BMP32_CODESEARCH ]  := "CODE_SEARCH" 
	::aBitmaps[ BMP32_HANDOK ]  	:= "HANDOK" 
	::aBitmaps[ BMP16_SEARCH ]		:= "SEARCH_16"
	::aBitmaps[ BMP16_ON ]				:= "ON_16"
	::aBitmaps[ BMP16_OFF ]				:= "OFF_16"
	::aBitmaps[ BMP16_CALC ]			:= "GETCALC_16"
	::aBitmaps[ BMP16_CREDITCARD ]:= "CREDITCARD_16"
	::aBitmaps[ BMP16_REFRESH ]		:= "REFRESH_16"
	::aBitmaps[ BMP16_CALENDAR ]	:= "CALENDAR_16" 
			 
return Nil

//----------------------------------------

METHOD LoadVarFiles() CLASS TVariable

	::Cliente 				= "cliente"
	::Provedor        = "provedor"
	::CatalogoMaestro = "catalogo_maestro"
	::Catalogo        = "catalogo"
	::Empresas  			= "empresas"
	::TipoEmpresa			= "tipo_empresa"
	::Balance         = "balance"
	::AsientoMaestro 	= "asiento_maestro"
	::AsientoLineas   = "asiento_lineas"
  ::Regiones    		= "regiones"
  ::Ciudades    		= "ciudades"
  ::Comunas     		= "comunas"
  ::TipoDocum				= "tipo_documento"
  
return Nil

//----------------------------------------

METHOD SetTitle() CLASS TVariable
	Local cTitle := ""
	
	if ::Session["activa"]
	
		 cTitle += ::Version
		 cTitle += Space(4)
		 
		 cTitle += "EMPRESA : "
		 cTitle += StrZero( ::Session["id_empresa"], 4 )
		 cTitle += ", "
		 cTitle += AllTrim( ::Session["razon"] )
		 cTitle += Space(4) 
		 
		 cTitle += "RUT : "
		 cTitle += AllTrim( ::Session["rut"] )
		 cTitle += Space(4) 
		 
		 cTitle += "NOMBRE : "
		 cTitle += AllTrim( ::Session["nombre"] ) + ", "
		 cTitle += AllTrim( ::Session["paterno"] )
		 cTitle += Space(4) 
		 
		 cTitle += "ACTIVIDAD : "
		 cTitle += AllTrim( ::Session["nomtipo"] )
		 cTitle += Space(4) 
		 
		 cTitle += "EJERCICIO {"
		 cTitle += "A�O : " + Str( ::Session["year"], 4 )
		 cTitle += ", MES : " + StrZero( ::Session["month"], 2 )
		 cTitle += " " + ::Session["namemonth"] + "}"
		 		 
	else 
	
		 cTitle += ::Version
		 cTitle += Space(4)
		 cTitle += "<SELECCIONE UNA EMPRESA PARA TRABAJAR>"	 
	
	endif
	
	::oWnd:cTitle( cTitle )
	
return Nil	

// FINAL