// CATALOGO

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_REGIONES =>	oApp():Regiones

//---------------------------------------

Function Regiones( lBuscar )
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local bBlock
  Local bKey
  Local bClick
  Local h     := {=>}  
  Local lExit := FALSE
  
  h["id_region"] := 0
  
  DEFAULT lBuscar := FALSE
    
  bBlock := {|| h["id_region"]:= oQry:id_region, lExit:= TRUE, oDlg:End() }
  if lBuscar
     bClick := bBlock
     bKey   := bBlock
  else
     bClick	:= Nil
  	 bKey 	:= bBlock
  endif  
    
  TEXT INTO cQuery
  	SELECT 	id_region,
  					nombre,
						my_recno 		
  	FROM %1 
  	ORDER BY nombre
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TABLA_REGIONES )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE "TABLA REGIONES " RESOURCE "xbrowse" ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "id_region", "Num. Region" , Nil, 100, AL_LEFT },; 
    { "nombre"   , "Nombre"      , Nil, 400, AL_LEFT } ;
   }    
 
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyDown     = {|nKey| if(nKey==VK_RETURN, Eval(bKey), Nil)}
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= bClick
  END
      
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) );
    ON INIT ( DisableSysmenuDlg(oDlg), CreaButonBar( oDlg, oBrw, oQry, @h, @lExit ) )
    
  RELEASE FONT oFont  
    
  oQry:End()

Return h["id_region"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, h, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70

  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() )

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( LeaDatos( oQry, FALSE ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Reporte Regiones"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
   
Return Nil

//---------------------------------------

// http://forums.fivetechsupport.com/viewtopic.php?f=6&t=31223

static Function LeaDatos( oQry, lNew )
	Local oDlg
	Local oFont
	Local oGet 		:= Array(2)
	Local oBtn    := Array(2)
	Local h       := { => }
	Local lExit   := FALSE
	Local lProcess:= FALSE
	Local cTitle  := if(lNew, "AGREGAR REGION", "MODIFICA REGION")
	
	h["id_region"] := if(lNew, LastIdRegion(), oQry:id_region)
	h["nombre"]    := if(lNew, Space(60), oQry:nombre)
	 	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "REGIONES" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "Codigo :"
	REDEFINE SAY ID 101 OF oDlg PROMPT "Nombre :"
	 	
	REDEFINE GET oGet[1] VAR h["id_region"] ID 200 OF oDlg;
		WHEN FALSE
		
	REDEFINE GET oGet[2] VAR h["nombre"]	  ID 201 OF oDlg;
		PICTURE "@!"
    	
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
	
	ACTIVATE DIALOG oDlg;
		VALID lExit;
		ON INIT ( DisableSysMenuDlg(oDlg) )
		
  if lProcess
   	 if lNew
   	    MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_REGIONES,;
  									{ "id_region" => h["id_region" ],;
  							  		"nombre"   	=> h["nombre"    ] } )
  			  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_REGIONES ,;
  									{ "nombre"  => h["nombre" ] } ,;
  								   	"my_recno = " +ClipValue2SQL(oQry:my_recno) )
  										  
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )
  
  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_REGIONES, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

Function SiIdRegion( id_region )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE id_region = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_REGIONES, ClipValue2SQL(id_region) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

static Function LastIdRegion()
  Local oQry
  Local cQuery
  Local id := 0
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	ORDER BY id_region DESC
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_REGIONES )	
    
  oQry = oSrv:CreateQuery( cQuery )

	if ( oQry:RecCount() > 0 )
  	id := oQry:id_region
	endif  	
  
  oQry:End()
   
  id++
  
Return id

//---------------------------------------

Function GetArrayRegiones()
	Local oQry
	lOCAL cQuery
	Local aArray := {}
	
	TEXT INTO cQuery
		SELECT id_region, nombre
		FROM %1
		ORDER BY id_region
	ENDTEXT
	
	cQuery := StrFormat( cQuery, TABLA_REGIONES )

	oQry = oSrv:CreateQuery( cQuery )
	
	while !oQry:Eof()
		AAdd( aArray, { oQry:id_region, oQry:nombre } )
		oQry:Skip()
	enddo
	
	oQry:End()
	
	if Len(aArray) == 0
		 AAdd( aArray, { 0, "No hay Regiones" } )
	endif
	
Return aArray	

//---------------------------------------

Function GetNameRegion( id )
	Local oQry
	Local cQuery
	Local cName := ""
		
	TEXT INTO  cQuery
		SELECT id_region, nombre
		FROM %1
		WHERE id_region = %2
		LIMIT 1
	ENDTEXT
	
	cQuery := StrFormat( cQuery,TABLA_REGIONES, ClipValue2Sql(id) )

	oQry = oSrv:CreateQuery( cQuery )
	
	if oQry:RecCount() > 0
		cName := oQry:nombre	
	endif
	
	oQry:End()
	
Return cName

//---------------------------------------

Function Crear_Regiones()
  Local cStruct
  
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( id_region	  int(10) NOT NULL DEFAULT 0,
     	nombre    	char(80) NOT NULL DEFAULT ' ',
     	my_recno	  int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX nombre (nombre)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_REGIONES )
  
  oSrv:VerifyTable( TABLA_REGIONES, cStruct ) 	
          
Return Nil

// FINAL