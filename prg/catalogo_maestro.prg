// CATALOGO MAESTRO

#include "fivewin.ch"
#include "xbrowse.ch"
#include "contabil.ch"

//---------------------------------------

#xtranslate  	oSrv 	=> 	oApp():oServer  

//---------------------------------------

#xtranslate		aBitmap 			=>	oApp():aBitmaps
#xtranslate  	Name_Empresa 	=>  oApp():Version

//---------------------------------------

#xtranslate		TABLA_CATALOGO =>	oApp():CatalogoMaestro

//---------------------------------------

Function Catalogo_Maestro()
  Local oQry
  Local oDlg, oBrw, oFont
  Local cQuery 
  Local aCols
  Local cOrder
  Local h      := {=>}  
  Local lExit  := FALSE
      
  h["cuenta"] := Space(10)
       
  TEXT INTO cQuery
  	SELECT 	cuenta,      
						nombre,    	
						nivel,       
						CASE
							WHEN nivel = 1 THEN "ACTIVO"
							WHEN nivel = 2 THEN "PASIVO"
							WHEN nivel = 3 THEN "PERDIDAS"
							WHEN nivel = 4 THEN "GANANCIAS"
							WHEN nivel = 5 THEN "ORDEN" 
						END AS namenivel,
						IF(imputable="S","0= SI","1= NO") AS imputable,		
						IF(activa="S","0= SI","1= NO") AS activa,    	
						IF(analisis="S","0= SI","1= NO") AS analisis,   	
						my_recno 		
  	FROM %1 
  	ORDER BY nombre
  ENDTEXT	
  
  cQuery := StrFormat( cQuery, TABLA_CATALOGO )
    
  oQry = oSrv:CreateQuery( cQuery )
  
  DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-11 BOLD

  DEFINE DIALOG oDlg TITLE "PLAN DE CUENTAS, CATALOGO MAESTRO"; 
  	RESOURCE "xbrowse" ICON aBitmap[ BMP32_ICON ] FONT oFont
      
  aCols := { ;
  	{ "cuenta"      , "Cuenta" 	  , "@R 9-9-9-9-9-9", 120, AL_LEFT },;
  	{ "nombre"  		, "Nombre"    , Nil, 400, AL_LEFT },;
  	{ "nivel"       , "Nivel"     , Nil,  35, AL_RIGHT},;
  	{ "namenivel"   , "NomNivel"  , Nil, 120, AL_LEFT },;
  	{ "imputable" 	, "Imputable" , Nil, 120, AL_LEFT },; 
  	{ "activa" 	    , "Activa"    , Nil, 120, AL_LEFT },; 
   	{ "analisis"  	, "Analisis"	, Nil, 120, AL_LEFT } ;
  }    
      
  REDEFINE XBROWSE oBrw ID 100 OF oDlg;
    DATASOURCE oQry;
    COLUMNS aCols;
    LINES FOOTERS AUTOSORT
  
  WITH OBJECT oBrw
    :MyConfig()
    :bKeyDown     = {|nKey| if(nKey==VK_RETURN, Eval(oBrw:bLDblClick), Nil)}
    :bKeyChar     = {|nKey| QuerySeekWild( oBrw, oQry, cQuery, nKey )}
    :bChange      = oBrw:bClrHeaderFooter()
    :bLDblClick 	= {|| h["cuenta"]:= oQry:cuenta, lExit:= TRUE, oDlg:End() }
  END
    
  REDEFINE SAY ID 101 OF oDlg 
  REDEFINE SAY oBrw:oSeek VAR oBrw:cSeek ID 102 OF oDlg COLOR CLR_RED,CLR_YELLOW PICTURE '@!' 
  
  oDlg:bInit = <||
                DisableSysmenuDlg(oDlg)
                CreaButonBar( oDlg, oBrw, oQry, @lExit )
  							Return Nil
  						>	
  
  ACTIVATE DIALOG oDlg; 
    CENTER;
    VALID if( lExit, TRUE, lExit:= MsgYesNo( "Desea Salir ?", Name_Empresa ) )
    
  RELEASE FONT oFont  
    
  oQry:End()

Return h["cuenta"]

//---------------------------------------

static Function CreaButonBar( oDlg, oBrw, oQry, lExit )
  Local oBar
  
  DEFINE BUTTONBAR oBar OF oDlg 2010 SIZE 70, 70
    
  DEFINE BUTTON OF oBar; 
  	PROMPT "Nuevo"; 
  	RESOURCE aBitmap[ BMP32_PLUS ];  
    ACTION ( LeaDatos( oQry, TRUE ), oBrw:Refresh(), oBrw:Setfocus() )

  DEFINE BUTTON OF oBar; 
  	PROMPT "Modificar"; 
  	RESOURCE aBitmap[ BMP32_EDIT ]; 
  	ACTION ( LeaDatos( oQry, FALSE ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP

  DEFINE BUTTON OF oBar; 
  	PROMPT "Eliminar"; 
  	RESOURCE aBitmap[ BMP32_MINUS ]; 
  	ACTION ( DelRecord( oQry ), oBrw:Refresh(), oBrw:Setfocus() ); 
  	GROUP
      		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Inicio"; 
  	RESOURCE aBitmap[ BMP32_TOP ]; 
  	ACTION ( oBrw:GoTop(), oBrw:Setfocus() ); 
  	GROUP
  		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Final"; 
  	RESOURCE aBitmap[ BMP32_BOTTOM ]; 
  	ACTION ( oBrw:GoBottom(), oBrw:Setfocus() ) 
    		
  DEFINE BUTTON OF oBar; 
  	PROMPT "Imprimir"; 
  	RESOURCE aBitmap[ BMP32_PRINT ]; 
  	ACTION ( oBrw:Report("Reporte Catalogo"), oBrw:Setfocus() ); 
  	GROUP				
    	
  DEFINE BUTTON OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE aBitmap[ BMP32_EXIT ]; 
  	ACTION ( oDlg:End() ); 
  	GROUP
     
Return Nil

//---------------------------------------

// http://forums.fivetechsupport.com/viewtopic.php?f=6&t=31223

static Function LeaDatos( oQry, lNew )
	Local oDlg
	Local oFont
	Local oGet 			:= Array(6)
	Local oBtn    	:= Array(2)
	Local h       	:= { => }
	Local aTipos   	:= ArrayNivel()
	Local aIndNivel := ArrTranspose(aTipos)[1] 
	Local aNomNivel := ArrTranspose(aTipos)[2] 
	Local lExit   	:= FALSE
	Local lProcess	:= FALSE
	Local cTitle  	:= if(lNew, "AGREGAR PLAN DE CUENTA", "MODIFICA PLAN DE CUENTA")
	
	h["cuenta"]    := if(lNew, Space(6), oQry:cuenta)
 	h["nombre"]    := if(lNew, Space(60), oQry:nombre)
	h["nivel"]     := if(lNew, aIndNivel[1], oQry:nivel)
	h["namenivel"] := if(lNew, aNomNivel[1], aNomNivel[oQry:nivel])
 	h["imputable"] := if(lNew, ArraySiNo()[1], oQry:imputable)
 	h["activa"]    := if(lNew, ArraySiNo()[1], oQry:activa)
 	h["analisis"]  := if(lNew, ArraySiNo()[1], oQry:analisis)
 	 	
 	DEFINE FONT oFont NAME "TAHOMA" SIZE 0,-12 BOLD
 
	DEFINE DIALOG oDlg TITLE cTitle RESOURCE "CATALOGO_MAESTRO" FONT oFont
	
	REDEFINE GROUP ID 4000 OF oDlg
	
	REDEFINE SAY ID 100 OF oDlg PROMPT "Cuenta :" 
	REDEFINE SAY ID 101 OF oDlg PROMPT "Nombre :"
  REDEFINE SAY ID 102 OF oDlg PROMPT "Nivel :" 
	REDEFINE SAY ID 103 OF oDlg PROMPT "Imputable:"
  REDEFINE SAY ID 104 OF oDlg PROMPT "Activa :" 
	REDEFINE SAY ID 105 OF oDlg PROMPT "Analisis:"
	 	
	REDEFINE GET oGet[1] VAR h["cuenta"]  ID 200 OF oDlg PICTURE "@R 9-9-9-9-9-9";
		WHEN lNew
	  oGet[1]:bValid = <|oGet|
	  									if SiCuenta( oGet:varget() )
	  									   MsgStop("Cuenta Ya Existe", Name_Empresa)
	  										 Return FALSE
	  									endif
	  									Return TRUE
	  								>	
		
  REDEFINE GET oGet[2] VAR h["nombre"] ID 201 OF oDlg
  
  REDEFINE COMBOBOX oGet[3] VAR h["namenivel"] ITEMS aNomNivel	ID 202 OF oDlg;
		ON CHANGE ( h["nivel"] := aIndNivel[ oGet[3]:nAt ] )
  
	REDEFINE COMBOBOX oGet[4] VAR h["imputable"] ITEMS ArraySiNo() ID 203 OF oDlg
  REDEFINE COMBOBOX oGet[5] VAR h["activa"] 	 ITEMS ArraySiNo() ID 204 OF oDlg
	REDEFINE COMBOBOX oGet[6] VAR h["analisis"]  ITEMS ArraySiNo() ID 205 OF oDlg
	
	REDEFINE BTNBMP oBtn[1] ID 300 OF oDlg;
	  CENTER;
		PROMPT "Grabar";
		NOROUND;
		ACTION ( lProcess := TRUE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradGreen()
		
	REDEFINE BTNBMP oBtn[2] ID 301 OF oDlg;
	  CENTER;
		PROMPT "Cancela";
		NOROUND;
		ACTION ( lProcess := FALSE, lExit := TRUE, oDlg:End() );
		GRADIENT BtnGradRed()
    oBtn[2]:lCancel = .T.	
	
	ACTIVATE DIALOG oDlg;
		VALID lExit;
		ON INIT ( DisableSysMenuDlg(oDlg) )
		
  if lProcess
   	 if lNew
   	 	  MsgProgress("Procesando", "Espere", {|| NewRecord( h, oQry )})
   	 else	  
   	 	  MsgProgress("Procesando", "Espere", {|| ModRecord( h, oQry )})
   	 endif
  endif		
			
Return Nil

//---------------------------------------

static Function ArrayNivel()
Return { { 1, "1= ACTIVO" 		},;
				 { 2, "2= PASIVO" 		},;
				 { 3, "3= PERDIDAS"   },;
				 { 4, "4= GANANCIAS" 	},;
				 { 5, "5= ORDEN" 			} }

//---------------------------------------

static Function ArraySiNo()
Return { "0= SI", "1= NO" }

//---------------------------------------

static Function NewRecord( h, oQry )
    
  oSrv:InsertHash(  TABLA_CATALOGO,;
  									{ "cuenta"      => h["cuenta"  ],;
  										"nombre"   		=> h["nombre"  ],;
  										"nivel"    		=> h["nivel"   ],;
                      "imputable" 	=> if(SubStr(h["imputable" ],1,1)=="0","S","N"),;
  										"activa"   		=> if(SubStr(h["activa" ],1,1)=="0","S","N"),;
  										"analisis"    => if(SubStr(h["analisis" ],1,1)=="0","S","N") } )
  										
  oQry:Requery()		
 
Return Nil

//---------------------------------------

static Function ModRecord( h, oQry )
    
  oSrv:UpdateHash(  TABLA_CATALOGO ,;
  									{ "cuenta"     	=> h["cuenta" ] ,;
  										"nombre"   		=> h["nombre"],;
  										"nivel"    		=> h["nivel"],;
  										"imputable" 	=> if(SubStr(h["imputable" ],1,1)=="0","S","N"),;
  										"activa"   		=> if(SubStr(h["activa" ],1,1)=="0","S","N"),;
  										"analisis"    => if(SubStr(h["analisis" ],1,1)=="0","S","N") } ,;
  									"my_recno = " +ClipValue2SQL(oQry:my_recno) )
  										
  oQry:Requery()	
  
Return Nil

//---------------------------------------

static Function DelRecord( oQry )
  
  if oQry:RecCount() > 0
  	if MsgYesNo( "Esta Seguro de Eliminar "+CRLF+oQry:nombre, Name_Empresa )
    	if !oSrv:Delete2( TABLA_CATALOGO, "my_recno = "+ClipValue2SQL(oQry:my_recno) )
      		MsgStop("No se pudo Borrar el Registro", Name_Empresa) 
      else
      		oQry:ReQuery()
      endif 
    endif
  endif
  
Return Nil

//---------------------------------------

STATIC Function SiCuenta( cCuenta )
  Local oQry
  Local cQuery
  Local lExiste := FALSE
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE cuenta = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_CATALOGO, ClipValue2SQL(cCuenta) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  lExiste := if(oQry:RecCount() > 0, TRUE, FALSE)
  
  oQry:End()
   
Return lExiste

//---------------------------------------

sTATIC Function GetNameCuenta( cCuenta )
  Local oQry
  Local cQuery
  Local cName := ""
  
  TEXT INTO cQuery
  	SELECT * FROM %1
  	WHERE cuenta = %2
  	LIMIT 1
  ENDTEXT	
  	
  cQuery := StrFormat( cQuery, TABLA_CATALOGO, ClipValue2SQL(cCuenta) )	
    
  oQry = oSrv:CreateQuery( cQuery )

  if oQry:RecCount() > 0
  	 cName := oQry:nombre
  endif	 
  
  oQry:End()
   
Return cName

//---------------------------------------

Function Crear_Catalogo_Maestro()
  Local cStruct
    
  TEXT INTO cStruct
  	CREATE TABLE IF NOT EXISTS %1
  	( cuenta        char(6) NOT NULL DEFAULT '',
     	nombre    		char(80) NOT NULL DEFAULT ' ',
     	nivel         int(1) NOT NULL DEFAULT 1,
     	imputable			char(1) NOT NULL DEFAULT 'N',
     	activa    		char(1) NOT NULL DEFAULT 'N',
     	analisis   		char(1) NOT NULL DEFAULT 'N',
     	my_recno 			int(12) NOT NULL AUTO_INCREMENT,
      PRIMARY KEY (my_recno),
      INDEX cuenta (cuenta),
      INDEX nombre (nombre)
  	  ) COLLATE = 'latin1_spanish_ci' ENGINE = InnoDB
  ENDTEXT
  
  cStruct := StrFormat( cStruct, TABLA_CATALOGO )
  
  oSrv:VerifyTable( TABLA_CATALOGO, cStruct ) 	
          
Return Nil

// FINAL