// WinAlma

#include  "fivewin.ch"
#include  "contabil.ch"

/*
	
	My issue happened right after a power failure. 
	I got the error 1067 The process terminated unexpectedly. MySQL needless to say did not start. 
	The answer was simple

	1- Open mysql path\data
	2- Remove (delete) both ib_logfile0 and ib_logfile1.
	3- Start the service

*/

//----------------------------------------

// #define _DEBUG

//----------------------------------------

#define  BLUE0  RGB( 218, 229, 243 )      
#define  BLUE2  RGB( 194, 217, 240 )      
#define  BLUE1  RGB( 199, 216, 237 )

//----------------------------------------

static oPublic

static oFontBtn
static oFontMnu

//----------------------------------------

REQUEST DBFFPT
REQUEST DBFCDX

REQUEST HB_LANG_ES
REQUEST HB_CODEPAGE_ESWIN

//----------------------------------------

Function Main()
  Local oFactu
  Local oFont, oFont1
  Local oWnd
  Local oBrush
  Local oMsgBar
  Local oFontMsg
  Local cMessage
  Local bExit  
  Local lExit 	:= FALSE
    
  // Llama las Extensiones y Overrides de las CLASS 
  OverrideAndExtend()  
  
  SET EPOCH TO 1950
  SET CENTURY ON
  SET DATE FORMAT TO "DD/MM/YYYY"
  SET TIME FORMAT TO "HH:MM:SS"
    
  SET DELETED 				ON
  SET CASESENSITIVE 	ON
  SET PADRIGHT 				ON
  SET LOGICALVALUE 		ON 
  SET DEFAULTCLPVALUE ON
  
  SET EXACT ON   
	SET DELETED ON
	
	// ? versionLib()
	    
  Set_MyLang( "es_ES" )
 
  HB_LangSelect( "ES" )
  HB_SetCodePage( "ESWIN" )
  	
  RddSetDEFAULT( "DBFCDX" ) // Carga Driver's DBFCDX	
  
  Fw_SetUnicode( .F. )
  
  FWNumFormat( "E", .T. ) // call this function at the beginning of the project
	
	SetCancel( .F. ) // Inutiliza ALT + C para abortar programa 
	
	SetBalloon( .T. ) 
	
	SetGetColorFocus( GET_COLORFOCUS )  // Color para el Get Activo 
  SetCbxColorFocus( GET_COLORFOCUS )  // Color para el Combobox Activo 
  SetMGetColorFocus( GET_COLORFOCUS ) // Color para el Memo Get Activo
	
	if IsExeRunning( cFilename( HB_ARGV( 0 ) ) )
     MsgInfo( cFilename( HB_ARGV( 0 ) ) + " ya se esta Ejecutando", "Avc Sistemas" )
     Return Nil
  endif
      
  // Carga las Variables del Sistema
  oPublic := TVariable():New()
    
  oPublic:AddVar( "oBrw", 			Nil )
  oPublic:AddVar( "bExit", 			Nil )
  oPublic:AddVar( "omsgItem1", 	Nil )
  oPublic:AddVar( "oMsgItem2", 	Nil )
    
  if ! oPublic:lSucess
     Return Nil
  endif
    
  OpenServerMySQL()  // Abre el Servidor MYSQL
  AccesoTablasMySQL()
  
  GetnHeightItem( 1.75 )  // Alto de Item
	GetnWidthItem( 1.50 )		// Ancho de Item
     		 
  	 DEFINE FONT oFontMsg NAME "TAHOMA" SIZE 0,-12 BOLD   
  	 DEFINE FONT oFontBtn NAME "TAHOMA" SIZE 0,-11 BOLD   
  	 DEFINE FONT oFontMnu NAME "TAHOMA" SIZE 0,-11 BOLD

  	 DEFINE WINDOW oWnd ICON oPublic:aBitmaps[ BMP32_ICON ] 
  	 
  	 bExit = <||
  	 					if MsgYesNo("Salir del Sistema", "Seleccione")		
  	 						 lExit := TRUE
  	 						 oWnd:End()
  	 					endif	 
  	 					Return Nil
  	 				 >
  	 				 
  	 oWnd:bKeydown = {|nKey| if(GetKeyState(VK_ESCAPE), Eval(bExit), Nil)}  	 								 
  	 
  	  	 								 
		 DEFINE MSGBAR oMsgBar OF oWnd 2015 CENTER FONT oFontMsg  	
		 		 
		 DEFINE MSGITEM oPublic:omsgItem1 OF oMsgBar PROMPT "" FONT oFontMsg SIZE 900 
		 
		 oMsgBar:KeybOn()
     oMsgBar:DateOn()
     oMsgBar:ClockOn()
     oWnd:oMsgBar:oClock:bMsg = {|| AmPm( Time() ) }  // Displays Time in AmPm format
   	 oWnd:oMsgBar:oClock:nWidth += 20  							 // Increase width of Time column on Msg Bar
             
  	 ACTIVATE WINDOW oWnd MAXIMIZED ;
     	 VALID lExit;
     	 ON INIT ( CreateButtonBar( oWnd, bExit ),; 
     	           oPublic:oWnd := oWnd,;
     	 					 oPublic:SetTitle() )
  	   	 
  	 RELEASE FONT oFontMsg
  	 RELEASE FONT oFontBtn
  	 RELEASE FONT oFontMnu
	
Return Nil

//----------------------------------------

Function oApp()
Return oPublic

//----------------------------------------

static Function CreateButtonBar( oWnd, bExit )
  Local oBar
	Local oBtn := Array(7)
		
	DEFINE BUTTONBAR oBar OF oWnd 2015 SIZE 70, 70 
	
	DEFINE BUTTON oBtn[1] OF oBar; 
  	PROMPT "Empresa"; 
  	RESOURCE oPublic:aBitmaps[ BMP32_EMPRESA ];
  	ACTION Do_Empresas();
  	FONT oFontBtn;
    TOOLTIP "Empresa"
    
  DEFINE BUTTON oBtn[2] OF oBar; 
  	PROMPT "Tablas"; 
  	RESOURCE oPublic:aBitmaps[ BMP32_DBF ];
  	ACTION PopupTablas( oBtn[2] );
    FONT oFontBtn;
    TOOLTIP "Catalogo";
    WHEN oPublic:Session["activa"];
    GROUP
    
  DEFINE BUTTON oBtn[3] OF oBar; 
  	PROMPT "Asientos"; 
  	RESOURCE oPublic:aBitmaps[ BMP32_REPORT ];
  	ACTION Do_Asientos();
  	FONT oFontBtn;
    TOOLTIP "Asientos";
    WHEN oPublic:Session["activa"];
    GROUP  
    
  DEFINE BUTTON oBtn[4] OF oBar; 
  	PROMPT "Reportes"; 
  	RESOURCE oPublic:aBitmaps[ BMP32_PRINT ];
  	ACTION PopupReporte( oBtn[4] );
  	FONT oFontBtn;
    TOOLTIP "Reportes";
    WHEN oPublic:Session["activa"];
    GROUP    
        
  DEFINE BUTTON oBtn[5] OF oBar; 
  	PROMPT "Dise�ador"; 
  	RESOURCE oPublic:aBitmaps[ BMP32_DESIGNER ];
  	ACTION PopupDesigner( oBtn[5] );
  	FONT oFontBtn;
    TOOLTIP "Dise�ador";
    WHEN oPublic:Session["activa"];
    GROUP  
    
	DEFINE BUTTON oBtn[6] OF oBar; 
  	PROMPT "Salir"; 
  	RESOURCE oPublic:aBitmaps[ BMP32_EXIT ]; 
  	ACTION Eval(bExit); 
  	FONT oFontBtn;
  	GROUP
  		 
Return Nil  	 

//----------------------------------------

static Function OpenServerMySQL()
  
  if hb_isObject( oPublic:oServer )
     if !oPublic:oServer:IskindOf( "TMYSQLSERVER" )
  			oPublic:oServer = TMySqlServer():New()
  	 endif		
  endif			
  
Return Nil

//----------------------------------------

static Function CloseServerMySQL()
  
  if hb_isObject( oPublic:oServer )
     if oPublic:oServer:IskindOf( "TMYSQLSERVER" )
  	 	  oPublic:oServer:End()
  	 else
  	    MsgStop("No se pudo cerra la conexion MYSQL Server", "Error MYSQL")	  
  	 endif	  
  endif
  
Return Nil  

//----------------------------------------

static Function AccesoTablasMySQL()

	MsgProgress( "Verificando Tablas", ;
							 "Espere por favor ...",; 
							 {|| CheckTablasMySQL()} )
		
Return Nil

//----------------------------------------

static Function CheckTablasMySQL()

	Crear_Catalogo_Maestro()
	Crear_Empresas()
	Crear_TipoEmpresa()
  Crear_Clientes()
  Crear_Provedores()
  Crear_Catalogo()
  Crear_Regiones()
  Crear_Ciudades()
  Crear_Comunas()
  Crear_TipoDocumento()
  Crear_Balance()
  Crear_AsientoMaestro()
  Crear_AsientoLineas()
  
  Crear_TempLibroMayor()
    
Return Nil

//----------------------------------------

static Function PopupTablas( oBtn )
  Local oMenu
      
  MENU oMenu POPUP 2015 HEIGHT 1.0 * 2 FONT oFontMnu
  
    MENUITEM "Catalogo Maestro" ACTION Catalogo_Maestro()	WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Regiones" ACTION Regiones() WHEN oPublic:Session["activa"]
    MENUITEM "Ciudades" ACTION Ciudades() WHEN oPublic:Session["activa"]
    MENUITEM "Comunas" 	ACTION Comunas()  WHEN oPublic:Session["activa"]
    MENUITEM "Tipo Documento" ACTION Tipo_Documento() WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Catalogo" 	ACTION Catalogo()	WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Clientes" 	ACTION Clientes()	WHEN oPublic:Session["activa"]
    MENUITEM "Provedores" ACTION Provedores() WHEN oPublic:Session["activa"]
    		
	ENDMENU
	
	ACTIVATE POPUP oMenu AT oBtn:nHeight() + oBtn:nTop, oBtn:nLeft OF oBtn:oWnd
    
Return oMenu

//----------------------------------------

static Function PopupReporte( oBtn )
  Local oMenu
      
  MENU oMenu POPUP 2015 HEIGHT 1.0 * 2 FONT oFontMnu
  
    MENUITEM "Catalogo" ACTION Fr3_Catalogo( FR3_SHOW)	WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Clientes" ACTION Fr3_Cliente( FR3_SHOW ) WHEN oPublic:Session["activa"]
    MENUITEM "Provedor" ACTION Fr3_Provedor( FR3_SHOW ) WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Libro Diario Borrador" ACTION Fr3_LibroDiarioBorrador( FR3_SHOW ) WHEN oPublic:Session["activa"]
    MENUITEM "Libro Diario Oficial" ACTION Fr3_LibroDiarioOficial( FR3_SHOW ) WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Libro Mayor" ACTION Fr3_LibroMayor( "TODAS", FR3_SHOW ) WHEN oPublic:Session["activa"]
    MENUITEM "Libro Mayor Cuenta" ACTION Fr3_LibroMayor( "CUENTA", FR3_SHOW ) WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Balance" ACTION Fr3_LibroBalance( FR3_SHOW ) WHEN oPublic:Session["activa"]
    		
	ENDMENU
	
	ACTIVATE POPUP oMenu AT oBtn:nHeight() + oBtn:nTop, oBtn:nLeft OF oBtn:oWnd
    
Return oMenu

//----------------------------------------

static Function PopupDesigner( oBtn )
  Local oMenu
      
  MENU oMenu POPUP 2015 HEIGHT 1.0 * 2 FONT oFontMnu
  
    MENUITEM "Catalogo" ACTION Fr3_Catalogo( FR3_DESIGNER )	WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Clientes" ACTION Fr3_Cliente( FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    MENUITEM "Provedor" ACTION Fr3_Provedor( FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Libro Diario Borrador" ACTION Fr3_LibroDiarioBorrador( FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    MENUITEM "Libro Diario Oficial" ACTION Fr3_LibroDiarioOficial( FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Libro Mayor" ACTION Fr3_LibroMayor( "TODAS", FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    MENUITEM "Libro Mayor Cuenta" ACTION Fr3_LibroMayor( "CUENTA", FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    SEPARATOR
    MENUITEM "Balance" ACTION Fr3_LibroBalance( FR3_DESIGNER ) WHEN oPublic:Session["activa"]
    		
	ENDMENU
	
	ACTIVATE POPUP oMenu AT oBtn:nHeight() + oBtn:nTop, oBtn:nLeft OF oBtn:oWnd
    
Return oMenu

// FINAL