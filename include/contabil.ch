#define __VERSION "v.001a"

#define  TRUE  .T.
#define  FALSE .F.

#define  true  .T.
#define  false .F.

// Manejo para MySQL
//---------------------------------------
#define BFALSE 0
#define BTRUE  1

// Color de Get Activo
//---------------------------------------
#define  GET_COLORFOCUS  RGB( 0, 255, 255 ) // RGB( 255, 141, 28 )

// Defines para los Archivos de BitMaps
//---------------------------------------
#define  BMP32_NEW        	1
#define  BMP32_PLUS       	2
#define  BMP32_EDIT       	3
#define  BMP32_MINUS      	4
#define  BMP32_EXIT       	5
#define  BMP32_BACKGRND   	6
#define  BMP32_EMPRESA    	7
#define  BMP32_PROCESS    	8
#define  BMP32_YES        	9
#define  BMP32_RUN        	10
#define  BMP32_ICON       	12
#define  BMP32_SEARCH     	13
#define  BMP32_INDEX      	14
#define  BMP32_TOP        	15
#define  BMP32_BOTTOM     	16
#define  BMP32_PRINT      	17
#define  BMP32_REPORT     	18
#define  BMP32_PEOPLE     	19
#define  BMP32_USER       	20
#define  BMP16_SEARCH     	21
#define  BMP32_OPEN       	22
#define  BMP32_MONEY      	23
#define  BMP32_VENTAS     	24
#define  BMP32_COMPRAS    	25
#define  BMP32_EXCEL      	26
#define  BMP32_DBF 			  	27
#define  BMP32_CATALOGO   	28
#define  BMP32_DISABLE    	29
#define  BMP32_INVENTARIO 	30
#define  BMP32_PARTNER    	31
#define  BMP32_OFFER      	32
#define  BMP32_SAVE       	33
#define  BMP32_LLAVE      	34
#define  BMP32_NOTES      	35
#define  BMP32_ADMIN      	36
#define  BMP32_BARCODE    	37
#define  BMP32_CREDITCARD 	38
#define  BMP32_DRAWER     	39
#define  BMP32_LENTE      	40
#define  BMP32_MONTURA    	41
#define  BMP32_ORDENLAB   	42
#define  BMP32_DELETE     	43
#define  BMP32_SERVICIO   	44
#define  BMP32_FINISH     	45
#define  BMP32_BROWSE     	46
#define  BMP32_IMPORTAR   	47
#define  BMP32_CLEAR      	48
#define  BMP32_MESA       	49
#define  BMP32_MESONERO   	50
#define  BMP32_FOLDERADD  	51
#define  BMP32_FOLDERLIS  	52
#define  BMP32_FOLDERCHK  	53
#define  BMP32_DESIGNER   	54
#define  BMP32_CANCEL		  	55
#define  BMP32_BALANZA    	56
#define  BMP32_RETENCION  	57
#define  BMP32_COMPONENTES 	58
#define  BMP32_CODESEARCH		59
#define  BMP32_HANDOK				60
#define  BMP16_ON         	61
#define  BMP16_OFF        	62
#define  BMP16_CALC       	63
#define  BMP16_CREDITCARD 	64
#define  BMP16_REFRESH			65 
#define  BMP16_CALENDAR   	66

#define  LEN_BITMAPS  66

// item de Asiento Contable
//---------------------------------------
#define _CUENTA			1
#define _NOMBRE			2
#define _DEBE				3
#define _HABER			4
#define _DOCUMENTO  5

//---------------------------------------

#include "c:\dolphin\include\tdolphin.ch"
#include "defines_fr3.ch"
